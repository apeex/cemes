<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";');
        Schema::create('t_detail_transactions', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->integer('amount')->default(1);
            $table->float('total_amount')->default(0);
            $table->string('resi_code')->nullable();
            $table->timestamp('resi_date')->nullable();
            $table->string('expedition_code')->nullable();
            $table->string('expedition_code_item')->nullable();
            $table->float('fee_delivery')->default(0);
            $table->uuid('id_delivery')->nullable();
            $table->uuid('id_cattle')->nullable();
            $table->uuid('id_transaction')->nullable();
            $table->date('date')->nullable();
            $table->integer('status')->default(0);
            $table->date('deleted_at')->nullable();
            $table->timestamps();
        });
        DB::statement('ALTER TABLE t_detail_transactions ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_detail_transactions');
    }
};

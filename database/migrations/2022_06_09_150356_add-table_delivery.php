<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";');
        Schema::create('t_delivery_tracks', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('share_lock')->nullable();
            $table->float('latitude')->nullable();
            $table->float('longitude')->nullable();
            $table->uuid('id_detail_transaction')->nullable();
            $table->uuid('id_delivery')->nullable();
            $table->integer('status')->default(0);
            $table->date('deleted_at')->nullable();
            $table->timestamps();
        });
        DB::statement('ALTER TABLE t_delivery_tracks ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_delivery_tracks');
    }
};

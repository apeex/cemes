<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";');
        Schema::create('t_cattles', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('cattle_id')->nullable();
            $table->string('cattle_name')->nullable();
            $table->integer('type')->default(1);
            $table->integer('gender')->default(1);
            $table->date('birthday')->nullable();
            $table->date('purchase_date')->nullable();
            $table->uuid('id_category')->nullable();
            $table->uuid('male_parent')->nullable();
            $table->uuid('female_parent')->nullable();
            $table->float('price')->nullable();
            $table->float('weigth')->nullable();
            $table->integer('status')->default(1);
            $table->timestamps();
            $table->date('deleted_at')->nullable();
        });
        DB::statement('ALTER TABLE t_cattles ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_cattles');
    }
};

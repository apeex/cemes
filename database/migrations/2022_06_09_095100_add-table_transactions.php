<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";');
        Schema::create('t_transactions', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('no_invoice')->nullable();
            $table->string('va_number')->nullable();
            $table->string('no_resi')->nullable();
            $table->string('invoice_url')->nullable();
            $table->integer('type')->default(1);
            $table->float('nominal')->default(0);
            $table->float('total_amount')->default(0);
            $table->float('discount')->default(0);
            $table->float('fee_delivery')->default(0);
            $table->uuid('id_promotion')->nullable();
            $table->uuid('id_payment')->nullable();
            $table->uuid('id_user')->nullable();
            $table->date('date')->nullable();
            $table->timestamp('paid_at')->nullable();
            $table->timestamp('limit_payment_date')->nullable();
            $table->integer('status')->default(0);
            $table->date('deleted_at')->nullable();
            $table->timestamps();
        });
        DB::statement('ALTER TABLE t_transactions ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_transactions');
    }
};

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";');
        Schema::create('t_eat_histories', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('id_cattle')->nullable();
            $table->uuid('id_shed')->nullable();
            $table->uuid('id_food')->nullable();
            $table->integer('status')->default(1);
            $table->float('weigth')->default(0);
            $table->date('date')->nullable();
            $table->timestamps();
            $table->date('deleted_at')->nullable();
        });
        DB::statement('ALTER TABLE t_eat_histories ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_eat_histories');
    }
};

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";');
        Schema::create('t_deliveries', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('delivery_name')->nullable();
            $table->string('delivery_code')->nullable();
            $table->string('delivery_icon')->nullable();
            $table->text('description')->nullable();
            $table->string('group')->nullable();
            $table->integer('is_self_delivery')->default(0);
            $table->uuid('id_user')->nullable();
            $table->integer('maximum_distance')->nullable();
            $table->float('price')->nullable();
            $table->integer('status')->default(0);
            $table->date('deleted_at')->nullable();
            $table->timestamps();
        });
        DB::statement('ALTER TABLE t_deliveries ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_deliveries');
    }
};

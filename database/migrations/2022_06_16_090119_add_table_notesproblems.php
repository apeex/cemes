<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";');
        Schema::create('t_note_problems', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('id_note')->nullable();
            $table->uuid('id_object')->nullable();
            $table->integer('type')->default(1);
            $table->timestamps();
        });
        DB::statement('ALTER TABLE t_note_problems ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_note_problems');
    }
};

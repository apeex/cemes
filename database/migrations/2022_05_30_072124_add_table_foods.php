<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";');
        Schema::create('t_foods', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('food_name')->nullable();
            $table->string('food_id')->nullable();
            $table->string('icon')->nullable();
            $table->date('date')->nullable();
            $table->float('weigth')->default(0);
            $table->float('capital_price')->default(0);
            $table->integer('status')->default(1);
            $table->text('description')->nullable();
            $table->timestamps();
            $table->date('deleted_at')->nullable();
        });
        DB::statement('ALTER TABLE t_foods ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_foods');
    }
};

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";');
        Schema::create('t_sheds', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('shed_id')->nullable();
            $table->string('shed_name')->nullable();
            $table->string('address')->nullable();
            $table->integer('id_province')->nullable();
            $table->integer('id_city')->nullable();
            $table->integer('status')->default(1);
            $table->timestamps();
            $table->date('deleted_at')->nullable();
        });
        DB::statement('ALTER TABLE t_sheds ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_sheds');
    }
};

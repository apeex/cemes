<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\MenuModel;
use App\Models\RoleModel;
use DB;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        date_default_timezone_set('Asia/Jakarta');
        DB::table('t_menus')->truncate();
        DB::table('t_menus')->insert([
            ['name' =>  'Dashboard',  'parent_menu_id' => 0,'created_at' => date('Y-m-d H:m:s'),'updated_at' => date('Y-m-d H:m:s'),'menu_id' => 1,'slug' =>'dashboard','icon'=>'fas fa-tachometer-alt','no_urut'=>1],
            ['name' =>  'Administrator',  'parent_menu_id' => 0,'created_at' => date('Y-m-d H:m:s'),'updated_at' => date('Y-m-d H:m:s'),'menu_id' => 2,'slug' =>'administrator','icon'=>'fas fa-users-cog','no_urut'=>2],
            ['name' =>  'Peternak',  'parent_menu_id' => 0,'created_at' => date('Y-m-d H:m:s'),'updated_at' => date('Y-m-d H:m:s'),'menu_id' => 3,'slug' =>'farmer','icon'=>'fas fa-list-alt','no_urut'=>3],
            ['name' =>  'Kandang',  'parent_menu_id' => 0,'created_at' => date('Y-m-d H:m:s'),'updated_at' => date('Y-m-d H:m:s'),'menu_id' => 4,'slug' =>'shed','icon'=>'fas fa-star-and-crescent','no_urut'=>4],
            ['name' =>  'Pakan',  'parent_menu_id' => 0,'created_at' => date('Y-m-d H:m:s'),'updated_at' => date('Y-m-d H:m:s'),'menu_id' => 5,'slug' =>'food','icon'=>'fas fa-star-and-crescent','no_urut'=>5],
            ['name' =>  'Hewan Ternak',  'parent_menu_id' => 0,'created_at' => date('Y-m-d H:m:s'),'updated_at' => date('Y-m-d H:m:s'),'menu_id' => 6,'slug' =>'cattle','icon'=>'fas fa-mosque','no_urut'=>6],
            ['name' =>  'Notifikasi',  'parent_menu_id' => 0,'created_at' => date('Y-m-d H:m:s'),'updated_at' => date('Y-m-d H:m:s'),'menu_id' => 7,'slug' =>'notifikasi','icon'=>'fas fa-bell','no_urut'=>7],
            ['name' =>  'Laporan',  'parent_menu_id' => 0,'created_at' => date('Y-m-d H:m:s'),'updated_at' => date('Y-m-d H:m:s'),'menu_id' => 8,'slug' =>'laporan','icon'=>'fas fa-pen-square','no_urut'=>8],
            ['name' =>  'Master',  'parent_menu_id' => 0,'created_at' => date('Y-m-d H:m:s'),'updated_at' => date('Y-m-d H:m:s'),'menu_id' => 9,'slug' =>'master','icon'=>'fas fa-server','no_urut'=>9],
            ['name' =>  'Logs',  'parent_menu_id' => 0,'created_at' => date('Y-m-d H:m:s'),'updated_at' => date('Y-m-d H:m:s'),'menu_id' => 10,'slug' =>'logs','icon'=>'fas fa-rss-square','no_urut'=>10],
            // sub menu admin
            ['name' =>  'List Admin','parent_menu_id' => 2,'created_at' => date('Y-m-d H:m:s'),'updated_at' => date('Y-m-d H:m:s'),'menu_id' => 0,'slug' =>'list-admin','icon'=>'null','no_urut'=>2],
            ['name' =>  'Role Admin','parent_menu_id' => 2,'created_at' => date('Y-m-d H:m:s'),'updated_at' => date('Y-m-d H:m:s'),'menu_id' => 0,'slug' =>'role-admin','icon'=>'null','no_urut'=>3],

            // sub menu peternak
            ['name' =>  'List Peternak','parent_menu_id' => 3,'created_at' => date('Y-m-d H:m:s'),'updated_at' => date('Y-m-d H:m:s'),'menu_id' => 0,'slug' =>'list-farmer','icon'=>'null','no_urut'=>1],
            ['name' =>  'Catatan Peternak','parent_menu_id' => 3,'created_at' => date('Y-m-d H:m:s'),'updated_at' => date('Y-m-d H:m:s'),'menu_id' => 0,'slug' =>'notes','icon'=>'null','no_urut'=>2],

            // sub menu kandang
            ['name' =>  'List Kandang','parent_menu_id' => 4,'created_at' => date('Y-m-d H:m:s'),'updated_at' => date('Y-m-d H:m:s'),'menu_id' => 0,'slug' =>'list','icon'=>'null','no_urut'=>1],
            ['name' =>  'Riwayat Kandang','parent_menu_id' => 4,'created_at' => date('Y-m-d H:m:s'),'updated_at' => date('Y-m-d H:m:s'),'menu_id' => 0,'slug' =>'history','icon'=>'null','no_urut'=>2],
            // sub menu pakan
            ['name' =>  'Jenis Pakan','parent_menu_id' => 5,'created_at' => date('Y-m-d H:m:s'),'updated_at' => date('Y-m-d H:m:s'),'menu_id' => 0,'slug' =>'category','icon'=>'null','no_urut'=>1],
            ['name' =>  'List Pakan','parent_menu_id' => 5,'created_at' => date('Y-m-d H:m:s'),'updated_at' => date('Y-m-d H:m:s'),'menu_id' => 0,'slug' =>'list','icon'=>'null','no_urut'=>2],
            ['name' =>  'Riwayat Pakan','parent_menu_id' => 5,'created_at' => date('Y-m-d H:m:s'),'updated_at' => date('Y-m-d H:m:s'),'menu_id' => 0,'slug' =>'list','icon'=>'null','no_urut'=>3],

            // sub menu hewan ternak
            ['name' =>  'Jenis Ternak','parent_menu_id' => 6,'created_at' => date('Y-m-d H:m:s'),'updated_at' => date('Y-m-d H:m:s'),'menu_id' => 0,'slug' =>'category','icon'=>'null','no_urut'=>1],
            ['name' =>  'List Ternak','parent_menu_id' => 6,'created_at' => date('Y-m-d H:m:s'),'updated_at' => date('Y-m-d H:m:s'),'menu_id' => 0,'slug' =>'list','icon'=>'null','no_urut'=>2],
            ['name' =>  'Riwayat Ternak','parent_menu_id' => 6,'created_at' => date('Y-m-d H:m:s'),'updated_at' => date('Y-m-d H:m:s'),'menu_id' => 0,'slug' =>'history','icon'=>'null','no_urut'=>3],
            
            // List notifikasi
            ['name' =>  'List Notifikasi','parent_menu_id' => 7,'created_at' => date('Y-m-d H:m:s'),'updated_at' => date('Y-m-d H:m:s'),'menu_id' => 0,'slug' =>'notifikasi','icon'=>'null','no_urut'=>1],
            // List banner
            ['name' =>  'List Laporan','parent_menu_id' => 8,'created_at' => date('Y-m-d H:m:s'),'updated_at' => date('Y-m-d H:m:s'),'menu_id' => 0,'slug' =>'list','icon'=>'null','no_urut'=>1],


            ['name' =>  'Master Negara','parent_menu_id' => 9,'created_at' => date('Y-m-d H:m:s'),'updated_at' => date('Y-m-d H:m:s'),'menu_id' => 0,'slug' =>'negara','icon'=>'null','no_urut'=>1],
            ['name' =>  'Master Provinsi','parent_menu_id' => 9,'created_at' => date('Y-m-d H:m:s'),'updated_at' => date('Y-m-d H:m:s'),'menu_id' => 0,'slug' =>'provinsi','icon'=>'null','no_urut'=>2],
            ['name' =>  'Master Kota','parent_menu_id' => 9,'created_at' => date('Y-m-d H:m:s'),'updated_at' => date('Y-m-d H:m:s'),'menu_id' => 0,'slug' =>'kota','icon'=>'null','no_urut'=>3],
            ['name' =>  'Master Kecamatan','parent_menu_id' => 9,'created_at' => date('Y-m-d H:m:s'),'updated_at' => date('Y-m-d H:m:s'),'menu_id' => 0,'slug' =>'kecamatan','icon'=>'null','no_urut'=>4],
            ['name' =>  'Master Desa','parent_menu_id' => 9,'created_at' => date('Y-m-d H:m:s'),'updated_at' => date('Y-m-d H:m:s'),'menu_id' => 0,'slug' =>'desa','icon'=>'null','no_urut'=>5],

            ['name' =>  'Logs Admin','parent_menu_id' => 10,'created_at' => date('Y-m-d H:m:s'),'updated_at' => date('Y-m-d H:m:s'),'menu_id' => 0,'slug' =>'logs_admin','icon'=>'null','no_urut'=>1],
            ['name' =>  'Logs Peternak','parent_menu_id' => 10,'created_at' => date('Y-m-d H:m:s'),'updated_at' => date('Y-m-d H:m:s'),'menu_id' => 0,'slug' =>'logs_farmer','icon'=>'null','no_urut'=>2],
            ['name' =>  'Logs Error','parent_menu_id' => 10,'created_at' => date('Y-m-d H:m:s'),'updated_at' => date('Y-m-d H:m:s'),'menu_id' => 0,'slug' =>'logs_error','icon'=>'null','no_urut'=>4],
        ]);
        date_default_timezone_set('Asia/Jakarta');
        // seeder departement
        DB::table('t_roles')->truncate();
        DB::table('t_roles')->insert(
            ['name' =>  'SUPERADMIN','slug'=>'superadmin',  'status' => 1,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d')]
        );
        
        //seeder2
        DB::table('t_role_menus')->truncate();
        $data_array = MenuModel::all();
        $id_role = RoleModel::select('id')->pluck('id');
        foreach ($data_array as $k => $v) {
                date_default_timezone_set('Asia/Jakarta');
                DB::table('t_role_menus')->insert(
                    ['role_id' => $id_role[0],  'menu_id' => $v->id,'created_at' => date('Y-m-d H:m:s'),'updated_at' => date('Y-m-d H:m:s')]
                );
        }
        //seeder3
        DB::table('t_administrators')->truncate();
        $data_array = RoleModel::all();
        foreach ($data_array as $k => $v) {
                date_default_timezone_set('Asia/Jakarta');
                DB::table('t_administrators')->insert(
                    ['id_role' => $v->id,'admin_name' => 'admin',  'phone' => '081320938989','email' => 'adminaslis@gmail.com','password' => bcrypt('aslis123'),'confirm_password' => encrypt('aslis123'),'address' => 'surabaya','status' => 1,'created_at' => date('Y-m-d H:m:s'),'updated_at' => date('Y-m-d H:m:s')]
                );
        }
    }
}

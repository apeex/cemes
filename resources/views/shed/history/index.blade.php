@extends('layout.app')
@section('asset')
<link rel="stylesheet" type="text/css" href="{{URL::asset('assets')}}/vendors/css/forms/select/select2.min.css">
@endsection
@section('title', 'Administrator')
@section('content')
<div class="row">
    <div class="">
        <!--begin::List Widget 9-->
        <div class="card card-custom card-stretch gutter-b">
            <!--begin::Header-->
            <div class="p-4">
                <form id="form_filter">
                    <div class="row">
                        <div class="col-md-4">
                            <label>Tanggal Pindah</label>
                            <input type="date" id="tanggal" onchange="DataTable()" name="tanggal" class="form-control">
                        </div>
                        <div class="col-md-4">
                            <label>Asal Kandang</label>
                            <select class="form-control select2" name="old_shed" id="old_shed" onchange="DataTable()">
                                <option value="" selected>Semua kandang </option>
                                @foreach($shed as $k=>$v)
                                <option value="{{$v->id}}">{{$v->shed_name}} ({{$v->user_name}})</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label>Kandang Baru</label>
                            <select class="form-control select2" name="new_shed" id="new_shed" onchange="DataTable()">
                                <option value="" selected>Semua kandang </option>
                                @foreach($shed as $k=>$v)
                                <option value="{{$v->id}}">{{$v->shed_name}} ({{$v->user_name}})</option>
                                @endforeach
                            </select>
                        </div>
                    </div>  
                </form>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive m-t-40">
                                <table id="data_tabel" class="table display table-bordered table-striped no-wrap">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Hewan Ternak</th>
                                            <th>Kandang Asal</th>
                                            <th>Kandang Baru</th>
                                            <th>Tanggal</th>
                                            <th>Deskripsi</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('components/componen_js')
@include('components/js/shed/history/index')
@endsection
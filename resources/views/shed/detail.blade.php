@extends('layout.app')
@section('asset')
<link rel="stylesheet" type="text/css" href="{{URL::asset('assets')}}/vendors/css/forms/select/select2.min.css">
@endsection
@section('title', 'Detail Kandang')
@section('content')
<!--  main page// -->
    <div class="content-header">
        <a href="javascript:history.back()"><i class="material-icons md-arrow_back"></i> Go back </a>
    </div>
    <div class="card mb-4">
        <div class="card-header bg-brand-2" style="height: 150px"></div>
        <div class="card-body">
            <div class="row">
                <div class="col-xl col-lg flex-grow-0" style="flex-basis: 230px">
                    <div class="img-thumbnail shadow w-100 bg-white position-relative text-center" style="height: 190px; width: 200px; margin-top: -120px">
                        @if($data->profile != null)
                        <img src="{{env('BASE_IMG')}}{{$data->profile}}" class="center-xy img-fluid" alt="Logo Brand" style="height: 190px;">
                        @else
                        <img src="{{URL::asset('assets')}}/imgs/items/1.jpg" class="center-xy img-fluid" alt="Logo Brand">
                        @endif
                    </div>
                </div>
                <!--  col.// -->
                <div class="col-xl col-lg">
                    <h3>{{$data->shed_name}} - {{$data->shed_id}}</h3>
                    <p><i class="fa fa-map-marker-alt ml-2"></i> {{GetAddressName('cities',$data->id_city)}} - {{$data->address}}</p>
                </div>
                <!--  col.// -->
                <div class="col-xl-4 text-md-end">
                    
                </div>
                <!--  col.// -->
            </div>
            <!-- card-body.// -->
            <hr class="my-4">
            <div class="row g-4">
                <div class="col-md-3">
                    <article class="box">
                        <p class="mb-0 text-muted">Total Ternak Saat Ini:</p>
                        <h5 class="text-success">{{WeigthOfShed($data->id,1)}} Ekor</h5>
                        <p class="mb-0 text-muted">Total Berat Saat Ini:</p>
                        <h5 class="text-success mb-0">{{WeigthOfShed($data->id,2)}} Kg</h5>
                    </article>
                </div>
                <!--  col.// -->
                <div class="col-sm-6 col-lg-4 col-xl-3">
                    <h6>Data Kandang</h6>
                    <p>
                        Kapasitas        : {{$data->capacity }} Ekor<br>
                        Kondisi Sekarang : {{$data->condition}}
                    </p>
                </div>
                <!--  col.// -->
                <div class="col-sm-6 col-lg-4 col-xl-3">
                    <h6>Lokasi Kandang</h6>
                    <p>
                        Provinsi :  {{GetAddressName('provinces',$data->id_province)}}<br>
                        Kota     :  {{GetAddressName('cities',$data->id_city)}}<br>
                        Address  : {{$data->address}} <br>
                    </p>
                </div>
                <div class="col-sm-6 col-lg-4 col-xl-3">
                    <h6>Data Pemilik</h6>
                    <p>
                        email          : {{$data->user_name}} <br>
                        email          : {{$data->user_email}} <br>
                        phone          : {{$data->user_phone}}<br>
                        Jenis kelamin  : {{$data->gender}}<br>
                        Tanggal Lahir  : {{$data->birthday}} <br>
                        No Nik         : {{$data->user_nik}}<br>
                    </p>
                </div>
                <!--  col.// -->
                <div class="col-sm-6 col-xl-4 text-xl-end">
                    
                </div>
                <!--  col.// -->
            </div>
            <!--  row.// -->
        </div>
        <!--  card-body.// -->
    </div>
<div class="row">
    <div class="">
        <!--begin::List Widget 9-->
        <div class="card card-custom card-stretch gutter-b">
            <!--begin::Header-->
            <div class="p-4">
                <form id="form_filter">
                    <div class="row">
                        <div class="col-md-4">
                            <label>Tanggal Pindah</label>
                            <input type="date" id="tanggal" onchange="DataTable()" name="tanggal" class="form-control">
                        </div>
                        <input type="hidden" name="old_shed" id="old_shed" value="">
                        <input type="hidden" name="new_shed" id="new_shed" value="{{$data->id}}">
                    </div>  
                </form>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive m-t-40">
                                <table id="data_tabel" class="table display table-bordered table-striped no-wrap">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Hewan Ternak</th>
                                            <th>Kandang Asal</th>
                                            <th>Kandang Baru</th>
                                            <th>Tanggal</th>
                                            <th>Deskripsi</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('components/componen_js')
@include('components/js/shed/history/index')
@endsection
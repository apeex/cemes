@extends('layout.app')
@section('asset')
<link rel="stylesheet" type="text/css" href="{{URL::asset('assets')}}/vendors/css/forms/select/select2.min.css">
@endsection
@section('title', 'Administrator')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <form id="form_edit" method="post" enctype="multipart/form-data">@csrf
                    <div class="form-body">
                        <div class="row p-t-20">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Nama Kategori</label>
                                    <input type="text" name="category_name" id="nama" class="form-control" value="{{ $data->category_name}}" placeholder="">
                                    <input type="hidden" name="id" value="{{$data->id}}">
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="card h-100">
                                    <div class="card-header text-center">
                                        ICON
                                    </div>
                                    <div class="card-body text-center">
                                        <div class="input-upload">
                                            @if($data->icon != null)
                                            <img id="profile" src="{{ env('BASE_IMG')}}{{$data->icon}}" alt="your image">
                                            @else
                                            <img id="profile" src="{{URL::asset('assets')}}/imgs/theme/upload.svg" alt="your image">
                                            @endif
                                            <label class="btn btn-success btn-sm mt-1 w-100 align-self-center">
                                            Change File <input type="file" name="profile" style="display: none;" onchange="gantiProfile(this);">
                                            </label>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->
                    </div>


                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary mr-2 float-right" id="save_edit">Simpan Perubahan</button>
                        <button type="button" class="btn btn-primary mr-2 float-right" style="display: none;" id="loading_edit" disabled>Loading...</button>
                        <a href="/food/category">
                        <button type="button" class="btn btn-info float-right" style="margin-right: 10px"><i class="material-icons md-keyboard_return"></i> Kembali ke Kategori Pakan</button></a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@include('components/componen_js')
@include('components/js/category_food/edit')
@endsection
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Admin Panel SOUQ</title>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta http-equiv="x-ua-compatible" content="ie=edge" />
        <meta name="description" content="" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta property="og:title" content="" />
        <meta property="og:type" content="" />
        <meta property="og:url" content="" />
        <meta property="og:image" content="" />
        <!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="{{URL::asset('assets')}}/imgs/favicon.png" />
        <!-- Template CSS -->
        <link href="{{URL::asset('assets')}}/css/main.css?v=1.1" rel="stylesheet" type="text/css" />
    </head>
    <style type="text/css">
        span {
            font-size: 11px;
        }
        body {
            overflow-x: hidden;
            max-width: 96%;
        }
    </style>

    <body>
        <div class="p-3 row" style="background-color: white;">
            <div class="col-10">
                <h5 class="content-title card-title mb-0">Dashboard</h5>
                <p id="p_periode">periode : Maret 2022 - Mei 2022</p>
            </div>
            <div class="col-2">
                <a href="javascript:void(0)" onclick="ChangePeriode()" class="text-right" style="float: right;"><img src="{{URL::asset('assets')}}/imgs/calendar.png" style="width: 30px;"></a>
            </div>
        </div>
        <div class="row p-3 mt-5" style="background-color: white;" id="div_isi">
            <div class="text-center"><img src="https://souq.s3-id-jkt-1.kilatstorage.id/laodinggif_1653276083.gif"></div>
        </div>
    </body>
    <!-- Modal -->
    <div class="modal fade text-left" id="modal_calender" tabindex="-1" role="dialog" aria-labelledby="myModalLabel4" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Pilih Periode</h4>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-6">
                            <label>Periode 1</label>
                            <input type="hidden" id="id_user" value="{{$id}}" name="">
                            <input type="month" id="periode1" value="{{$this_month}}" name="" class="form-control">
                        </div>
                        <div class="col-6">
                            <label>Periode 2</label>
                            <input type="month" id="periode2" value="{{$last_month}}" name="" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-bs-dismiss="modal" onclick="Filter()">Submit</button>
                </div>
            </div>
        </div>
    </div>
    <script>var KTAppSettings = { "breakpoints": { "sm": 576, "md": 768, "lg": 992, "xl": 1200, "xxl": 1200 }, "colors": { "theme": { "base": { "white": "#ffffff", "primary": "#6993FF", "secondary": "#E5EAEE", "success": "#1BC5BD", "info": "#8950FC", "warning": "#FFA800", "danger": "#F64E60", "light": "#F3F6F9", "dark": "#212121" }, "light": { "white": "#ffffff", "primary": "#E1E9FF", "secondary": "#ECF0F3", "success": "#C9F7F5", "info": "#EEE5FF", "warning": "#FFF4DE", "danger": "#FFE2E5", "light": "#F3F6F9", "dark": "#D6D6E0" }, "inverse": { "white": "#ffffff", "primary": "#ffffff", "secondary": "#212121", "success": "#ffffff", "info": "#ffffff", "warning": "#ffffff", "danger": "#ffffff", "light": "#464E5F", "dark": "#ffffff" } }, "gray": { "gray-100": "#F3F6F9", "gray-200": "#ECF0F3", "gray-300": "#E5EAEE", "gray-400": "#D6D6E0", "gray-500": "#B5B5C3", "gray-600": "#80808F", "gray-700": "#464E5F", "gray-800": "#1B283F", "gray-900": "#212121" } }, "font-family": "Poppins" };</script>
    <!--end::Global Config-->
    <!--begin::Global Theme Bundle(used by all pages)-->
    <script src="{{URL::asset('assets')}}/plugins/global/plugins.bundle.js"></script>
    <script src="{{URL::asset('assets')}}/plugins/custom/prismjs/prismjs.bundle.js"></script>
    <script src="{{URL::asset('assets')}}/js/scripts.bundle.js"></script>
    <script src="{{URL::asset('assets')}}/js/vendors/bootstrap.bundle.min.js"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="https://code.highcharts.com/modules/accessibility.js"></script>
    <script type="text/javascript">
        function ChangePeriode(){
            //alert("okey");
            $("#modal_calender").modal('show');
        }

        $(function() {
            Filter()
        });

        function Filter(){
            $("#div_isi").html('<div class="text-center"><img src="https://souq.s3-id-jkt-1.kilatstorage.id/laodinggif_1653276083.gif"></div>');
            periode1 = $("#periode1").val();
            periode2 = $("#periode2").val();
            id = $("#id_user").val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({ //line 28
                type: 'POST',
                url: '/get_graphic_ternak',
                dataType: 'html',
                data: {id:id,periode1:periode1,periode2:periode2},
                success: function(data) {  
                    $("#div_isi").html(data);
                }
            });
        }
    </script>
</html>
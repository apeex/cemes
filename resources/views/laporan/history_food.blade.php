<div class="col-12 mb-5">
    <h5 class="content-title card-title mb-0">Periode : {{$bulan1}}</h5>
</div>
<div class="col-6">
    <p>Berat</p>
    <h4><b>{{$weigth1}}</b></h4>
    @if($weigth_progress1 > 0)
    <span class="text-success">+{{$weigth_progress1}}%</span>
    @else
    <span class="text-danger">{{$weigth_progress1}}%</span>
    @endif
    <span class="ml-5">dari bulan lalu</span>
</div>
<div class="col-6 mb-40">
    <p>Nilai Pakan</p>
    <h4><b>Rp {{number_format($price1)}}</b></h4>
    @if($price1 > 0)
    <span class="text-success">+{{$price_progress1}}%</span>
    @else
    <span class="text-danger">{{$price_progress1}}%</span>
    @endif
    <span class="ml-5">dari bulan lalu</span>
</div>
<div class="col-12 mb-5">
    <h5 class="content-title card-title mb-0">Periode : {{$bulan2}}</h5>
</div>
<div class="col-6">
    <p>Berat</p>
    <h4><b>{{$weigth2}}</b></h4>
    @if($weigth_progress2 > 0)
    <span class="text-success">+{{$weigth_progress2}}%</span>
    @else
    <span class="text-danger">{{$weigth_progress2}}%</span>
    @endif
    <span class="ml-5">dari bulan lalu</span>
</div>
<div class="col-6 mb-40">
    <p>Nilai Pakan</p>
    <h4><b>Rp {{number_format($price2)}}</b></h4>
    @if($price_progress2 > 0)
    <span class="text-success">+{{$price_progress2}}%</span>
    @else
    <span class="text-danger">{{$price_progress2}}%</span>
    @endif
    <span class="ml-5">dari bulan lalu</span>
</div>
<div class="col-md-12">
<label>Grafik Jenis Kelamin</label>
  <figure class="highcharts-figure">
    <div id="container"></div>
  </figure>
</div>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<script type="text/javascript">
    Highcharts.chart('container', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Grafik Jenis Kelamin'
        },
        subtitle: {
            text: 'Aslis'
        },
        xAxis: {
            categories: <?php echo json_encode($time); ?>,
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'jumlah (ekor)'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: <?php echo json_encode($data_graph); ?>
    });
</script>
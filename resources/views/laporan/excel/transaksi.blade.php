<style type="text/css">
    .judul {
        text-align: center;
    }
    .judulpt {
        background: yellow;
    }
</style>
<table width="100%" style="" border="1" class="data_diri">
    <tr>
        <td colspan="11" style="font-weight: bold;font-size: 20px">Laporan Transaksi</td>
    </tr>
    @if($start != null)
    <tr>
        <td colspan="2" style="font-size: 20px">Periode</td>
       
        <td colspan="9" style="font-size: 20px">: {{$start}} - {{$end}} </td>
    </tr>
    @endif
    <tr>
        <td colspan="2" style="font-size: 20px">Total Data </td>
       
        <td colspan="9" style="font-size: 20px">: {{$jumlah_data}}</td>
    </tr>
    <tr>
        <td class="judul"><span class="judul">NO</span></td>
        <td class="judul"><span class="judul">No Invoice</span></td>
        <td class="judul"><span class="judul">User</span></td>
        <td class="judul"><span class="judul">Nomimal</span></td>
        <td class="judul"><span class="judul">Diskon</span></td>
        <td class="judul"><span class="judul">Total</span></td>
        <td class="judul"><span class="judul">Status Pembayarn</span></td>
        <td class="judul"><span class="judul">Tanggal Transaksi</span></td>
        <td class="judul"><span class="judul">Tanggal Bayar</span></td>
        <td class="judul"><span class="judul">Cabang</span></td>
        <td class="judul"><span class="judul">Metode Pembayaran</span></td>
    </tr>
    <?php $no = 0;?>
    @foreach($data as $k=>$v)
    <?php $no = $no + 1;?>
    <tr>
        <td><span class="isino">{{$no}}</span></td>
        <td class="isi"><span>{{$v->no_invoice}}</span></td>
        <td class="isi"><span>{{$v->user_name}}</span></td>
        <td class="isi"><span>Rp {{number_format($v->nominal)}}</span></td>
        <td class="isi"><span>Rp {{number_format($v->discount)}}</span></td>
        <td class="isi"><span>Rp {{number_format($v->total_off_pay)}}</span></td>
        <td class="judul">
            
            @if($v->status == 1)
            <span style="color: green">Sudah</span>
            @else
            Belum
            @endif
        </td>
        <td class="judul"><span>{{date('d F Y H:i',strtotime($v->created_at))}}</span></td>
        <td class="judul">
            @if($v->paid_at != null)
            <span>{{date('d F Y H:i',strtotime($v->paid_at))}}</span>
            @else
            -
            @endif
        </td>
        <td class="judul"><span class="judul">{{$v->branch_name}}</span></td>
        <td class="judul"><span>{{$v->metode}}</span></td>
    </tr>
    @endforeach
</table>
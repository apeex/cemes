@extends('layout.app')
@section('asset')
<link rel="stylesheet" type="text/css" href="{{URL::asset('assets')}}/vendors/css/forms/select/select2.min.css">
@endsection
@section('title', 'Administrator')
@section('content')
<div id="show_edit">
</div>
<div id="edit_password">
</div>
<div class="row" id="show_add" style="display: none;">
    <div class="">
        <section class="multiple-validation">
            <div class="card mb-3">
                <div class="card-content">
                    <div class="card-body">
                        <form method="POST" class="form-horizontal p-3" id="form_add">@csrf
                            <div class="form-body">
                                <div class="row p-t-20">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Nama Kategori</label>
                                            <input type="text" name="nama_category" id="nama" class="form-control" value="{{ old('nama_kategori') }}" placeholder="">
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="card h-100">
                                            <div class="card-header text-center">
                                                ICON
                                            </div>
                                            <div class="card-body text-center">
                                                <div class="input-upload">
                                                    <img id="profile" src="{{URL::asset('assets')}}/imgs/theme/upload.svg" alt="your image">
                                                    <label class="btn btn-success btn-sm mt-1 w-100 align-self-center">
                                                    Change File <input type="file" name="profile" style="display: none;" onchange="gantiProfile(this);">
                                                    </label>
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                            </div>
                            <div class="row mt-10">
                                <div class="col-sm-12 col-xs-12">
                                    <button type="submit" class="btn btn-primary float-right" id="save_add">Submit</button>
                                    <button type="button" disabled class="btn btn-primary float-right" id="loading_add" style="display: none;">Loading....</button>
                                    <button type="button" class="btn btn-danger mr-2 float-right" id="batalkan">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<div class="row">
    <div class="">
        <!--begin::List Widget 9-->
        <div class="card card-custom card-stretch gutter-b">
            <!--begin::Header-->
            <div class="p-4">
                <div class="content-header mb-0">
                    <div>
                        <a class="btn btn-primary font-weight-bolder mr-3" id="tambah">Tambah Kategori</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive m-t-40">
                                <table id="data_tabel" class="table display table-bordered table-striped no-wrap">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Kategori</th>
                                            <th>Icon</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('components/componen_js')
@include('components/js/cattle_category/index')
@endsection
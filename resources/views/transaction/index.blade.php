@extends('layout.app')
@section('asset')
<link rel="stylesheet" type="text/css" href="{{URL::asset('assets')}}/vendors/css/forms/select/select2.min.css">
@endsection
@section('title', 'Transaksi')
@section('content')
<div class="row" id="hide_add">
    <div class="col-lg-12">
        <!--begin::List Widget 9-->
        <div class="card card-custom card-stretch gutter-b">
            <!--begin::Header-->
            <div class="card-header border-0 mt-2">
                <form id="form_filter" enctype="multipart/form-data" style="display: contents;">@csrf
                    <div class="row">
                        <div class="col-md-3">
                            <label class="label-control">Status</label>
                            <select class="select2 form-control" onchange="data_tabel('data_transaksi')" name="status" id="status">
                                <option value="">--Semua Status--</option>
                                <option value="1">Belum Dibayar</option>
                                <option value="2">Sudah Dibayar</option>
                                <option value="3">Sedang Diproses</option>
                                <option value="4">Sedang Dikirim</option>
                                <option value="5">Sudah Selesai</option>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <label class="label-control">Tgl Mulai</label>
                            <input type="date" name="" id="start" onchange="data_tabel('data_transaksi')" class="form-control">
                        </div>
                        <div class="col-md-2">
                            <label class="label-control">Tgl Selesai</label>
                            <input type="date" name="" id="end" onchange="data_tabel('data_transaksi')" class="form-control">
                        </div>
                        <div class="col-md-4">
                            <input type="text" placeholder="Search No Invoice" class="form-control form-control-lg mt-20" id="search" onkeyup="data_tabel('data_notifikasi')" name="search">
                        </div>
                        <div class="col-md-1">
                            <button class="btn btn-md btn-danger mt-20" style="width:100%" onclick="Reset()">Reset</button>
                        </div>
                    </div>
                </form>
            </div>
            <!--end::Header-->
            <!--begin::Body-->
            <div class="card-body pt-2">
                <section id="basic-datatable">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <!-- <div class="card-header">
                                    <h4 class="card-title">List Admin</h4>
                                </div> -->
                                <div class="card-content">
                                    <div class="card-body card-dashboard">
                                        <div class="table-responsive">
                                            <table class="table zero-configuration" id="data_tabel">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>No Invoice</th>
                                                        <th>Tanggal</th>
                                                        <th>Nominal</th>
                                                        <th>Status</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <!--end: Card Body-->
        </div>
        <!--end: Card-->
        <!--end: List Widget 9-->
    <!-- </div> -->
</div>
@include('components/componen_js')
@include('components/js/transaction/index')
@endsection
@extends('layout.app')
@section('asset')
<link rel="stylesheet" type="text/css" href="{{URL::asset('assets')}}/vendors/css/forms/select/select2.min.css">
@endsection
@section('title', 'Transaksi')
@section('content')
<div class="card">
    <header class="card-header">
        <div class="row align-items-center">
            <div class="col-lg-6 col-md-6 mb-lg-0 mb-15">
                <span> <i class="material-icons md-calendar_today"></i> <b>{{date('d F Y H:i',strtotime($transaction->created_at))}}</b> </span> <br>
                <small class="text-muted">Transaksi ID: {{$transaction->no_invoice}} </small>
            </div>
            <div class="col-lg-6 col-md-6 ms-auto text-md-end">
                <select class="form-select d-inline-block mb-lg-0 mr-5 mw-200" id="status">
                    <option value="0" <?php if($transaction->status == 0){
                        echo "selected";
                    } ?>>Belum Diselesaikan</option>
                    <option value="1" <?php if($transaction->status == 1){
                        echo "selected";
                    } ?>>Belum Dibayar</option>
                    <option value="2" <?php if($transaction->status == 2){
                        echo "selected";
                    } ?>>Sudah Dibayar</option>
                    <option value="3" <?php if($transaction->status == 3){
                        echo "selected";
                    } ?>>Sedang Diproses</option>
                    <option value="4" <?php if($transaction->status == 4){
                        echo "selected";
                    } ?>>Sedang Dikirim</option>
                    <option value="5" <?php if($transaction->status == 5){
                        echo "selected";
                    } ?>>Sudah Selesai</option>
                </select>
                <input type="hidden" id="id_trans" value="{{$transaction->id}}" name="">
                <a class="btn btn-primary" href="javascript:void(0)" onclick="Save()">Save</a>
                <a class="btn btn-secondary print ms-2 d-none" href="#"><i class="icon material-icons md-print"></i></a>
            </div>
        </div>
    </header>
    <!-- card-header end// -->
    <div class="card-body">
        <div class="row mb-50 mt-20 order-info-wrap">
            <div class="col-md-4">
                <article class="icontext align-items-start">
                    <span class="icon icon-sm rounded-circle bg-primary-light">
                        <i class="text-primary material-icons md-person"></i>
                    </span>
                    <div class="text">
                        <h6 class="mb-1">Customer</h6>
                        <p class="mb-1">
                            {{$transaction->user_name}}<br>
                            
                            {{$transaction->user_phone}}
                        </p>
                    </div>
                </article>
            </div>
            <!-- col// -->
            <!-- col// -->
            <div class="col-md-4">
                <article class="icontext align-items-start">
                    <span class="icon icon-sm rounded-circle bg-primary-light">
                        <i class="text-primary material-icons md-place"></i>
                    </span>
                    <div class="text">
                        <h6 class="mb-1">Alamat Pengiriman</h6>
                        <p class="mb-1">
                            Kota: {{GetColoumnValue($transaction->id_city,'cities','name')}}, Indonesia <br>{{$transaction->address}} <br>
                            Kode Pos {{$transaction->pos_code}}
                        </p>
                    </div>
                </article>
            </div>
            <!-- col// -->
        </div>
        <!-- row // -->
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table">
                        
                        <tbody>
                            @foreach($data as $c)
                            @for($i =0;$i < count($c->product);$i++)
                            <tr class="pt-20" id="tr{{$c->id}}{{$c->product[$i]->id_cart}}">
                                <td class="image product-thumbnail pt-10 pl-10" style="width: 130px;"><img src="{{$c->product[$i]->image}}" alt="#" style="max-height: 100px;max-width: 110px;"></td>
                                <td class="product-des product-name">
                                    <h6 class="w-160 mb-5"><a href="shop-product-full.html" class="text-heading">{{$c->product[$i]->cattle_name}}</a></h6>
                                    <div class="product-rate-cover">
                                        <div class="">
                                            <span class="font-small text-muted">Gender : 
                                                @if($c->product[$i]->gender == 1)
                                                <span class="text-danger">Jantan</span>
                                                @else
                                                <span class="text-success">Betina</span>
                                                @endif
                                            </span>
                                        </div>
                                        <div class="">
                                            <span class="font-small text-muted">Berat  : {{$c->product[$i]->weigth}} Kg</span>
                                        </div>
                                        <div class="">
                                            <span class="font-small text-muted">Umur   : {{$c->product[$i]->age}} </span>
                                        </div>
                                    </div>   
                                </td>
                                <td class="price" data-title="Price">
                                    <h4 class="text-brand">Rp {{number_format($c->product[$i]->final_price)}} </h4>
                                </td>
                                <td class="action text-center pr-10" data-title="Remove">
                                    @if($c->product[$i]->delivery_type == 1)
                                    Diantar (Rp {{number_format($c->product[$i]->fee_delivery)}}) Max {{$c->product[$i]->max_distance}} Km
                                    @else
                                    Ambil Sendiri di kandang (Free)
                                    @endif
                                </td>
                            </tr>
                            @endfor
                            @endforeach
                            <tr>
                                <td colspan="4">
                                    <article class="float-end">
                                        <dl class="dlist">
                                            <dt>Subtotal:</dt>
                                            <dd>Rp {{number_format($transaction->nominal)}}</dd>
                                        </dl>
                                        <dl class="dlist">
                                            <dt>Diskon:</dt>
                                            <dd>Rp {{number_format($transaction->discount)}}</dd>
                                        </dl>
                                        <dl class="dlist">
                                            <dt>Biaya Pengiriman:</dt>
                                            <dd>Rp {{number_format($transaction->fee_delivery)}}</dd>
                                        </dl>
                                        <dl class="dlist">
                                            <dt>Total:</dt>
                                            <dd><b class="h5">Rp {{number_format($transaction->total_of_pay)}}</b></dd>
                                        </dl>
                                        <dl class="dlist">
                                            <dt class="text-muted">Status:</dt>
                                            <dd>
                                                
                                                @if($transaction->is_valid == 0)
                                                <span class="text-muted">Transaksi Belum Selesai</span>
                                                @else
                                                    @if($transaction->status == 1)
                                                    <span class="badge rounded-pill alert-danger text-danger">Belum Dibayar</span>
                                                    
                                                    @elseif($transaction->status == 2)
                                                    <span class="badge rounded-pill alert-success text-success">Sudah Dibayar</span>
                                                    
                                                    @elseif($transaction->status == 3)
                                                    <span class="badge rounded-pill alert-info text-info">Sedang Diproses</span>
                                                    
                                                    @elseif($transaction->status == 4)
                                                    <span class="badge rounded-pill alert-warning text-warning">Sedang Dikirim</span>
                                                    
                                                    @else
                                                    <span class="badge rounded-pill alert-success text-success">Pesanan Selesai</span>
                                                    
                                                    @endif
                                                @endif
                                            </dd>
                                        </dl>
                                    </article>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!-- table-responsive// -->
            </div>
            <!-- col// -->
            <!-- col// -->
        </div>
    </div>
    <!-- card-body end// -->
</div>
<!-- card end// -->
@include('components/componen_js')
@include('components/js/transaction/index')
@endsection
@extends('layout.app')
@section('asset')
<link rel="stylesheet" type="text/css" href="{{URL::asset('assets')}}/vendors/css/forms/select/select2.min.css">
@endsection
@section('title', 'Administrator')
@section('content')
<section class="multiple-validation" id="detail_edit">
    <div class="row">
        <div class="col-md-12">
            <div class="card mb-3">
                <div class="card-content">
                    <div class="card-body">
                        <form class="form-horizontal p-3" novalidate id="form_edit">@csrf
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <div class="mb-4">
                                        <label for="product_name" class="form-label">Name</label>
                                        <input type="text" name="admin_name" class="form-control form-control-lg" id="admin_name" placeholder="Name" required data-validation-required-message="Nama Wajib diisi" value="{{$data->admin_name}}">
                                        <input type="hidden" name="id" value="{{$data->id}}">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <div class="mb-4">
                                        <label class="form-label">Jenis kelamin</label>
                                        <select class="form-control" required data-validation-required-message="Jenis Kelamin Wajib diisi" name="gender">
                                            <option value="1" <?php if ($data->gender == 1) {
                                                echo "selected";
                                            } ?>> Laki - Laki</option>
                                            <option value="2" <?php if ($data->gender == 2) {
                                                echo "selected";
                                            } ?>> Perempuan</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <div class="mb-4">
                                        <label class="form-label">Email</label>
                                        <input type="email" name="email" class="form-control form-control-lg" id="email" placeholder="Email" required data-validation-required-message="Email Wajib diisi" value="{{$data->email}}">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <div class="mb-4">
                                        <label class="form-label">Phone</label>
                                        <input type="text" name="phone" class="form-control form-control-lg" placeholder="Phone" required data-validation-required-message="Phone Wajib diisi" value="{{$data->phone}}">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <div class="mb-4">
                                        <label class="form-label">Role</label>
                                        <select class="form-control" required data-validation-required-message="Role Wajib diisi" name="id_role" id="role">
                                            <option value="">Select Role</option>
                                            @foreach($data_role as $v)
                                            <option value="{{$v->id}}" <?php if ($data->id_role == $v->id) {
                                                echo "selected";
                                                } ?> >{{$v->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <div class="mb-4">
                                        <label class="form-label">Address</label>
                                        <input value="{{$data->address}}" type="text" name="address" class="form-control form-control-lg" required>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="card h-100">
                                        <div class="card-header text-center">
                                            Foto Profile
                                        </div>
                                        <div class="card-body text-center pb-0">
                                            <div class="input-upload">
                                                @if($data->profile != null)
                                                <img id="profile" src="{{env('BASE_IMG')}}{{$data->profile}}" alt="your image">
                                                @else
                                                <img id="profile" src="{{URL::asset('assets')}}/imgs/theme/upload.svg" alt="your image">
                                                @endif
                                                <label class="btn btn-success btn-sm mt-1 w-100 align-self-center">
                                                Change File <input type="file" name="profile" style="display: none;" onchange="gantiProfile(this);">
                                                </label>
                                            </div>  
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-sm-12 col-xs-12">
                                    <button type="submit" class="btn btn-primary mr-2 float-right" id="save_edit">Simpan Perubahan</button>
                                    <button type="button" class="btn btn-primary mr-2 float-right" style="display: none;" id="loading_edit" disabled>Loading...</button>
                                    <a href="/administrator/list-admin/">
                                    <button type="button" class="btn btn-info float-right" style="margin-right: 10px"><i class="material-icons md-keyboard_return"></i> Kembali ke List Admin</button></a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('components/componen_js')
@include('components/js/admin/edit')
@endsection
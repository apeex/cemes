<!-- Pop Up Edit -->
<section class="multiple-validation" id="detail_edit">
    <div class="row">
        <div class="col-md-12">
            <div class="card mb-3">
                <div class="card-content">
                    <div class="card-body">
                        <form class="form-horizontal p-3" novalidate id="form_edit">
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Name</label>
                                        <input type="text" name="admin_name" class="form-control form-control-lg" id="admin_name" placeholder="Name" required data-validation-required-message="Nama Wajib diisi" value="{{$data->admin_name}}">
                                        <input type="hidden" name="id" value="{{$data->id}}">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Nama Pengirim</label>
                                        <input type="text" name="sender" class="form-control form-control-lg" value="{{$data->sender}}" placeholder="Name" required>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Email</label>
                                        <input type="email" name="email" class="form-control form-control-lg" id="email" placeholder="Email" required data-validation-required-message="Email Wajib diisi" value="{{$data->email}}">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Phone</label>
                                        <input type="text" name="phone" class="form-control form-control-lg" placeholder="Phone" required data-validation-required-message="Phone Wajib diisi" value="{{$data->phone}}">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Role</label>
                                        <select class="form-control" required data-validation-required-message="Role Wajib diisi" name="id_role" id="role">
                                            <option value="">Select Role</option>
                                            @foreach($data_role as $v)
                                            <option value="{{$v->id}}" <?php if ($data->id_role == $v->id) {
                                                echo "selected";
                                                } ?> >{{$v->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Cabang</label>
                                        <select class="form-control" id="select_edit_cabang" name="branch_id" required>
                                            <option value="">Pilih Cabang</option>
                                            @foreach($data_cabang as $c)
                                            @if($c->id == $data->branch_id)
                                            <option value="{{$c->id}}" selected>{{$c->branch_name}}</option>
                                            @else
                                            <option value="{{$c->id}}">{{$c->branch_name}}</option>
                                            @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Address</label>
                                        <input value="{{$data->address}}" type="text" name="address" class="form-control form-control-lg" required>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="card h-100">
                                        <div class="card-header text-center">
                                            Foto Profile
                                        </div>
                                        <div class="card-body text-center">
                                            
                                            <img id="profile" src="{{base_img()}}{{$data->profile}}" alt="your image" style="max-width: 200px;max-height: 200px;" />
                                            <label class="btn btn-white btn-sm mb-0 w-100 align-self-center">
                                                Change File <input type="file" name="profile" style="display: none;" onchange="gantiProfile(this);">
                                            </label>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-xs-12">
                                    <button type="button" class="btn btn-primary mr-2 float-right" id="kirim_edit">Submit</button>
                                    <button type="button" class="btn btn-danger float-right" id="batalkan3" style="margin-right: 10px">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Pop Up Edit -->

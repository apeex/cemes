<!-- Pop Up Edit -->
<section class="multiple-validation" id="detail_edit">
    <div class="row">
        <div class="col-md-12">
            <div class="card mb-5">
                <div class="card-content">
                    <div class="card-body">
                        <form class="form-horizontal p-3" novalidate id="form_edit">
                            <div class="row">
                                <div class="col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Question</label>
                                        <textarea placeholder="Masukan Question" class="form-control form-control-lg required" id="lastName3" name="question" data-validation-required-message="Name Required" required>{{$data->question}}</textarea>
                                        <input type="hidden" name="id" value="{{$data->id}}">
                                    </div>
                                </div>
                               
                               <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="deskripsi" class="label_input">Answer </label>
                                        <textarea placeholder="Masukan Answer" class="form-control form-control-lg required" id="lastName3" name="answer" data-validation-required-message="Name Required" required>{{$data->answer}}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-xs-12">
                                    <button type="button" class="btn btn-primary mr-2 float-right" id="kirim_edit">Submit</button>
                                    <button type="button" class="btn btn-danger float-right" id="batalkan3" style="margin-right: 10px">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Pop Up Edit -->

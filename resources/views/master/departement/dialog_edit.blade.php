<!-- Pop Up Edit -->
<section class="multiple-validation" id="detail_edit">
    <div class="row">
        <div class="col-md-12">
            <div class="card mb-5">
                <div class="card-content">
                    <div class="card-body">
                        <form class="form-horizontal p-3" novalidate id="form_edit">
                            <div class="row">
                                <div class="col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Nama Departement</label>
                                        <input type="text" name="name" class="form-control form-control-lg" id="departement_name" placeholder="Name" required data-validation-required-message="Nama Jurusan Wajib diisi" value="{{$data->departement_name}}">
                                        <input type="hidden" name="id" value="{{$data->id}}">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="card h-100">
                                        <div class="card-header text-center">
                                            Icon
                                        </div>
                                        <div class="card-body text-center">
                                            
                                            <img id="profile" src="{{base_img()}}{{$data->icon}}" alt="your image" style="max-width: 200px;max-height: 200px;" />
                                            <label class="btn btn-white btn-sm mb-0 w-100 align-self-center">
                                                Change File <input type="file" name="profile" style="display: none;" onchange="gantiProfile(this);">
                                            </label>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 mt-5">
                                    <div class="form-group">
                                        <label for="deskripsi" class="label_input">Deskripsi Cabang</label>
                                        <fieldset class="form-label-group mb-0">
                                            <textarea data-length="2000" class="form-control char-textarea active summernote" id="edit_description" rows="3" placeholder="Deskripsi" style="color: rgb(78, 81, 84);" name="">{{$data->description}}</textarea>
                                        </fieldset>
                                        <input type="hidden" name="description" value="" id="isi_deskripsi">
                                    </div>
                                </div>
                               
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-xs-12">
                                    <button type="button" class="btn btn-primary mr-2 float-right" id="kirim_edit">Submit</button>
                                    <button type="button" class="btn btn-danger float-right" id="batalkan3" style="margin-right: 10px">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
var config = {};
config.placeholder = 'some value';
CKEDITOR.replace('edit_description', config); 
var config = {};
config.placeholder = 'some value';
CKEDITOR.replace('edit_description1', config); 
    
</script>
<!-- End Pop Up Edit -->

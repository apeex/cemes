<!-- Pop Up Edit -->
<section class="multiple-validation" id="detail_edit">
    <div class="row">
        <div class="col-md-12">
            <div class="card mb-5">
                <div class="card-content">
                    <div class="card-body">
                        <form class="form-horizontal p-3" novalidate id="form_edit">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group mb-0">
                                        <label>Tingkatan Pendidikan</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="checkbox checkbox-square checkbox-success">
                                        <input type="checkbox" value="1" name="for_s1" <?php if ($data->for_s1 == 1) {
                                            echo "checked";
                                        } ?> >S1
                                        <span></span>
                                    </label>
                                    <label class="checkbox checkbox-square checkbox-success">
                                        <input type="checkbox" name="for_s2" value="1" <?php if ($data->for_s2 == 1) {
                                            echo "checked";
                                        } ?>>S2
                                        <span></span>
                                    </label>
                                    <label class="checkbox checkbox-square checkbox-success">
                                        <input type="checkbox" name="for_s3" value="1" <?php if ($data->for_s3 == 1) {
                                            echo "checked";
                                        } ?>>S3
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Nama Jurusan</label>
                                        <input type="text" name="name" class="form-control form-control-lg" id="name" placeholder="Name" required data-validation-required-message="Nama Jurusan Wajib diisi" value="{{$data->major_name}}">
                                        <input type="hidden" name="id" value="{{$data->id}}">
                                        
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="controls">
                                            <label class="control-label">Departement</label>
                                            <select class="select2 form-control country" required data-validation-required-message="Country Wajib diisi" name="id_departement" >
                                                <option value="">Pilih Departement</option>
                                                @foreach($departement as $v)
                                                @if($data->id_departement == $v->id)
                                                <option value="{{$v->id}}" selected>{{$v->departement_name}}</option>
                                                @else
                                                <option value="{{$v->id}}">{{$v->departement_name}}</option>
                                                @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div> 
                                <div class="col-md-12">
                                    <div class="card h-100">
                                        <div class="card-header text-center">
                                            Icon
                                        </div>
                                        <div class="card-body text-center">
                                            
                                            <img id="profile" src="{{base_img()}}{{$data->icon}}" alt="your image" style="max-width: 200px;max-height: 200px;" />
                                            <label class="btn btn-white btn-sm mb-0 w-100 align-self-center">
                                                Change File <input type="file" name="profile" style="display: none;" onchange="gantiProfile(this);">
                                            </label>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 mt-5">
                                    <div class="form-group">
                                        <label for="deskripsi" class="label_input">Deskripsi Cabang</label>
                                        <fieldset class="form-label-group mb-0">
                                            <textarea data-length="2000" class="form-control char-textarea active summernote" id="edit_description" rows="3" placeholder="Deskripsi" style="color: rgb(78, 81, 84);" name="address">{{$data->description}}</textarea>
                                        </fieldset>
                                        <input type="hidden" name="deskripsi" value="" id="isi_deskripsi">
                                    </div>
                                </div>
                               
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-xs-12">
                                    <button type="button" class="btn btn-primary mr-2 float-right" id="kirim_edit">Submit</button>
                                    <button type="button" class="btn btn-danger float-right" id="batalkan3" style="margin-right: 10px">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
var config = {};
config.placeholder = 'some value';
CKEDITOR.replace('edit_description', config); 
var config = {};
config.placeholder = 'some value';
CKEDITOR.replace('edit_description1', config); 
    
</script>
<!-- End Pop Up Edit -->

@extends('layout.app')
@section('asset')
<link rel="stylesheet" type="text/css" href="{{URL::asset('assets')}}/vendors/css/forms/select/select2.min.css">
@endsection
@section('title', 'Master Banner')
@section('content')
<div class="form_edit_modul" id="show_edit">
</div>
<div class="row" id="show_add" style="display: none;">
    <div class="col-lg-12 col-xxl-12">
        <section class="multiple-validation">
            <div class="card mb-3">
                <div class="card-content">
                    <div class="card-body">
                        <form class="form-horizontal" id="form_add">@csrf
                            <div class="row mb-5">
                                <div class="col-md-6 internal">
                                    <div class="mb-4">
                                        <label>Mengarah ke bagian (ketika di klik)</label>
                                        <select class="form-control" name="type" id="type_internal" onchange="InternalType()" required>
                                            <option value="1">Hewan ternak</option>
                                            <option value="2">Kandang</option>
                                            <option value="3">Makanan</option>
                                            <option value="4">Event</option>
                                        </select>
                                    </div>
                                </div> 
                                <div class="col-md-6 internal">
                                    <div class="mb-4">
                                        <label>Detail Banner</label>
                                        <select class="form-control select2" name="id_detail" id="id_detail" required>
                                            <option value="">-- Pilih --</option>
                                            @foreach($product as $p)
                                            <option value="{{$p->id}}">{{$p->cattle_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div> 
                                <div class="col-md-12">
                                    <div class="card h-100">
                                        <div class="card-header text-center">
                                            Banner (400 X 900)
                                        </div>
                                        <div class="card-body text-center">
                                            
                                            <img id="banner" src="{{URL::asset('assets')}}/imgs/theme/upload.svg" alt="your image" style="" />
                                            <label class="btn btn-success btn-sm mt-1 w-100 align-self-center">
                                                Upload File <input type="file" name="profile" style="display: none;" onchange="ChangeBanner(this);">
                                            </label>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-xs-12">
                                    <button type="submit" class="btn btn-primary float-right" id="save_add">Submit</button>
                                    <button type="button" disabled class="btn btn-primary float-right" id="loading_add" style="display: none;">Loading....</button>
                                    <button type="button" class="btn btn-danger mr-2 float-right" id="batalkan">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 col-xxl-12">
        <!--begin::List Widget 9-->
        <div class="card card-custom card-stretch gutter-b">
            <!--begin::Header-->
            <div class="card-header align-items-center border-0 mt-2">
                @if($role != 'viewer')
                <a class="btn btn-primary font-weight-bolder mr-3" id="tambah">Add Banner</a>
                @endif
            </div>
            <!--end::Header-->
            <!--begin::Body-->
            <div class="card-body pt-2">
                <section id="basic-datatable">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <!-- <div class="card-header">
                                    <h4 class="card-title">List Admin</h4>
                                </div> -->
                                <div class="card-content">
                                    <div class="card-body card-dashboard">
                                        <div class="table-responsive">
                                            <table class="table zero-configuration" id="data_tabel" style="width: 100%">
                                                <thead>
                                                    <tr>
                                                        <th style="width: 5%">no</th>
                                                        <th style="width: 15%">Tipe</th>
                                                        <th style="width: 40%">Banner</th>
                                                        <th style="width: 10%">Status</th>
                                                        <th style="width: 10%">Action</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <!--end: Card Body-->
        </div>
        <!--end: Card-->
        <!--end: List Widget 9-->
    </div>
</div>

@include('components/componen_js')
@include('components/js/master/banner/index')
@endsection
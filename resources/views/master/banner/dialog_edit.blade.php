@extends('layout.app')
@section('asset')
<link rel="stylesheet" type="text/css" href="{{URL::asset('assets')}}/vendors/css/forms/select/select2.min.css">
@endsection
@section('title', 'Master Banner')
@section('content')
<!-- Pop Up Edit -->
<section class="multiple-validation" id="detail_edit">
    <div class="row">
        <div class="col-md-12">
            <div class="card mb-5">
                <div class="card-content">
                    <div class="card-body">
                        <form class="form-horizontal p-3" novalidate id="form_edit">@csrf
                            <div class="row">  
                                <input type="hidden" name="id" value="{{$data->id}}">
                                <div class="col-md-6 internal" style="display: <?php if($data->is_url == '1') { echo "none";} ?>">
                                    <div class="mb-4">
                                        <label>Mengarah ke bagian (ketika di klik)</label>
                                        <select class="form-control" name="type" id="type_internal" onchange="InternalType()" required>
                                            <option value="1" <?php if($data->type == '1') { echo "selected";} ?>>Hewan</option>
                                            <option value="2" <?php if($data->type == '2') { echo "selected";} ?>>Kandang</option>
                                            <option value="3" <?php if($data->type == '3') { echo "selected";} ?>>Makanan</option>
                                            <option value="4" <?php if($data->type == '4') { echo "selected";} ?>>Event</option>
                                        </select>
                                    </div>
                                </div> 
                                <div class="col-md-6 internal">
                                    <div class="mb-4">
                                        <label>Detail Banner</label>
                                        <select class="form-control select2" name="id_detail" id="id_detail" required>
                                            <option value="">-- Pilih --</option>
                                            @foreach($product as $p)
                                            <option value="{{$p->id}}" <?php if($data->id_detail == $p->id) { echo "selected";} ?>>{{$p->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div> 
                                <div class="col-md-12">
                                    <div class="card h-100">
                                        <div class="card-header text-center">
                                            Banner (400 X 900)
                                        </div>
                                        <div class="card-body text-center">
                                            @if($data->photo != null)
                                            <img id="banner" src="{{env('BASE_IMG')}}{{$data->photo}}" alt="your image" style="" />
                                            @else
                                            <img id="banner" src="{{URL::asset('assets')}}/imgs/theme/upload.svg" alt="your image" style="" />
                                            @endif
                                            <label class="btn btn-success btn-sm mt-1 w-100 align-self-center">
                                                Upload File <input type="file" name="profile" style="display: none;" onchange="ChangeBanner(this);">
                                            </label>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-20">
                                <div class="col-sm-12 col-xs-12">
                                    <button type="submit" class="btn btn-primary mr-2 float-right" id="save_edit">Simpan Perubahan</button>
                                    <button type="button" class="btn btn-primary mr-2 float-right" style="display: none;" id="loading_edit" disabled>Loading...</button>
                                    <a href="/master/banner">
                                    <button type="button" class="btn btn-info float-right" style="margin-right: 10px"><i class="material-icons md-keyboard_return"></i> Kembali ke List Banner</button></a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('components/componen_js')
@include('components/js/master/banner/edit')
<!-- End Pop Up Edit -->
@endsection
@extends('layout.app')
@section('asset')
<link rel="stylesheet" type="text/css" href="{{URL::asset('assets')}}/vendors/css/forms/select/select2.min.css">
@endsection
@section('title', 'Master Province')
@section('content')
<!-- Pop Up Edit -->
<section class="multiple-validation" id="detail_edit">
    <div class="row">
        <div class="col-md-12">
            <div class="card mb-5">
                <div class="card-content">
                    <div class="card-body">
                        <form class="form-horizontal" novalidate id="form_edit">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Name</label>
                                        <input type="text" class="form-control form-control-lg required" id="lastName3" name="name" value="{{$data->name}}">
                                        <input type="hidden" name="id" value="{{$data->id}}">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="controls">
                                            <label class="control-label">Country</label>
                                            <select class="select2 form-control" required data-validation-required-message="Role Wajib diisi" name="country_id">
                                                @foreach($data_country as $v)
                                                <option value="{{$v->id}}" <?php if ($v->id == $data->country_id) { echo "selected";} ?> >{{$v->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-20">
                                <div class="col-sm-12 col-xs-12">
                                    <button type="submit" class="btn btn-primary mr-2 float-right" id="save_edit">Simpan Perubahan</button>
                                    <button type="button" class="btn btn-primary mr-2 float-right" style="display: none;" id="loading_edit" disabled>Loading...</button>
                                    <a href="/master/provinsi">
                                    <button type="button" class="btn btn-info float-right" style="margin-right: 10px"><i class="material-icons md-keyboard_return"></i> Kembali ke List Provinsi</button></a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('components/componen_js')
@include('components/js/master/province/edit')
@endsection
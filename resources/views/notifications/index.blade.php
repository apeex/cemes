@extends('layout.app')
@section('asset')
<link rel="stylesheet" type="text/css" href="{{URL::asset('assets')}}/vendors/css/forms/select/select2.min.css">
@endsection
@section('title', 'Notifikasi')
@section('content')
<div class="form_edit_modul" id="show_edit">
</div>
<div class="row" id="show_add" style="display: none;">
    <div class="col-lg-12">
        <section class="multiple-validation">
            <div class="card mb-3">
                <div class="card-content">
                    <div class="card-body">
                        <form class="form-horizontal" enctype="multipart/form-data" id="form_add">@csrf
                            <div class="row mb-5">
                                <div class="col-md-6">
                                    <div class="mb-4">
                                        <label>Tipe Notifikasi</label>
                                        <select class="form-control select2" name="jenis_notifikasi" id="jenis_notifikasi">
                                            <option value="1" selected>Umum </option>
                                            <option value="2">Khusus Beberapa User</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mb-4">
                                        <label>Judul</label>
                                        <input type="text" class="form-control required" id="lastName3" name="title" required />
                                    </div>
                                </div> 
                                <div class="col-md-6">
                                    <div class="mb-4">
                                        <label>Jenis Notifikasi</label>
                                        <select class="form-control select2" id="type_internal" onchange="InternalType()" name="tipe">
                                            <option value="1">Pakan</option>
                                            <option value="2">Kandang</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mb-4">
                                        <label>Detail Notifikasi</label>
                                        <select class="form-control select2" name="id_detail" id="id_detail" required>
                                            <option value="">-- Pilih --</option>
                                            @foreach($data as $p)
                                            <option value="{{$p->id}}">{{$p->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div> 
                                <div class="col-md-12" id="member_khusus" style="display: none">
                                    <div class="mb-4">
                                        <label for="firstName3">
                                           Search Nama Member (miniimal 3 karakter)
                                        </label>
                                        <input type="text" class="form-control" id="searching_user">
                                    </div>
                                </div> 
                                <div id="loading_searching" class="text-center col-md-12" style="display: none">
                                    <span class="text-success">Loading........</span>
                                </div>  
                                <div class="col-md-12" id="data_member">
                                    
                                </div> 
                                <div class="col-md-12" id="member_terpilih" style="display: none">
                                    <label>Member Terpilih</label>
                                    <div class="row" id="user_terpilih">
                                    </div>
                                </div>     
                                <div class="col-md-12">
                                    <div class="mb-4">
                                        <label for="deskripsi" class="label_input">Notifikasi</label>
                                        <fieldset class="form-label-group mb-0">
                                            <textarea data-length="2000" class="form-control char-textarea active summernote" id="description" rows="3" placeholder="Deskripsi" style="color: rgb(78, 81, 84);" name="address"></textarea>
                                        </fieldset>
                                        <input type="hidden" name="deskripsi" value="" id="isi_deskripsi">
                                    </div>
                                </div>
                                <div class="col-md-12 mt-5">
                                    <div class="mb-4">
                                        <label>Image</label>
                                        <input type="file" class="form-control-file" name="image" id="inputfoto" onchange="readURL(this);">
                                        <img id="blah" src="#" alt="your image" style="max-width: 200px;max-height: 200px;display: none" />
                                    </div>
                                </div>
                            </div>
                            <div class="container mt-5">
                                <div class="col-sm-12 col-xs-12">
                                    <button type="button" disabled class="btn btn-primary float-right" style="display: none" id="is_loading">Loading.....</button>
                                    <button type="submit" class="btn btn-primary float-right" id="is_submit">Submit</button>
                                    <button type="button" class="btn btn-danger mr-2 float-right" id="batalkan">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<div class="row" id="hide_add">
    <div class="col-lg-12">
        <!--begin::List Widget 9-->
        <div class="card card-custom card-stretch gutter-b">
            <!--begin::Header-->
            <div class="card-header border-0 mt-2">
                <form id="form_filter" enctype="multipart/form-data" style="display: contents;">@csrf
                    <div class="row">
                        <div class="col-md-3">
                            <select class="select2 form-control" onchange="data_tabel('data_notifikasi')" name="tipe" id="tipe">
                                <option value="">--Semua Tipe--</option>
                                <option value="1">Pakan</option>
                                <option value="2">Kandang</option>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <select class="select2 form-control" onchange="data_tabel('data_notifikasi')" name="id_user" id="id_user">
                                <option value="">--Umum--</option>
                                @foreach($user as $u)
                                <option value="{{$u->id}}">{{$u->user_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-5">
                            <input type="text" placeholder="Search" class="form-control form-control-lg" id="search" onkeyup="data_tabel('data_notifikasi')" name="search">
                        </div>
                        <div class="col-md-1">
                            <button class="btn btn-md btn-danger" style="width:100%" onclick="Reset()">Reset</button>
                        </div>
                    </div>
                </form>
            </div>
            <!--end::Header-->
            <!--begin::Body-->
            <div class="card-body pt-2">
                <section id="basic-datatable">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <!-- <div class="card-header">
                                    <h4 class="card-title">List Admin</h4>
                                </div> -->
                                <div class="card-content">
                                    <div class="card-body card-dashboard">
                                        <div class="table-responsive">
                                            <table class="table zero-configuration" id="data_tabel">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Judul</th>
                                                        <th>User</th>
                                                        <th>Notifikasi</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <!--end: Card Body-->
        </div>
        <!--end: Card-->
        <!--end: List Widget 9-->
    <!-- </div> -->
</div>
@include('components/componen_js')
@include('components/js/notifications/index')
@endsection
@extends('layout.app')
@section('asset')
<link rel="stylesheet" type="text/css" href="{{URL::asset('assets')}}/vendors/css/forms/select/select2.min.css">
@endsection
@section('title', 'Detail Kandang')
@section('content')
<!--  main page// -->
    <div class="content-header">
        <a href="javascript:history.back()"><i class="material-icons md-arrow_back"></i> Go back </a>
    </div>
    <div class="card mb-4">
        <div class="card-header bg-brand-2" style="height: 150px"></div>
        <div class="card-body">
            <div class="row">
                <div class="col-xl col-lg flex-grow-0" style="flex-basis: 230px">
                    <div class="img-thumbnail shadow w-100 bg-white position-relative text-center" style="height: 190px; width: 200px; margin-top: -120px">
                        @if($data->icon != null)
                        <img src="{{env('BASE_IMG')}}{{$data->icon}}" class="center-xy img-fluid" alt="Logo Brand" style="height: 190px;">
                        @else
                        <img src="{{URL::asset('assets')}}/imgs/items/1.jpg" class="center-xy img-fluid" alt="Logo Brand">
                        @endif
                    </div>
                </div>
                <!--  col.// -->
                <div class="col-xl col-lg">
                    <h3>{{$data->food_name}} - {{$data->food_id}}</h3>
                    <p><i class="fas fa-hanukiah ml-2"></i> {{GetColoumnValue($data->id_category,'t_food_categories','category_name')}}</p>
                </div>
                <!--  col.// -->
                <div class="col-xl-4 text-md-end">
                    
                </div>
                <!--  col.// -->
            </div>
            <!-- card-body.// -->
            <hr class="my-4">
            <div class="row g-4">
                <div class="col-md-3">
                    <article class="box">
                        <p class="mb-0 text-muted">Stock Awal:</p>
                        <h5 class="text-success">{{$data->stock}} Kg</h5>
                        <p class="mb-0 text-muted">Stock Saat Ini:</p>
                        <h5 class="text-success mb-0">{{$data->available_stock}} Kg</h5>
                    </article>
                </div>
                <!--  col.// -->
                <div class="col-md-5">
                    <h6>Data Pakan</h6>
                    <p>
                        ID Pakan         : {{$data->food_id }}<br>
                        Nama Pakan       : {{$data->food_name}}<br>
                        Harga Pakan      : Rp {{number_format($data->capital_price)}}<br>
                        Tanggal Input    : {{$data->date}}<br>
                    </p>
                </div>
                <!--  col.// -->
                <div class="col-sm-6 col-lg-4 col-xl-3">
                    <h6>Data Pemilik Pakan</h6>
                    <p>
                        email          : {{$data->user_name}} <br>
                        email          : {{$data->user_email}} <br>
                        phone          : {{$data->user_phone}}<br>
                        Jenis kelamin  : {{$data->gender}}<br>
                        Tanggal Lahir  : {{$data->birthday}} <br>
                        No Nik         : {{$data->user_nik}}<br>
                    </p>
                </div>
                <!--  col.// -->
                <div class="col-sm-6 col-xl-4 text-xl-end">
                    
                </div>
                <!--  col.// -->
            </div>
            <!--  row.// -->
        </div>
        <!--  card-body.// -->
    </div>
<div class="row">
    <div class="">
        <!--begin::List Widget 9-->
        <div class="card card-custom card-stretch gutter-b">
            <!--begin::Header-->
            <div class="p-4">
                <form id="form_filter">
                    <div class="row">
                        <div class="col-md-4">
                            <label>Tanggal Pemberian Pakan</label>
                            <input type="date" id="tanggal" onchange="DataTable()" name="tanggal" class="form-control">
                        </div>
                        <input type="hidden" name="id_farmer" id="id_farmer" value="">
                        <input type="hidden" name="id_food" id="id_food" value="{{$data->id}}">
                        <div class="col-md-4">
                            <label>Pilih Kandang</label>
                            <select class="form-control select2" name="id_shed" id="id_shed" onchange="DataTable()">
                                <option value="" selected>Semua kandang </option>
                                @foreach($shed as $k=>$v)
                                <option value="{{$v->id}}">{{$v->shed_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>  
                </form>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive m-t-40">
                                <table id="data_tabel" class="table display table-bordered table-striped no-wrap">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Pakan</th>
                                            <th>Kandang</th>
                                            <th>Jumlah</th>
                                            <th>Tanggal</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('components/componen_js')
@include('components/js/food/history/index')
@endsection
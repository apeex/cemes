@extends('layout.app')
@section('asset')
<link rel="stylesheet" type="text/css" href="{{URL::asset('assets')}}/vendors/css/forms/select/select2.min.css">
@endsection
@section('title', 'Administrator')
@section('content')
<div class="row">
    <div class="">
        <!--begin::List Widget 9-->
        <div class="card card-custom card-stretch gutter-b">
            <!--begin::Header-->
            <div class="p-4">
                <form id="form_filter">
                    <div class="row">
                        <div class="col-md-3">
                            <label>Tanggal Pindah</label>
                            <input type="date" id="tanggal" onchange="DataTable()" name="tanggal" class="form-control">
                        </div>
                        <div class="col-md-3">
                            <label>Pilih Peternak</label>
                            <select class="form-control select2" name="id_farmer" id="id_farmer" onchange="Choosefarmer()">
                                <option value="" selected>Semua Peternak </option>
                                @foreach($farmer as $k=>$v)
                                <option value="{{$v->id}}">{{$v->user_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label>Pilih Pakan</label>
                            <select class="form-control select2" name="id_food" id="id_food" onchange="DataTable()">
                                <option value="" selected>Semua Pakan </option>
                                
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label>Pilih Kandang</label>
                            <select class="form-control select2" name="id_shed" id="id_shed" onchange="DataTable()">
                                <option value="" selected>Semua Kandang </option>
                            </select>
                        </div>
                    </div>  
                </form>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive m-t-40">
                                <table id="data_tabel" class="table display table-bordered table-striped no-wrap">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Pakan</th>
                                            <th>Kandang</th>
                                            <th>Jumlah</th>
                                            <th>Tanggal</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('components/componen_js')
@include('components/js/food/history/index')
@endsection
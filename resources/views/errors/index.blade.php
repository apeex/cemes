@extends('layout.app')
@section('title', 'Log Admin')
@section('content')
<div class="row mt-60">
    <div class="col-sm-12">
        <div class="w-50 mx-auto text-center">
            <img src="{{URL::asset('assets')}}/imgs/theme/404.png" width="350" alt="Page Not Found">
            <h3 class="mt-40 mb-15">Oops! Mohon Maaf</h3>
            <p>System Sedang Dalam Perbaikan. Harap ditunggu....</p>
            <a href="/dashboard" class="btn btn-primary mt-4"><i class="material-icons md-keyboard_return"></i> Back to Dasboard</a>
        </div>
    </div>
</div>
@include('components/componen_js')
@endsection

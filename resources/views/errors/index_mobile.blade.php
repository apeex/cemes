<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Admin Panel SOUQ</title>
        <meta http-equiv="x-ua-compatible" content="ie=edge" />
        <meta name="description" content="" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta property="og:title" content="" />
        <meta property="og:type" content="" />
        <meta property="og:url" content="" />
        <meta property="og:image" content="" />
        <!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="{{URL::asset('assets')}}/imgs/favicon.png" />
        <!-- Template CSS -->
        <link href="{{URL::asset('assets')}}/css/main.css?v=1.1" rel="stylesheet" type="text/css" />
    </head>

    <body>
        <div class="row mt-60">
            <div class="col-sm-12">
                <div class="w-50 mx-auto text-center">
                    <img src="{{URL::asset('assets')}}/imgs/theme/404.png" width="350" alt="Page Not Found">
                    <h3 class="mt-40 mb-15">Oops! Mohon Maaf</h3>
                    <p>System Sedang Dalam Tahap Pengembangan. Harap ditunggu....</p>
                </div>
            </div>
        </div>
    </body>
</html>
<aside class="navbar-aside" id="offcanvas_aside">
    <div class="aside-top">
        <a href="javascript:void(0)" class="brand-wrap">
            <img src="{{URL::asset('assets')}}/imgs/icon.png" class="logo" alt="RA Dashboard" />
        </a>
        <div>
            <button class="btn btn-icon btn-aside-minimize"><i class="text-muted material-icons md-menu_open"></i></button>
        </div>
    </div>
    <nav>
        <ul class="menu-aside">
            @foreach($menu ?? '' as $k => $v)
                @if(HasSubmenu($v->id) == 1)
                <li class="menu-item has-submenu <?php if ($v->slug == Request::segment(1) ) {echo 'active';} ?>">
                    <a class="menu-link" href="javascript:void(0)">
                        <i class="icon {{$v->icon}}"></i>
                        <span class="text">{{$v->name}}</span>
                    </a>
                    <div class="submenu" style="display: <?php if ($v->slug != Request::segment(1) ) {echo 'none';} ?>">
                        @foreach($submenu as $a => $s)
                            <?php if ($v->menu_id == $s->parent_menu_id) { ?>
                                <a href="/{{$v->slug}}/{{$s->slug}}" class="<?php if ($s->slug == Request::segment(2) ) {echo 'active';} ?>">{{$s->name}}</a>
                            <?php } ?>
                        @endforeach
                    </div>
                </li>
                @else
                    <li class="menu-item <?php if ($v->slug == Request::segment(1) ) {echo 'active';} ?>">
                        <a class="menu-link" href="/{{$v->slug}}/">
                            <i class="icon {{$v->icon}}"></i>
                            <span class="text">{{$v->name}}</span>
                        </a>
                    </li>
                @endif
            @endforeach
        </ul>
        <br />
        <br />
    </nav>
    <div class="text-center">
        <table border="0">
            <tr>
                <td style="text-align:right;width: 50%;vertical-align: middle;"><h5>By</h5></td>
                <td style="text-align: left;width: 50%"><a href="https://trendcas.com/" target="_blank"> <img src="https://trendcas.com/wp-content/uploads/2021/03/logo.png" style="width: 100px;" class="logo" alt="RA Dashboard"></a></td>
            </tr>
        </table>
    </div>
</aside>
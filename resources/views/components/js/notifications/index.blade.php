<script src="{{URL::asset('assets')}}/vendors/js/tables/datatable/datatables.min.js"></script>
<script src="{{URL::asset('assets')}}/vendors/js/tables/datatable/datatables.buttons.min.js"></script>
<script src="{{URL::asset('assets')}}/vendors/js/tables/datatable/buttons.print.min.js"></script>
<script src="{{URL::asset('assets')}}/vendors/js/tables/datatable/buttons.bootstrap.min.js"></script>
<script src="{{URL::asset('assets')}}/vendors/js/tables/datatable/datatables.bootstrap4.min.js"></script>
<script src="{{URL::asset('assets')}}/js/scripts/datatables/datatable.js"></script>
<script src="{{URL::asset('assets')}}/vendors/js/forms/select/select2.full.min.js"></script>
<script src="{{URL::asset('assets')}}/js/scripts/forms/select/form-select2.js"></script>
<script src="{{URL::asset('assets')}}/js/validate.js"></script>
<script src="{{URL::asset('assets')}}/js/additional-method.js"></script>
<script src="https://cdn.ckeditor.com/4.13.1/full/ckeditor.js"></script>
<script type="text/javascript">
var columns = [{
        "data": null,
        "sortable": false,
        render: function(data, type, row, meta) {
            return meta.row + meta.settings._iDisplayStart + 1;
        }
    },
    { "data": "title" },
    { "data": "name" },
    { "data": "text" },
    { "data": "actions" }
];

function data_tabel(tabel) {

    if (tabel == 'data_notifikasi') {
        //alert("bangsattt");
        id_user     = $("#id_user").val();
        tipe        = $("#tipe").val();
        search      = $("#search").val();
        var xin_table = $('#data_tabel').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": '/data_notifikasi',
                "dataType": "json",
                "type": "POST",
                "data": { _token: "{{csrf_token()}}",id_user:id_user,tipe:tipe,search:search }
            },
            "columns": columns,
            "bDestroy": true
        });
        $("#data_tabel_filter").html('<button class="btn btn-lg btn-block btn-success" type="button" onclick="Tambah()"><i class="fas fa-plus"></i> Tambah Notifikasi Baru</button>');
    }
}
$(function() {
    //$('#myTable').DataTable();
    $(".select2-container--default").css('width', '100%');
    data_tabel('data_notifikasi')
    //alert("tabel");
    var config = {};
    config.placeholder = 'some value';
    CKEDITOR.replace('description', config);
});

function Tambah(){
    // alert('tes')
    $("#hide_add").css('display', 'none');
    $("#show_add").css('display', '');
}
$('#batalkan').on("click", function() {
    $("#hide_add").css('display', '');
    $("#show_add").css('display', 'none');
});

$("#form_add").validate({
    submitHandler: function(form) {
       var description = CKEDITOR.instances.description.getData();
        $("#isi_deskripsi").val(description);
        $("#is_submit").css('display','none');
        $("#is_loading").css('display','');
        $.ajax({ //line 28
            type: 'POST',
            url: '/post_notifikasi',
            dataType: 'json',
            data: new FormData($("#form_add")[0]),
            processData: false,
            contentType: false,
            success: function(data) {
                if (data.code == 200) {
                    document.getElementById("form_add").reset();
                    $("#hide_add").css('display', '');
                    $("#show_add").css('display', 'none');
                    $("#message").remove();
                    show_toast(data.message, 1);
                    data_tabel('data_notifikasi');
                    $("#is_submit").css('display','');
                    $("#is_loading").css('display','none');
                } else {
                    alert("maaf ada yang salah!!!");
                }
                CKEDITOR.instances.description.setData(''); // ckeditor reset
            }
        });
    }
});

// batalkan edit
$(document).off('click', '#batalkan3').on('click', '#batalkan3', function() {

    $("#titel_head").remove();
    $("#head_modul").append('<span id="titel_head">Tambah Data City</span>');
    $("#hide_add").css('display', '');
    $("#show_edit").css('display', 'none');
});

$(document).off('change', '#jenis_notifikasi').on('change', '#jenis_notifikasi', function() {
    id = $(this).val();
    if (id == 2) {
       $("#member_khusus").css('display','');
       $("#member_terpilih").css('display','');
    } else {
       $("#member_khusus").css('display','none');
       $("#member_terpilih").css('display','none');
    }
});

$(document).off('click', '.hapus-row').on('click', '.hapus-row', function() {
    $(this).closest('.row').remove();

    row_number = $(this).attr('row-number');
    number_of_row(row_number);
});

$("#searching_user").keyup(function(){
   text = $("#searching_user").val();
    if ( text.length > 2 ){
        $("#loading_searching").css('display','');
        $.ajax({
            url: '/search_user/'+text,
            type: "GET",
            success: function(response) {
                console.log(response);
                if (response) {
                    $("#data_member").html(response);
                    $("#loading_searching").css('display','none');
                }
            }
        });
    }
});

$(document).off('click', '.pilih_user').on('click', '.pilih_user', function() { 
    no = $(this).attr('no');
    nama = $(this).attr('nama');
    $("#div_pilih_user"+no).remove();
    $("#user_terpilih").append('<div class="checkbox-inline col-md-3 pb-5"><label class="checkbox checkbox-square checkbox-primary"><input type="checkbox" class="pilih_user" name="subjek[]" value="'+no+'" checked>'+nama+'<span></span></label></div>');
});

function InternalType(){
    id = $("#type_internal").val();
    $.ajax({
        type: 'GET',
        url: '/get_type_notification/'+id,
        dataType: 'json',
        success: function(data) {
            console.log(data)
            $("#id_detail").empty();
            $("#id_detail").empty().append("<option value=''>--Pilih--</option>");
            for (let i = 0; i < data.length; i++) {
                $("#id_detail").append("<option value=" + data[i].id + ">" + data[i].name + "</option>");
            }
        },
        error: function(data) {
            console.log(data);
        }
    });
}
</script>
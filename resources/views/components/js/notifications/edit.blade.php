<script src="{{URL::asset('assets')}}/js/validate.js"></script>
<script src="{{URL::asset('assets')}}/js/additional-method.js"></script>
<script src="{{URL::asset('assets')}}/vendors/js/forms/select/select2.full.min.js"></script>
<script src="{{URL::asset('assets')}}/js/scripts/forms/select/form-select2.js"></script>
<script src="https://cdn.ckeditor.com/4.13.1/full/ckeditor.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAvTkPKa1jErT_Kh9ZPTIP2az48f8y0WGo&libraries=places"></script>
<script>
    function initialize() {
        $("#search_lokasi").attr("placeholder","Alamat");
        var input = document.getElementById('search_lokasi');
        var autocomplete = new google.maps.places.Autocomplete(input);
        google.maps.event.addListener(autocomplete, 'place_changed',
            function() {
                var place = autocomplete.getPlace();
                var lat = place.geometry.location.lat();
                var lng = place.geometry.location.lng();
                document.getElementById('latitude').value = lat;
                document.getElementById('longitude').value = lng;

            }
        );
    }
    google.maps.event.addDomListener(window, 'load', initialize);
    $(document).ready(function(){
        $('.select2').select2();
    })

    $(document).ready(function(){
        $('.select2').select2();
    })

    var config = {};
    config.placeholder = 'some value';
    CKEDITOR.replace('description', config);
</script>
<script type="text/javascript">
$('.select2').on('change', function() { // when the value changes
    $(this).valid(); // trigger validation on this element
});

$('#tambah').on("click", function() {
    // alert('tes')
    $(window).scrollTop(0);
    $("#show_add").css('display', '');
});
$('#batalkan').on("click", function() {
    $("#show_add").css('display', 'none');
});

$("#form_edit").validate({
    rules: {
        password: {
            minlength: 6
        },
        confirm_password: {
            equalTo: "#password"
        }
    },
    messages: {
        password: {
            minlength: "password minimal 6 character"
        },
        confirm_password: {
            equalTo: "password not match"
        }
    },
    submitHandler: function(form) {
        $("#loading_edit").css('display', '');
        $("#save_edit").css('display', 'none');
        var text = CKEDITOR.instances.description.getData();
        $("#isi_deskripsi").val(text);
        $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
        });
        $.ajax({ //line 28
            type: 'POST',
            url: '/update/majelis',
            dataType: 'json',
            data: new FormData($("#form_edit")[0]),
            processData: false,
            contentType: false,
            success: function(data) {
                $("#save_edit").css('display', '');
                $("#loading_edit").css('display', 'none');
                if (data.code == 200) {
                    show_toast(data.message, 1);
                } else {
                    show_toast(data.message, 2);
                }
            }
        });
    }
});
function gantiProfile(input) {
    //alert("okey");
    var $source = $('#video_here');
    $source[0].src = URL.createObjectURL(input.files[0]);
    $source.parent()[0].load();
}
// is link
$(document).off('click', '#is_link').on('click', '#is_link', function() {
    if ($("#is_link").prop('checked') == true) {
        $("#link").css('display', '');
        $("#upload").css('display', 'none');

    } else {
        $("#link").css('display', 'none');
        $("#upload").css('display', '');
    }

});
</script>
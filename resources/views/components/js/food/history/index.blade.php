<script src="{{URL::asset('assets')}}/vendors/js/tables/datatable/datatables.min.js"></script>
<script src="{{URL::asset('assets')}}/vendors/js/tables/datatable/datatables.buttons.min.js"></script>
<script src="{{URL::asset('assets')}}/vendors/js/tables/datatable/buttons.print.min.js"></script>
<script src="{{URL::asset('assets')}}/vendors/js/tables/datatable/buttons.bootstrap.min.js"></script>
<script src="{{URL::asset('assets')}}/vendors/js/tables/datatable/datatables.bootstrap4.min.js"></script>
<script src="{{URL::asset('assets')}}/js/scripts/datatables/datatable.js"></script>
<script src="{{URL::asset('assets')}}/vendors/js/forms/select/select2.full.min.js"></script>
<script src="{{URL::asset('assets')}}/js/scripts/forms/select/form-select2.js"></script>
<script src="{{URL::asset('assets')}}/js/validate.js"></script>
<script src="{{URL::asset('assets')}}/js/additional-method.js"></script>
<script type="text/javascript">
var column = [
        { "data": "no" },
        { "data": "food" },
        { "data": "shed" },
        { "data": "weigth" },
        { "data": "date" },
        { "data": "action" },
    ];

function data_tabel(tabel) {
    var tanggal   = $("#tanggal").val();
    var id_shed   = $("#id_shed").val();
    var id_food   = $("#id_food").val();
    var id_farmer = $("#id_farmer").val();
    
    if (tabel == 'data_food_history') {
        var xin_table = $('#data_tabel').DataTable({
            "processing": true,
            "serverSide": true,
            "orderable": false,
            "targets": 'no-sort',
            "bSort": false,
            "bFilter": true,
            "ajax": {
                "url": '/data_food_history',
                "dataType": "json",
                "type": "POST",
                "data": { _token: "{{csrf_token()}}",tanggal :tanggal,id_shed:id_shed,id_food:id_food,id_farmer:id_farmer }
            },
            "columns": column,
            "bDestroy": true
        });
        //$("#data_tabel_length").remove();
    }
    
}
$(function() {
    // $('#yourTable').DataTable();
    $(".select2-container--default").css('width', '100%');
    data_tabel('data_food_history')
});

function DataTable() {
    data_tabel('data_food_history')
}

function Choosefarmer(){
    data_tabel('data_food_history')
    id = $("#id_farmer").val()
    alert(id);
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'POST',
        url: '/change_farmer',
        dataType: 'json',
        data: { id:id },
        success: function(data) {
            console.log(data)
            $("#id_shed").empty();
            $("#id_food").empty();
            $("#id_shed").append("<option value=''>Semua Kandang</option>");
            $("#id_food").append("<option value=''>Semua Pakan</option>");
            for (let i = 0; i < data.shed.length; i++) {
                $("#id_shed").append("<option value=" + data.shed[i].id + ">" + data.shed[i].name + "</option>");
            }

            for (let a = 0; a < data.food.length; a++) {
                $("#id_food").append("<option value=" + data.food[a].id + ">" + data.food[a].name + "</option>");
            }
        },
        error: function(data) {
            console.log(data);
        }
    });
    
}
function detail_pakan(id){
    //alert(id);
    $("#modal_detail").modal('show');
    $("#modal_detail_title").html("Detail pakan per kandang");
    $.ajax({
        type: 'GET',
        url: '/detail_food/'+id,
        success: function(data) {
            //console.log(data)
            $("#modal_detail_isi").html(data);
        },
        error: function(data) {
            console.log(data);
        }
    });
}
</script>
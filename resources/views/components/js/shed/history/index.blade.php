<script src="{{URL::asset('assets')}}/vendors/js/tables/datatable/datatables.min.js"></script>
<script src="{{URL::asset('assets')}}/vendors/js/tables/datatable/datatables.buttons.min.js"></script>
<script src="{{URL::asset('assets')}}/vendors/js/tables/datatable/buttons.print.min.js"></script>
<script src="{{URL::asset('assets')}}/vendors/js/tables/datatable/buttons.bootstrap.min.js"></script>
<script src="{{URL::asset('assets')}}/vendors/js/tables/datatable/datatables.bootstrap4.min.js"></script>
<script src="{{URL::asset('assets')}}/js/scripts/datatables/datatable.js"></script>
<script src="{{URL::asset('assets')}}/vendors/js/forms/select/select2.full.min.js"></script>
<script src="{{URL::asset('assets')}}/js/scripts/forms/select/form-select2.js"></script>
<script src="{{URL::asset('assets')}}/js/validate.js"></script>
<script src="{{URL::asset('assets')}}/js/additional-method.js"></script>
<script type="text/javascript">
var column = [
        { "data": "no" },
        { "data": "name" },
        { "data": "old_shed" },
        { "data": "new_shed" },
        { "data": "date" },
        { "data": "description" },
    ];

function data_tabel(tabel) {
    var tanggal  = $("#tanggal").val();
    var old_shed = $("#old_shed").val();
    var new_shed = $("#new_shed").val();
    
    if (tabel == 'data_shed_history') {
        var xin_table = $('#data_tabel').DataTable({
            "processing": true,
            "serverSide": true,
            "orderable": false,
            "targets": 'no-sort',
            "bSort": false,
            "bFilter": true,
            "ajax": {
                "url": '/data_shed_history',
                "dataType": "json",
                "type": "POST",
                "data": { _token: "{{csrf_token()}}",tanggal :tanggal,old_shed:old_shed,new_shed:new_shed }
            },
            "columns": column,
            "bDestroy": true
        });
        //$("#data_tabel_length").remove();
    }
    
}
$(function() {
    // $('#yourTable').DataTable();
    $(".select2-container--default").css('width', '100%');
    data_tabel('data_shed_history')
});

function DataTable() {
    data_tabel('data_shed_history')
}
</script>
<script src="{{URL::asset('assets')}}/vendors/js/tables/datatable/datatables.min.js"></script>
<script src="{{URL::asset('assets')}}/vendors/js/tables/datatable/datatables.buttons.min.js"></script>
<script src="{{URL::asset('assets')}}/vendors/js/tables/datatable/buttons.print.min.js"></script>
<script src="{{URL::asset('assets')}}/vendors/js/tables/datatable/buttons.bootstrap.min.js"></script>
<script src="{{URL::asset('assets')}}/vendors/js/tables/datatable/datatables.bootstrap4.min.js"></script>
<script src="{{URL::asset('assets')}}/js/scripts/datatables/datatable.js"></script>
<script src="{{URL::asset('assets')}}/vendors/js/forms/select/select2.full.min.js"></script>
<script src="{{URL::asset('assets')}}/js/scripts/forms/select/form-select2.js"></script>
<script src="{{URL::asset('assets')}}/vendors/js/forms/select/select2.full.min.js"></script>
<script src="{{URL::asset('assets')}}/js/scripts/forms/select/form-select2.js"></script>
<script src="{{URL::asset('assets')}}/js/validate.js"></script>
<script src="{{URL::asset('assets')}}/js/additional-method.js"></script>
<script type="text/javascript">

var column_pakan = [
    { "data": "no" },
    { "data": "food" },
    { "data": "shed" },
    { "data": "weigth" },
    { "data": "date" },
];

var column_berat = [
    { "data": "no" },
    { "data": "shed" },
    { "data": "weigth" },
    { "data": "date" },
];

var column_kandang = [
    { "data": "no" },
    { "data": "old_shed" },
    { "data": "new_shed" },
    { "data": "date" },
    { "data": "description" },
];

var id_cattle = $("#id_cattle").val();

function DataPakan() {
    var tanggal = $("#tanggal").val();
    var id_food = $("#id_food").val();
    
    var xin_table = $('#data_pakan').DataTable({
        "processing": true,
        "serverSide": true,
        "orderable": false,
        "targets": 'no-sort',
        "bSort": false,
        "bFilter": true,
        "ajax": {
            "url": '/cattle/data_food_history',
            "dataType": "json",
            "type": "POST",
            "data": { _token: "{{csrf_token()}}",tanggal:tanggal,id_food:id_food,id_cattle:id_cattle }
        },
        "columns": column_pakan,
        "bDestroy": true
    });
    
}

function DataBerat() {
    var tanggal_timbang = $("#tanggal_timbang").val();
    
    var xin_table = $('#data_berat').DataTable({
        "processing": true,
        "serverSide": true,
        "orderable": false,
        "targets": 'no-sort',
        "bSort": false,
        "bFilter": true,
        "ajax": {
            "url": '/cattle/data_weigth_history',
            "dataType": "json",
            "type": "POST",
            "data": { _token: "{{csrf_token()}}",tanggal:tanggal_timbang,id_cattle:id_cattle }
        },
        "columns": column_berat,
        "bDestroy": true
    });
    
}

function DataKandang() {
    var tanggal_pindah = $("#tanggal_pindah").val();
    var id_shed = $("#id_shed").val();
    
    var xin_table = $('#data_kandang').DataTable({
        "processing": true,
        "serverSide": true,
        "orderable": false,
        "targets": 'no-sort',
        "bSort": false,
        "bFilter": true,
        "ajax": {
            "url": '/cattle/data_shed_history',
            "dataType": "json",
            "type": "POST",
            "data": { _token: "{{csrf_token()}}",tanggal:tanggal_pindah,id_shed:id_shed,id_cattle:id_cattle }
        },
        "columns": column_kandang,
        "bDestroy": true
    });
    
}

$(function() {
    // $('#yourTable').DataTable();
    $(".select2-container--default").css('width', '100%');
    DataPakan()
    DataKandang()
    DataBerat()
});
</script>
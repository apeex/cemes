<script src="{{URL::asset('assets')}}/vendors/js/tables/datatable/datatables.min.js"></script>
<script src="{{URL::asset('assets')}}/vendors/js/tables/datatable/datatables.buttons.min.js"></script>
<script src="{{URL::asset('assets')}}/vendors/js/tables/datatable/buttons.print.min.js"></script>
<script src="{{URL::asset('assets')}}/vendors/js/tables/datatable/buttons.bootstrap.min.js"></script>
<script src="{{URL::asset('assets')}}/vendors/js/tables/datatable/datatables.bootstrap4.min.js"></script>
<script src="{{URL::asset('assets')}}/js/scripts/datatables/datatable.js"></script>
<script src="{{URL::asset('assets')}}/vendors/js/forms/select/select2.full.min.js"></script>
<script src="{{URL::asset('assets')}}/js/scripts/forms/select/form-select2.js"></script>
<script type="text/javascript">
var columns = [{
        "data": null,
        "sortable": false,
        render: function(data, type, row, meta) {
            return meta.row + meta.settings._iDisplayStart + 1;
        }
    },
    { "data": "no_invoice" },
    { "data": "date" },
    { "data": "total" },
    { "data": "status" },
    { "data": "action" }
];

function data_tabel(tabel) {

    if (tabel == 'data_transaksi') {
        //alert("bangsattt");
        start     = $("#start").val();
        end       = $("#end").val();
        status    = $("#status").val();
        search    = $("#search").val();
        var xin_table = $('#data_tabel').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": '/data_transaction',
                "dataType": "json",
                "type": "POST",
                "data": { _token: "{{csrf_token()}}",start:start,end:end,search:search,status:status }
            },
            "columns": columns,
            "bDestroy": true
        });
    }
}
$(function() {
    //$('#myTable').DataTable();
    $(".select2-container--default").css('width', '100%');
    data_tabel('data_transaksi')
    //alert("tabel");
});

function Reset(){
    location.reload();
}

function Save(){
    status = $("#status").val();
    id = $("#id_trans").val();
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({ //line 28
            type: 'POST',
            url: '/update_transaction',
            dataType: 'json',
            data: { id:id,status:status },
            success: function(data) {  
                
                if (data.code == 200) {
                    show_toast(data.message, 1);
                    location.reload();
                } else {
                    alert("maaf ada yang salah!!!");
                }
            }
        });
}
</script>
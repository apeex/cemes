<div class="table-responsive m-t-40">
    <table id="data_tabel" class="table display table-bordered table-striped no-wrap">
        <thead>
            <tr>
                <th>ID Ternak</th>
                <th>Kandang</th>
                <th>Jumlah Pakan</th>
            </tr>
        </thead>
        <tbody>
            @foreach($data as $d)
            <tr>
                <td>{{$d->cattle_id}}</td>
                <td>{{$d->cattle_name}}</td>
                <td>{{$d->weigth}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@extends('layout.app')
@section('asset')
<link rel="stylesheet" type="text/css" href="{{URL::asset('assets')}}/vendors/css/forms/select/select2.min.css">
@endsection
@section('title', 'Hewan Ternak')
@section('content')
<div class="row">
    <div class="">
        <!--begin::List Widget 9-->
        <div class="card card-custom card-stretch gutter-b">
            <!--begin::Header-->
            <div class="p-4">
                <div class="content-header mb-0 row">    
                    <div class="col-md-3 mb-5">
                        <select class="select2 form-control" name="id_peternak" id="id_peternak" onchange="data_tabel('data_cattle')">
                            <option value="">Semua Peternak</option>
                            @foreach($peternak as $p)
                            
                            <option value="{{$p->id}}">{{$p->user_name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-3 mb-5">
                        <select class="select2 form-control" name="kandang" id="kandang" onchange="data_tabel('data_cattle')">
                            <option value="">Semua kandang</option>
                            @foreach($kandang as $k)
                            
                            <option value="{{$k->id}}">{{$k->shed_name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-3 mb-5">
                        <select class="select2 form-control" name="gender" id="gender" onchange="data_tabel('data_cattle')">
                            <option value="">Semua Gender</option>
                            <option value="1">Jantan</option>
                            <option value="2">Betina</option> 
                        </select>
                    </div>   
                    <div class="col-md-3 mb-5">
                        <select class="select2 form-control" name="type" id="type" onchange="data_tabel('data_cattle')">
                            <option value="">Semua Tipe</option>
                            <option value="1">Internal</option>
                            <option value="2">Eksternal</option> 
                        </select>
                    </div>     
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive m-t-40">
                                <table id="data_tabel" class="table display table-bordered table-striped no-wrap">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Ternak</th>
                                            <th>Peternak</th>
                                            <th>Berat</th>
                                            <th>Gender</th>
                                            <th>Tgl Lahir</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('components/componen_js')
@include('components/js/cattle/index')
@endsection
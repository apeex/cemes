@extends('layout.app')
@section('asset')
<link rel="stylesheet" type="text/css" href="{{URL::asset('assets')}}/vendors/css/forms/select/select2.min.css">
@endsection
@section('title', 'Detail Ternak')
@section('content')
<!--  main page// -->
    <div class="content-header">
        <a href="javascript:history.back()"><i class="material-icons md-arrow_back"></i> Go back </a>
    </div>
    <div class="card mb-4">
        <div class="card-header bg-brand-2" style="height: 150px"></div>
        <div class="card-body">
            <div class="row">
                <div class="col-xl col-lg flex-grow-0" style="flex-basis: 230px">
                    <div class="img-thumbnail shadow w-100 bg-white position-relative text-center" style="height: 190px; width: 200px; margin-top: -120px">
                        @if($data->profile != null)
                        <img src="{{env('BASE_IMG')}}{{$data->profile}}" class="center-xy img-fluid" alt="Logo Brand" style="height: 190px;">
                        @else
                        <img src="{{URL::asset('assets')}}/imgs/items/1.jpg" class="center-xy img-fluid" alt="Logo Brand">
                        @endif
                    </div>
                </div>
                <!--  col.// -->
                <div class="col-xl col-lg">
                    <h3>{{$data->cattle_name}} - {{$data->cattle_id}}</h3>
                    <p><i class="fas fa-hanukiah ml-2"></i> 
                        @if($data->type == 1)
                        Internal
                        @else
                        Eksternal
                        @endif
                    </p>
                </div>
                <!--  col.// -->
                <div class="col-xl-4 text-md-end">
                    
                </div>
                <!--  col.// -->
            </div>
            <!-- card-body.// -->
            <hr class="my-4">
            <div class="row g-4">
                <div class="col-md-6">
                    
                </div>
                <!--  col.// -->
                <div class="col-md-3">
                    <h6>Data Hewan</h6>
                    <p>
                        ID Pakan         : {{$data->cattle_id }}<br>
                        Nama Hewan       : {{$data->cattle_name}}<br>
                        @if($data->type == 1)
                        Induk Jantan     : {{GetColoumnValue($data->male_parent,'t_cattles','cattle_name')}}<br>
                        Induk Betina     : {{GetColoumnValue($data->female_parent,'t_cattles','cattle_name')}}<br>
                        @else
                        Harga Beli       : Rp {{number_format($data->price)}}<br>
                        Tanggal Beli     :  {{$data->purchase_date}}<br>
                        @endif
                        Tanggal Lahir    : {{$data->birthday}}<br>
                    </p>
                </div>
                <!--  col.// -->
                <div class="col-md-3">
                    <h6>Data Pemilik Hewan</h6>
                    <p>
                        email          : {{$data->user_name}} <br>
                        email          : {{$data->user_email}} <br>
                        phone          : {{$data->user_phone}}<br>
                        Jenis kelamin  : {{$data->jk}}<br>
                        Tanggal Lahir  : {{$data->ultah}} <br>
                        No Nik         : {{$data->user_nik}}<br>
                    </p>
                </div>
                <!--  col.// -->
                <div class="col-sm-6 col-xl-4 text-xl-end">
                    
                </div>
                <!--  col.// -->
            </div>
            <!--  row.// -->
        </div>
        <!--  card-body.// -->
    </div>
<input type="hidden" id="id_cattle" value="{{$data->id}}" name="">
<div class="row">
    <div class="">
        <!--begin::List Widget 9-->
        <div class="card card-custom card-stretch gutter-b">
            <!--begin::Header-->
            <div class="p-4">
                <div class="row">
                    <div class="col-md-12">
                        <h3>Riwayat Pemberian Pakan</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label>Tanggal Pemberian Pakan</label>
                        <input type="date" id="tanggal" onchange="DataPakan()" name="tanggal" class="form-control">
                    </div>
                    <div class="col-md-4">
                        <label>Pilih Pakan</label>
                        <select class="form-control select2" name="id_food" id="id_food" onchange="DataPakan()">
                            <option value="" selected>Semua Pakan </option>
                            @foreach($food as $f)
                            <option value="{{$f->id}}">{{$f->food_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>   
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive m-t-40">
                                <table id="data_pakan" class="table display table-bordered table-striped no-wrap">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Pakan</th>
                                            <th>Kandang</th>
                                            <th>Berat</th>
                                            <th>Tanggal</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="">
        <!--begin::List Widget 9-->
        <div class="card card-custom card-stretch gutter-b">
            <!--begin::Header-->
            <div class="p-4">
                <div class="row">
                    <div class="col-md-12">
                        <h3>Riwayat Penimbangan Berat</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label>Tanggal Penimbangan</label>
                        <input type="date" id="tanggal_timbang" onchange="DataTimbang()" name="tanggal" class="form-control">
                    </div>
                </div>   
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive m-t-40">
                                <table id="data_berat" class="table display table-bordered table-striped no-wrap">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Kandang</th>
                                            <th>Berat</th>
                                            <th>Tanggal</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="">
        <!--begin::List Widget 9-->
        <div class="card card-custom card-stretch gutter-b">
            <!--begin::Header-->
            <div class="p-4">
                <div class="row">
                    <div class="col-md-12">
                        <h3>Riwayat Perpindahan Kandang</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label>Tanggal Pindah Kandang</label>
                        <input type="date" id="tanggal_pindah" onchange="DataKandang()" name="tanggal" class="form-control">
                    </div>
                    <div class="col-md-4">
                        <label>Pilih Kandang</label>
                        <select class="form-control select2" name="id_shed" id="id_shed" onchange="DataKandang()">
                            <option value="" selected>Semua kandang </option>
                            @foreach($shed as $s)
                            <option value="{{$s->id}}">{{$s->shed_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>   
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive m-t-40">
                                <table id="data_kandang" class="table display table-bordered table-striped no-wrap">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Kandang Awal</th>
                                            <th>Kandang Baru</th>
                                            <th>Tanggal</th>
                                            <th>Deskripsi</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('components/componen_js')
@include('components/js/cattle/detail')
@endsection
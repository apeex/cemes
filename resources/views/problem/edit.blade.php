@extends('layout.app')
@section('asset')
<link rel="stylesheet" type="text/css" href="{{URL::asset('assets')}}/vendors/css/forms/select/select2.min.css">
@endsection
@section('title', 'Administrator')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <form id="form_edit" method="post" enctype="multipart/form-data">@csrf
                    <div class="form-body">
                        <div class="row p-t-20">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Nama Kategori</label>
                                    <input type="text" name="category_name" id="nama" class="form-control" value="{{ $data->category_name}}" placeholder="">
                                    <input type="hidden" name="id" value="{{$data->id}}">
                                </div>
                            </div>
                        </div>
                        <!--/row-->
                    </div>


                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary mr-2 float-right" id="save_edit">Simpan Perubahan</button>
                        <button type="button" class="btn btn-primary mr-2 float-right" style="display: none;" id="loading_edit" disabled>Loading...</button>
                        <a href="/master/problems">
                        <button type="button" class="btn btn-info float-right" style="margin-right: 10px"><i class="material-icons md-keyboard_return"></i> Kembali ke List Problem</button></a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@include('components/componen_js')
@include('components/js/problem/edit')
@endsection
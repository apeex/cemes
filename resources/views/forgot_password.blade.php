<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Admin Panel SOUQ</title>
        <meta http-equiv="x-ua-compatible" content="ie=edge" />
        <meta name="description" content="" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta property="og:title" content="" />
        <meta property="og:type" content="" />
        <meta property="og:url" content="" />
        <meta property="og:image" content="" />
        <!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="{{URL::asset('assets')}}/imgs/icons/logos.png" />
        <!-- Template CSS -->
        <link href="{{URL::asset('assets')}}/css/main.css?v=1.1" rel="stylesheet" type="text/css" />
    </head>

    <body>
        <main>
            <header class="main-header style-2 navbar">
                <div class="col-brand">
                    <a href="index.html" class="brand-wrap">
                        <img src="{{URL::asset('assets')}}/imgs/icons/logos.png" class="logo" alt="Admin Souq" />
                    </a>
                </div>
                <div class="col-nav">
                    
                </div>
            </header>
            <section class="content-main mt-80 mb-80">
                <div class="card mx-auto card-login">
                    <div class="card-body" id="card-body">
                        <h4 class="card-title mb-4">Masukkan Kata Sandi Baru</h4>
                        @if ($errors->any())
                            <div class="alert alert-danger form-group">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form class="form" novalidate="novalidate" id="form_pass">@csrf
                            <div class="mb-3">
                                <input class="form-control" placeholder="Kata Sandi" type="password" name="password" required id="password" />
                            </div>
                            <!-- form-group// -->
                            <div class="mb-3">
                                <input class="form-control" placeholder="Ulangi Kata Snadi" type="password" name="password_confirmation" id="password_confirmation" required />
                            </div>
                            <!-- form-group// -->
                            <div class="mb-3">
                                <input type="hidden" name="FORGOT_PASSWORD_KEY" value="{{env('FORGOT_PASSWORD_KEY')}}">
                                <input type="hidden" name="id" value="{{$id}}">
                            </div>
                            <!-- form-group form-check .// -->
                            <div class="mb-4">
                                <button type="submit" class="btn btn-primary w-100">Simpan</button>
                            </div>
                            <!-- form-group// -->
                        </form>
                    </div>
                </div>
            </section>
        </main>
        <script src="{{URL::asset('assets')}}/js/vendors/jquery-3.6.0.min.js"></script>
        <script src="{{URL::asset('assets')}}/js/vendors/bootstrap.bundle.min.js"></script>
        <script src="{{URL::asset('assets')}}/js/vendors/jquery.fullscreen.min.js"></script>
        <!-- Main Script -->
        <script src="{{URL::asset('assets')}}/js/main.js?v=1.1" type="text/javascript"></script>
        <script src="{{URL::asset('assets')}}/js/validate.js"></script>
        <script src="{{URL::asset('assets')}}/js/additional-method.js"></script>
        <script type="text/javascript">
            $("#form_pass").validate({
                rules: {
                    password: {
                        minlength: 6
                    },
                    confirm_password: {
                        equalTo: "#password"
                    }
                },
                messages: {
                    password: {
                        minlength: "password minimal 6 character"
                    },
                    confirm_password: {
                        equalTo: "password not match"
                    }
                },
                submitHandler: function(form) {
                    
                    $.ajax({ //line 28
                        type: 'POST',
                        url: '/post_fargot_password',
                        dataType: 'json',
                        data: new FormData($("#form_pass")[0]),
                        processData: false,
                        contentType: false,
                        success: function(data) {
                            $("#loading_add").css('display', 'none');
                            $("#save_add").css('display', '');
                            if (data.code == 200) {
                                show_toast(data.message, 1);
                            } else {
                                show_toast(data.message, 1);
                            }
                        }
                    });
                }
            });
        </script>
    </body>
</html>
<!-- Pop Up Edit -->
@extends('layout.app')
@section('asset')
<link rel="stylesheet" type="text/css" href="{{URL::asset('assets')}}/vendors/css/forms/select/select2.min.css">
@endsection
@section('title', 'Role')
@section('content')
<section class="multiple-validation" id="detail_edit">
    <div class="row">
        <div class="col-md-12">
            <div class="card mb-5">
                <div class="card-content">
                    <div class="card-body">
                        <form class="form-horizontal" novalidate id="form_edit">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="mb-4">
                                        <div class="controls">
                                            <label for="nomor" class="label_input">Nama role</label>
                                            <input type="text" name="name" class="form-control form-control-lg" placeholder="Name" required value="{{$data->name}}">
                                            <input type="hidden" name="id" value="{{$data->id}}">

                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <table class="table-striped table-bordered table-hover" width="100%" border="1">
                                            <thead>
                                                <th>Menu</th>
                                                <th>Sub Menu</th>
                                            </thead>
                                            <?php for ($i=0; $i < count($mymenu) ; $i++) { ?>
                                            <tr>
                                                <td style="padding: 5px;">
                                                    
                                                    <?php if ($mymenu[$i]->menu_id == rcheck($mymenu[$i]->menu_id,$data->id)) { ?>
                                                    <label class="form-check mb-2"><input type="checkbox" value="{{$mymenu[$i]->id}}" name="menu[]" id="menu{{$mymenu[$i]->menu_id}}" class="form-check-input check_menu" tabel="{{$mymenu[$i]->menu_id}}" checked><span class="form-check-label">{{$mymenu[$i]->name}}</span></label>
                                                    <?php } else { ?>
                                                    <label class="form-check mb-2"><input type="checkbox" value="{{$mymenu[$i]->id}}" name="menu[]" id="menu{{$mymenu[$i]->menu_id}}" class="form-check-input check_menu" tabel="{{$mymenu[$i]->menu_id}}"><span class="form-check-label">{{$mymenu[$i]->name}}</span></label>
                                                    <?php } ?>
                                                </td>
                                                <td style="padding: 5px;">
                                                    <?php for ($a=0; $a < count($mysubmenu) ; $a++) { ?>
                                                    <?php if ($mymenu[$i]->menu_id == $mysubmenu[$a]->parent_menu_id) { 

                                            if ($mysubmenu[$a]->parent_menu_id == rcheck2($mysubmenu[$a]->id,$data->id,$mysubmenu[$a]->parent_menu_id)) { ?>
                                                    <div class="col-md-12"><label class="form-check mb-2"><input type="checkbox" value="{{$mysubmenu[$a]->id}}" name="submenu[]" id="submenu{{$mysubmenu[$a]->parent_menu_id}}" class="form-check-input sub{{$mymenu[$i]->menu_id}}" checked><span class="form-check-label">{{$mysubmenu[$a]->name}}</span></label></div>
                                                    <?php } else { ?>
                                                    <div class="col-md-12"><label class="form-check mb-2"><input type="checkbox" value="{{$mysubmenu[$a]->id}}" name="submenu[]" id="submenu{{$mysubmenu[$a]->parent_menu_id}}" class="form-check-input sub{{$mymenu[$i]->menu_id}}" disabled><span class="form-check-label">{{$mysubmenu[$a]->name}}</span></label></div>
                                                    <?php } ?>
                                                    <?php }?>
                                                    <?php }?>
                                                </td>
                                            </tr>
                                            <?php } ?>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-xs-12 mt-3">
                                    <button type="submit" class="btn btn-primary mr-2 float-right" id="save_edit">Simpan Perubahan</button>
                                    <button type="button" class="btn btn-primary mr-2 float-right" style="display: none;" id="loading_edit" disabled>Loading...</button>
                                    <a href="/administrator/role-admin">
                                    <button type="button" class="btn btn-info float-right" style="margin-right: 10px"><i class="material-icons md-keyboard_return"></i> Kembali ke Role</button></a>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Pop Up Edit -->
@include('components/componen_js')
@include('components/js/roles/edit')
<!-- end -->
@endsection
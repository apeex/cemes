@extends('layout.app')
@section('asset')
<link rel="stylesheet" type="text/css" href="{{URL::asset('assets')}}/vendors/css/forms/select/select2.min.css">
@endsection
@section('title', 'Role')
@section('content')
<div class="form_edit_modul" id="show_edit">
</div>
<div class="row" id="show_add" style="display: none;">
    <div class="">
        <section class="multiple-validation">
            <div class="card mb-3">
                <div class="card-content">
                    <div class="card-body">
                        <form class="form-horizontal" id="form_add">@csrf
                            <div class="row mb-3">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="controls">
                                            <label for="nomor" class="label_input">Nama role</label>
                                            <input type="text" name="name" class="form-control form-control-lg" placeholder="Name" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12">
                                    <label class="control-label">Pilih Menu </label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <table class="table-striped table-bordered table-hover" width="100%" border="1">
                                            <thead>
                                                <th>Menu</th>
                                                <th>Sub Menu</th>
                                            </thead>
                                            @foreach($menu_choose as $k => $v)
                                            <tr>
                                                <td style="padding: 5px;">
                                                    <label class="form-check mb-2">
                                                        <input class="form-check-input check_menu" tabel="{{$v->menu_id}}" value="{{$v->id}}" name="menu[]" id="menu{{$v->menu_id}}" type="checkbox" value="">
                                                        <span class="form-check-label"> {{$v->name}} </span>
                                                    </label>
                                                </td>
                                                <td style="padding: 5px;">
                                                    @foreach($submenu_choose as $f => $s)
                                                    <?php if ($v->menu_id == $s->parent_menu_id) { ?>
                                                    <label class="form-check mb-2 me-3">
                                                        <input class="form-check-input sub{{$v->menu_id}}" value="{{$s->id}}" name="submenu[]" id="submenu{{$s->parent_menu_id}}" type="checkbox" value="" disabled>
                                                        <span class="form-check-label"> {{$s->name}} </span>
                                                    </label>
                                                    <?php }?>
                                                    @endforeach
                                                </td>
                                            </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-sm-12 col-xs-12">
                                    <button type="submit" class="btn btn-primary float-right" id="save_add">Submit</button>
                                    <button type="button" disabled class="btn btn-primary float-right" id="loading_add" style="display: none;">Loading....</button>
                                    <button type="button" class="btn btn-danger mr-2 float-right" id="batalkan">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<div class="row">
    <div class="">
        <!--begin::List Widget 9-->
        <div class="card card-custom card-stretch gutter-b">
            <!--begin::Header-->
            <div class="card-header align-items-center border-0 mt-2">
                <a class="btn btn-primary font-weight-bolder mr-3" id="tambah">Add Role</a>
            </div>
            <!--end::Header-->
            <!--begin::Body-->
            <div class="card-body pt-2">
                <section id="basic-datatable">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <!-- <div class="card-header">
                                    <h4 class="card-title">List Admin</h4>
                                </div> -->
                                <div class="card-content">
                                    <div class="card-body card-dashboard">
                                        <div class="table-responsive">
                                            <table class="table zero-configuration" id="data_table">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Name</th>
                                                        <th>Status</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <!--end: Card Body-->
        </div>
        <!--end: Card-->
        <!--end: List Widget 9-->
    </div>
</div>
@include('components/componen_js')
@include('components/js/roles/index')
@endsection
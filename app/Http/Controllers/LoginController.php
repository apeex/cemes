<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Administrator;
use Auth;
use Redirect;
use Illuminate\Support\MessageBag;
use DB;
use App\Exceptions\Handler;
use Illuminate\Support\Facades\Hash;
use App\Model\ParticipantModel;
use App\Traits\Fungsi;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        try {
            $this->validate($request, [
                'username' => 'required',
                'password' => 'required',
            ]);

            // dd(Administrator::get());
            // dd($request->all());
            $cek = Administrator::where('email', $request->username)->whereNull('deleted_at')->get();
            // $cekok = Administrator::get();
            //dd($cek);
            if ($cek->isNotEmpty()) {
                $role = $cek[0]->id_role;
                //dd($role);
                
                if (Auth::guard('admin')->attempt(['email' => $request->username, 'password' => $request->password])) {
                    // dd("success");
                    $insert_log      = parent::LogAdmin(\Request::ip(),Auth::guard('admin')->user()->id,'Login ke System','Login');
                    return redirect()->intended('/dashboard');

                } else {
                    $errors = new MessageBag(['password' => ['Your Email Or password invalid!. Please Check and Try Again!!!!']]);
                     return Redirect::back()->withErrors($errors);
                }
            } else {
                //dd("not except");
                     $errors = new MessageBag(['password' => ['Email Tidak Terdaftar di System']]);
                     return Redirect::back()->withErrors($errors);
            }
        // dd($request->all());
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'LoginController@login';
            $insert_error = parent::InsertErrorSystem($data);
            $error = parent::sidebar();
            $error['id'] = $insert_error;
            return view('errors.index',$error); // jika Metode Get
            //return response()->json($data); // jika metode Post
        }
    }

    public function logout(){
        try {
            $id_admin = Auth::guard('admin')->user()->id;
            date_default_timezone_set('Asia/Jakarta');
            $insert_log      = parent::LogAdmin(\Request::ip(),Auth::guard('admin')->user()->id,'Keluar dari System','Log Out');
            Auth::guard('admin')->logout();
            return redirect('/');
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'LoginController@logout';
            $insert_error = parent::InsertErrorSystem($data);
            $error = parent::sidebar();
            $error['id'] = $insert_error;
            return view('errors.index',$error); // jika Metode Get
            //return response()->json($data); // jika metode Post
        }
    }

    public function cronjobs_kirim_password(){
        try {
            //$id_admin = Auth::guard('admin')->user()->id;
            date_default_timezone_set('Asia/Jakarta');
            //$insert_log      = parent::LogAdmin(\Request::ip(),Auth::guard('admin')->user()->id,'Cronjobs Mengirimkan Password','Cronjobs');
            $data = DB::table('participant')->whereNull('password')->whereNull('deleted_at')->orderBy('participant_name','asc')->take(10)->get();
            //dd($data);
            //$pass = $this->randomPassword();
            foreach ($data as $k => $v) {
                //$cekhp = substr($v->participant_phone, -1);
                //if ($cekhp == 0 || $cekhp == 1 || $cekhp == 2 || $cekhp == 3 ){ //Kondisi
                    $pass = $this->randomPassword();
                    $data_update = ['password'=>Hash::make($pass),'confirm_password'=>encrypt($pass),'created_otp'=>date('Y-m-d H:i:s'),'updated_at'=>null];
                    $update = ParticipantModel::where('id',$v->id)->update($data_update);
                    $message1 = 'Rahasia! Halo Alumni Teknik UI! Password: '.$pass.' bisa kamu gunakan untuk mengaktifkan akun di pemira.iluniteknik.org! Yuk, login!';
                    $phone = substr($v->participant_phone, 1);
                    //dd($phone);
                    //$send_message = Fungsi::BlastWA($message1,$phone);
                    $send_message = Fungsi::sendWa_zenzifa($phone,$message1);
                    $send_email = Fungsi::EmailSending($v->participant_email,$pass,$v->participant_name);
                //}
                
            }
            $count = DB::table('participant')->whereNull('password')->whereNull('deleted_at')->count();
            dd($count);
            
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'LoginController@cronjobs_kirim_password';
            $insert_error = parent::InsertErrorSystem($data);
            // $error = parent::sidebar();
            // $error['id'] = $insert_error;
            // return view('errors.index',$error); // jika Metode Get
            return response()->json($data); // jika metode Post
        }
    }

    public function cronjobs_kirim_password_two(){
        try {
            $id_admin = Auth::guard('admin')->user()->id;
            date_default_timezone_set('Asia/Jakarta');
            $insert_log      = parent::LogAdmin(\Request::ip(),Auth::guard('admin')->user()->id,'Cronjobs Mengirimkan Password','Cronjobs');
            $data = DB::table('participant')->whereNull('password')->whereNull('deleted_at')->orderBy('participant_name','asc')->take(30)->get();
            //dd($data);
            //$pass = $this->randomPassword();
            foreach ($data as $k => $v) {
                $cekhp = substr($v->participant_phone, -1);
                if ($cekhp == 5 || $cekhp == 6 || $cekhp == 4 ){ //Kondisi
                    $pass = $this->randomPassword();
                    $data_update = ['password'=>Hash::make($pass),'confirm_password'=>encrypt($pass),'created_otp'=>date('Y-m-d H:i:s'),'updated_at'=>null];
                    $update = ParticipantModel::where('id',$v->id)->update($data_update);
                    $message1 = 'Rahasia! Halo Alumni Teknik UI! Password: '.$pass.' bisa kamu gunakan untuk mengaktifkan akun di pemira.iluniteknik.org! Yuk, login!';
                    $phone = substr($v->participant_phone, 1);
                    //dd($phone);
                    $send_message = Fungsi::BlastWA_two($message1,$phone);
                    $send_email = Fungsi::EmailSending($v->participant_email,$pass,$v->participant_name);
                }
                
            }
            $count = DB::table('participant')->whereNull('password')->whereNull('deleted_at')->count();
            dd($count);
            
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'LoginController@cronjobs_kirim_password';
            $insert_error = parent::InsertErrorSystem($data);
            // $error = parent::sidebar();
            // $error['id'] = $insert_error;
            // return view('errors.index',$error); // jika Metode Get
            return response()->json($data); // jika metode Post
        }
    }

    public function cronjobs_kirim_password_tiga(){
        try {
            $id_admin = Auth::guard('admin')->user()->id;
            date_default_timezone_set('Asia/Jakarta');
            $insert_log      = parent::LogAdmin(\Request::ip(),Auth::guard('admin')->user()->id,'Cronjobs Mengirimkan Password','Cronjobs');
            $data = DB::table('participant')->whereNull('password')->whereNull('deleted_at')->orderBy('participant_name','asc')->take(30)->get();
            //dd($data);
            //$pass = $this->randomPassword();
            foreach ($data as $k => $v) {
                $cekhp = substr($v->participant_phone, -1);
                if ($cekhp == 7 || $cekhp == 8 || $cekhp == 9){ //Kondisi
                    $pass = $this->randomPassword();
                    $data_update = ['password'=>Hash::make($pass),'confirm_password'=>encrypt($pass),'created_otp'=>date('Y-m-d H:i:s'),'updated_at'=>null];
                    $update = ParticipantModel::where('id',$v->id)->update($data_update);
                    $message1 = 'Rahasia! Halo Alumni Teknik UI! Password: '.$pass.' bisa kamu gunakan untuk mengaktifkan akun di pemira.iluniteknik.org! Yuk, login!';
                    $phone = substr($v->participant_phone, 1);
                    //dd($phone);
                    $send_message = Fungsi::BlastWA_tiga($message1,$phone);
                    $send_email = Fungsi::EmailSending($v->participant_email,$pass,$v->participant_name);
                }
                
            }
            $count = DB::table('participant')->whereNull('password')->whereNull('deleted_at')->count();
            dd($count);
            
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'LoginController@cronjobs_kirim_password';
            $insert_error = parent::InsertErrorSystem($data);
            // $error = parent::sidebar();
            // $error['id'] = $insert_error;
            // return view('errors.index',$error); // jika Metode Get
            return response()->json($data); // jika metode Post
        }
    }
 
    public function randomPassword() {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!#$';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 11; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }
}

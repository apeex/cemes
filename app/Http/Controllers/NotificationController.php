<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Classes\upload;
use App\Traits\Fungsi;
use App\Models\NotifikasiModel;
use App\Models\RoleModel;
use Auth;
use DB;
use Redirect;
use App\Exceptions\Handler;

class NotificationController extends Controller
{
    
    public function index()
    {
        // dd(QuestionModel::get());
        try {
            $data = parent::sidebar();
            $data['header_title'] = "List Data Notifikasi";
            if ($data['access'] == 0) {
                return redirect('/');
            } else {
                $role_id           = Auth::guard('admin')->user()->id_role;
                $data['data_role'] = RoleModel::whereNull('deleted_at')->where('status', 1)->get();
                $data['data']      = DB::table('t_foods')->where('status',1)->select('id','food_name as name')->get();
                $in_id_user = NotifikasiModel::whereNotNull('id_user')->pluck('id_user')->toArray();
                $data['user'] = DB::table('t_users')->whereIn('id',$in_id_user)->select('id','user_name')->get();
                return view('notifications.index', $data);
            }

        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'NotificationController@index';
            $insert_error = parent::InsertErrorSystem($data);
            $error = parent::sidebar();
            $error['id'] = $insert_error;
            return view('errors.index',$error); // jika Metode Get
            //return response()->json($data); // jika metode Post
        }
    }

    public function list_data(Request $request)
    {
        $totalData = NotifikasiModel::whereNull('deleted_at')->count();

        $limit = $request->input('length');
        $start = $request->input('start');
        $dir   = $request->input('order.0.dir');
        $search = $request->search;

        $posts = DB::table('t_notifications as q')->leftJoin('t_users as u', 'u.id', 'q.id_user')
                 ->whereNull('q.deleted_at')->where(function ($query) use ($search,$request) {
                if ($search != null) {
                    $query->where(function ($query2) use ($search,$request) {
                        $query->where('title','ilike', "%{$search}%");
                        $query->orWhere('text','ilike', "%{$search}%");
                    });
                } 
                if ($request->tipe != null) {
                    $query->where('tipe',$request->tipe);
                }
                if ($request->id_admin != null) {
                    $query->where('created_by',$request->id_admin);
                }
            })
            ->select('user_name as name','q.*')
            ->orderBy('q.created_at','desc');

        $totalFiltered = $posts->count();

        $posts = $posts->limit($limit)->offset($start)->get();

        $data = array();
        if (!empty($posts)) {
            $no = 0;
            foreach ($posts as $d) {
                $no = $no + 1;
                if ($d->name == null) {
                    $name = "Semua User";
                } else {
                    $name = $d->name;
                }
                $text = strip_tags($d->text);
                $length_str = strlen($text);
                if ($length_str > 200) {
                    $text = substr($text, 0, 200).' ....';
                }
                //delete
                $action = '<div style="float: left; margin-left: 5px;">
                    <button type="button" class="btn btn-danger btn-sm aksi btn-aksi" data="data_notifikasi"  id="' . $d->id . '" aksi="delete" tujuan="notifikasi" style="min-width: 110px;margin-left: 2px;margin-top:3px;text-align:left"><i class="fa fa-trash"></i> Hapus</button>
                </div>';

                $column['title']     = $d->title;
                $column['name']      = $name;
                $column['text']      = $text;
                $column['actions']   = $action;
                $data[]              = $column;

            }
        }
        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data,
        );

        echo json_encode($json_data);
    }

    public function post(Request $request)
    {
        try {
            $input = $request->all();
            if ($request->file('image')) {
                $image  = parent::uploadFileS3($request->file('image'));
            } else {
                $image = null;
            }

            //umum
            if ($request->jenis_notifikasi == 1) {
                $user = DB::table('t_users')->whereNull('deleted_at')->whereNotNull('user_email')->get();
                $insertadmin = array(
                    'id_user'   => null,
                    'file'       => $image,
                    'text' => $request->deskripsi,
                    'title' => $request->title,
                    'status'      => 1,
                    'tipe'      => $request->tipe,
                    'id_detail' => $request->id_detail,
                    'is_umum' => 1,
                    'created_by' => Auth::guard('admin')->user()->id,
                    'created_at'  => date('Y-m-d H:i:s'),
                    'updated_at'  => date('Y-m-d H:i:s'),
                    );
                    $insert = DB::table('t_notifications')->insertGetId($insertadmin);
                       $messages = strip_tags($request->deskripsi);
                       $id_object = $insert;
                       $title = $request->title;
                    foreach ($user as $k => $v) {
                        // seng ngerjakno
                        $id = $v->id;
                        
                        $notif_android  = parent::NotifeAndroid($id,$request->id_detail,$request->title,$messages,$request->tipe);
                    }
                $insert_log      = parent::LogAdmin(\Request::ip(),Auth::guard('admin')->user()->id,'Menambah Notifikasi Umum','Notifikasi');
            //khusus
            } else if($request->jenis_notifikasi == 2) {
              $subjek = $request->subjek;
                foreach ($subjek as $a => $b) {
                    $user = DB::table('t_users')->where('id',$subjek[$a])->first();
                    $insertadmin = array(
                    'id_user'   => $subjek[$a],
                    'file'       => $image,
                    'text' => $request->deskripsi,
                    'title' => $request->title,
                    'id_detail' => $request->id_detail,
                    'status' => 1,
                    'tipe' => $request->tipe,
                    'is_umum' => 0,
                    'created_by'      => Auth::guard('admin')->user()->id,
                    'created_at'  => date('Y-m-d H:i:s'),
                    'updated_at'  => date('Y-m-d H:i:s'),
                    );
                    $insert = DB::table('t_notifications')->insertGetId($insertadmin);

                    // seng ngerjakno
                    $id = $subjek[$a];
                    $messages = strip_tags($request->deskripsi);
                    $id_object = $insert;
                    $title = $request->title;
                    $action = "activities.others.NotificationActivity";
                    $notif_android  = parent::NotifeAndroid($id,$request->id_detail,$request->title,$messages,$request->tipe);
                    //$send_email = parent::EmailSenderNotifikasi($user->user_email,$user->user_name,$image,$request->title,$request->deskripsi);
                    $insert_log      = parent::LogAdmin(\Request::ip(),Auth::guard('admin')->user()->id,'Menambah Notifikasi '.$subjek[$a].'','Notifikasi');
                }
            }

            $id_admin    = Auth::guard('admin')->user()->id;
            
            if ($insert) {
                
                $data['code']    = 200;
                $data['message'] = 'Berhasil menambah Data Notifikasi';

                return response()->json($data);
            } else {
                $data['code']    = 500;
                $data['message'] = 'Maaf Ada yang Error ';
                return response()->json($data);
            }
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'NotificationController@post';
            $insert_error = parent::InsertErrorSystem($data);
            return response()->json($data); // jika metode Post
        }
        // return response()->json($request->all());
    }

    public function delete(Request $request)
    {
        $bank             = NotifikasiModel::find($request->id);
        $bank->status     = 0;
        $bank->deleted_at = date('Y-m-d');
        $bank->save();

        if ($bank) {
            $id_admin    = Auth::guard('admin')->user()->id;
            
            $data['code']    = 200;
            $data['message'] = 'Berhasil Menghapus Data Notifikasi';
            $insert_log      = parent::LogAdmin(\Request::ip(),Auth::guard('admin')->user()->id,'Menghapus Notifikasi '.$request->id,'Notifikasi');
            return response()->json($data);
        } else {
            $data['code']    = 500;
            $data['message'] = 'Maaf Ada yang Error ';
            return response()->json($data);
        }
    }

    public function search_user($id){
        $data['data'] = DB::table('t_users')->whereNull('deleted_at')->where('user_name', 'ILIKE', "%{$id}%")->select('id','user_name','user_phone as nik')->get();
          return view('notifications.data_user',$data);

    }

    public function get_type($id){
        $data = [];
        if ($id == '1') {
            $data = DB::table('t_foods')->where('status',1)->select('id','food_name as name')->get();
        } else if ($id == '2') {
            $data = DB::table('t_sheds')->where('status',1)->select('id','shed_name as name')->get();
        }

        return json_encode($data);
    }


}
<?php

namespace App\Http\Controllers;

use App\Models\LogAdmin;
use App\Models\LogError;
use App\Traits\Fungsi;
use App\CompanyVerifikasi;
use Auth;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use DB;
use URL;
use DateTime;
use Illuminate\Routing\Controller as BaseController;
use Redirect;
use SendGrid\Mail\Mail;
use App\Classes\S3;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function sidebar()
    {
        $currentURL = url()->current();
        $arrayURL = explode("/",$currentURL);
        $jumlah = count($arrayURL);
        //dd(Auth::guard('admin')->user());
        $role_id         = Auth::guard('admin')->user()->id_role;
        $id_admin         = Auth::guard('admin')->user()->id;
        $data['id_admin'] = $id_admin;
        $data['id_cabang'] = Auth::guard('admin')->user()->id_cabang;

        if ($jumlah == 4) {
            //dd("oke");
            $menu = $arrayURL[3];
            $id_submenu = DB::table('t_menus')->where('slug',$menu)->pluck('id');
        } else {
            
            $menu = $arrayURL[3];
            $submenu = $arrayURL[4];
            //dd($submenu);
            if ($menu == 'tanya-jawab' || $menu == 'ustadz' || $menu == 'member') {
                $id_submenu = DB::table('t_menus')->where('slug',$menu)->pluck('id');
            } else {
                $id_menu = DB::table('t_menus')->where('slug',$menu)->where('parent_menu_id',0)->pluck('menu_id');
                // dd($id_menu);
                if ($submenu == 'filter_user' || $submenu == 'detail') {
                    $id_submenu = DB::table('t_menus')->where('slug','list-user-member')->where('parent_menu_id',$id_menu[0])->pluck('id');
                } else {
                    $id_submenu = DB::table('t_menus')->where('slug',$submenu)->where('parent_menu_id',$id_menu[0])->pluck('id');
                }
            }
            
        } 

        //dd($role_id);
        $validasi = DB::table('t_role_menus')->where('role_id',$role_id)->where('menu_id',$id_submenu[0])->get();

        //dd($validasi);
        if ($validasi->isEmpty()) {
            $data['access'] = 0;
        } else {
            $data['access'] = 1;
            $data['menu']    = Fungsi::getmenu($role_id);
            $data['submenu'] = Fungsi::getsubmenu($role_id);
            $role = DB::table('t_roles')->where('id',$role_id)->pluck('slug');
            $data['role']    = $role[0];
        }

        $data_admin = DB::table('t_administrators')->where('id',$id_admin)->get();
        //dd($data_admin);
        if ($data_admin[0]->profile == null) {
            $data['admin_profile'] = base_img()."default_image.png";
        } else {
            $data['admin_profile'] = base_img().$data_admin[0]->profile;
        }

        $data['admin_name'] = $data_admin[0]->admin_name;

        //dd($data['data_chat']);
        //$data['jumlah_data_chat'] = count($data_chat);
        // dd($validasi);
        return $data;
    }

    public function admin_data() {
        $role_id         = Auth::guard('admin')->user()->id_role;
        $id_admin         = Auth::guard('admin')->user()->id;
        $data['id_admin'] = $id_admin;
        $role = DB::table('t_roles')->where('id',$role_id)->pluck('slug');
        $data['role']    = $role[0];
        //$data['id_cabang'] = Auth::guard('admin')->user()->id_cabang;

        return $data;
    }
    public function LogAdmin($mac,$admin,$actifity,$menu)
    {
        date_default_timezone_set("Asia/Jakarta");
        $insertlog = array(
            'id_admin' => $admin
            , 'activity' => $actifity
            , 'menu' => $menu
            ,'mac_address' => $mac
            , 'created_at' => date('Y-m-d H:i:s')
            , 'updated_at' => date('Y-m-d H:i:s'),
        );
        LogAdmin::insert($insertlog);
        //dd("masuk");
    }

    public function EmailSender($email_x,$name,$message)
    {

        $email = new Mail();
        $email->setFrom("info@genikalab.id", "Laboratorium Klinik Genika");
        $email->setSubject("Pemberitahuan");
        $email->addTo($email_x, $name);
        $email->setTemplateId('d-1d799b9c85374ea9b39cab09cd0bd585');
        $id = base64_encode($email_x);
        $link = "";

        $substitutions = [
              "greeting" => "Yth ".$name
        ];
        $email->addDynamicTemplateDatas($substitutions);
        $sendgrid = new \SendGrid('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');
        try {
            $response = $sendgrid->send($email);
            $data['status'] = $response->statusCode() . "\n";
            $data['header'] = ($response->headers());
            $data['body'] = $response->body() . "\n";
        } catch (Exception $e) {
            $data['status'] = 'Caught exception: '. $e->getMessage() ."\n";
        }

        return $data;
        
    }

    public function EmailSenderFile($email_x,$name,$file,$layanan,$cabang,$hasil)
    {
        $email = new Mail();
        $email->setFrom("info@genikalab.id", "Laboratorium Klinik Genika");
        $email->setSubject("Pemberitahuan");
        $email->addTo($email_x, $name);
        $email->setTemplateId('d-65ede2aaed044a57b4d14cf02da7142f');
        $id = base64_encode($email_x);
        $link = "";

        $substitutions = [
              "name" => "".$name,
              "layanan_labs" => $layanan,
              "no_lab" => $layanan,
              "cabang" => $cabang,
              "hasil_labs" => $hasil
        ]; 

        $file_encoded = base64_encode(file_get_contents("./assets/file_hasil/".$file.".pdf"));
        $email->addAttachment(
           $file_encoded,
           "application/pdf",
           $file.".pdf",
           "attachment"
        );

        $email->addDynamicTemplateDatas($substitutions);
        $sendgrid = new \SendGrid('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');
        // $sendgrid = new \SendGrid('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');
        try {
            $response = $sendgrid->send($email);
            $data['status'] = $response->statusCode() . "\n";
            $data['header'] = ($response->headers());
            $data['body'] = $response->body() . "\n";
        } catch (Exception $e) {
            $data['status'] = 'Caught exception: '. $e->getMessage() ."\n";
        }

        // dd($data);

        return $data;
    }

    public function EmailSenderHasil($email_x,$name,$file,$layanan,$cabang)
    {
        $email = new Mail();
        $email->setFrom("info@genikalab.id", "Laboratorium Klinik Genika");
        $email->setSubject("Pemberitahuan");
        $email->addTo($email_x, $name);
        $email->setTemplateId('d-65ede2aaed044a57b4d14cf02da7142f');
        $id = base64_encode($email_x);
        $link = "";

        $substitutions = [
              "name" => "".$name,
              "layanan_labs" => $layanan,
              "no_lab" => $layanan,
              "cabang" => $cabang,
              "hasil_labs" => "Berhasil"
        ]; 

        $file_encoded = base64_encode(file_get_contents("./assets/file_hasil/".$file));
        $email->addAttachment(
           $file_encoded,
           "application",
           $file,
           "attachment"
        );

        $email->addDynamicTemplateDatas($substitutions);
        $sendgrid = new \SendGrid('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');
        // $sendgrid = new \SendGrid('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');
        try {
            $response = $sendgrid->send($email);
            $data['status'] = $response->statusCode() . "\n";
            $data['header'] = ($response->headers());
            $data['body'] = $response->body() . "\n";
        } catch (Exception $e) {
            $data['status'] = 'Caught exception: '. $e->getMessage() ."\n";
        }

        // dd($data);

        return $data;
    }

    public function EmailSenderNotifikasi($user_email,$user_name,$file,$title,$deskripsi){
         
        $email = new Mail();
        $email->setFrom("info@genikalab.id", "Laboratorium Klinik Genika");
        $email->setSubject("Pemberitahuan");
        $email->addTo($user_email, $user_name);
        $email->setTemplateId('d-c8eac32fdee54c72a0fe961caf54b0f9');
        $id = base64_encode($user_email);
        $link = "";

        $substitutions = [
              "name" => "Yth ".$user_name,
              "title" => $title,
              "text" => strip_tags($deskripsi)
        ]; 

        if ($file != null) {
            $file_encoded = base64_encode(file_get_contents("./assets/file_hasil/".$file));
            $email->addAttachment(
               $file_encoded,
               "application",
               $file,
               "attachment"
            );
        }

        $email->addDynamicTemplateDatas($substitutions);
        $sendgrid = new \SendGrid('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');
        // $sendgrid = new \SendGrid('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');
        try {
            $response = $sendgrid->send($email);
            $data['status'] = $response->statusCode() . "\n";
            $data['header'] = ($response->headers());
            $data['body'] = $response->body() . "\n";
        } catch (Exception $e) {
            $data['status'] = 'Caught exception: '. $e->getMessage() ."\n";
        }

        // dd($data);

        return $data;
    }

    public function InsertErrorSystem($data) {
        date_default_timezone_set("Asia/Jakarta");
        $id_o = null;
        if (isset(Auth::guard('admin')->user()->id)) {
            $id_o = Auth::guard('admin')->user()->id;
        }
        $data_insert = array(
            'exception' => $data['message'],
            'line_error' => $data['line'],
            'controller' => $data['controller'],
            'id_user'    => $id_o,
            'tipe_user' => 2,
        );
        $insert = LogError::insertGetId($data_insert);
        return $insert;
    }

    public function cleanHazard($string) {
       $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

       return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }

    public function MakeSlug($string){
        $slug = str_replace(array(' ', '/', '(', ')', '[', ']', '?', ':', ';','<', '>', '&', '{', '}', '*'), array('-'),$string);
        $slug = strtolower($slug);
        return $slug;
    }

    public function uploadFileS3($file){
        $acces_key = env('AWS_ACCESS_KEY_ID');
        //dd($acces_key);
        $secret_key = env('AWS_SECRET_ACCESS_KEY');
        $s3 = new S3($acces_key,$secret_key);
        $bucket = env('AWS_BUCKET');
        //$image = $file->hashName();
        $name = $this->cleanHazard(pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME));
        //dd($name);
        $image = $name.'_'.time().'.'.$file->getClientOriginalExtension();
        
        $file_path = public_path('images/'.$image);
        $file->move('images/', $image);
        $s3->putObjectFile($file_path, $bucket, $image, S3::ACL_PUBLIC_READ);
        //File::delete($file_path);

        return $image;
    }

    public static function NotifeAndroid($id,$id_object,$title,$messages,$tipe) {
            
        $firebase_token = DB::table('t_users')->where('id',$id)->select('firebase_android','firebase_ios')->get();
        if ($firebase_token->isNotEmpty()) {
            // firebase android
            if ($firebase_token[0]->firebase_android != null || $firebase_token[0]->firebase_android != '') {
                $url = 'https://fcm.googleapis.com/fcm/send';
                $registrationIds = array($firebase_token[0]->firebase_android);   
                // prepare the message
                $fields = array (
                            'registration_ids' => $registrationIds,
                            "notification" => array(
                                "body" => $messages,
                                "title" =>$title,
                                "click_action" => 'FLUTTER_NOTIFICATION_CLICK'
                            ),
                            "data" => array("body" =>$messages,"title"=>"Notifikasi","tipe" => $tipe,'id_detail'=>$id_object)
                    );
                
                $headers = array (
                    'Authorization: key=' . "AAAA8xZHI9g:APA91bHJGuoF7n9MuI3PF8KxU5LeFvzpFZ_j4T1mT-3yieqlw1V0vwFycrgH2yqTq3L-nSAacudeFlzyVj26QS0oHpJdbDVlmEp29p4fUydp7KJJdg0CFvP6hM1956u_1eAd7dmK1iYu",
                    'Content-Type: application/json'
                );

                $ch = curl_init();
                curl_setopt( $ch,CURLOPT_URL,$url);
                curl_setopt( $ch,CURLOPT_POST,true);
                curl_setopt( $ch,CURLOPT_HTTPHEADER,$headers);
                curl_setopt( $ch,CURLOPT_RETURNTRANSFER,true);
                curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER,false);
                curl_setopt( $ch,CURLOPT_POSTFIELDS,json_encode($fields));
                $result = curl_exec($ch);
                curl_close($ch);

                //echo $result;
            }

            // firebase ios
            if ($firebase_token[0]->firebase_ios != null || $firebase_token[0]->firebase_ios != '') {
                $url = 'https://fcm.googleapis.com/fcm/send';
                $registrationIds = array($firebase_token[0]->firebase_ios);
                     
                // prepare the message
                $fields = array (
                            'registration_ids' => $registrationIds,
                            "notification" => array(
                                "body" => $messages,
                                "title" =>$title,
                                "click_action" => 'FLUTTER_NOTIFICATION_CLICK'
                            ),
                            "data" => array("body" =>$messages,"title"=>"Notifikasi","tipe" => $tipe,'id_detail'=>$id_object)
                    );
                
                $headers = array (
                    'Authorization: key=' . "AAAA8xZHI9g:APA91bHJGuoF7n9MuI3PF8KxU5LeFvzpFZ_j4T1mT-3yieqlw1V0vwFycrgH2yqTq3L-nSAacudeFlzyVj26QS0oHpJdbDVlmEp29p4fUydp7KJJdg0CFvP6hM1956u_1eAd7dmK1iYu",
                    'Content-Type: application/json'
                );

                $ch = curl_init();
                curl_setopt( $ch,CURLOPT_URL,$url);
                curl_setopt( $ch,CURLOPT_POST,true);
                curl_setopt( $ch,CURLOPT_HTTPHEADER,$headers);
                curl_setopt( $ch,CURLOPT_RETURNTRANSFER,true);
                curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER,false);
                curl_setopt( $ch,CURLOPT_POSTFIELDS,json_encode($fields));
                $result = curl_exec($ch);
                curl_close($ch);
                //echo $result;
            }
        }
    }

    public function getAge($tgl) {
        if ($tgl != null) {
            $date1 = $tgl;
            $date2 = date('Y-m-d');
            $diff = abs(strtotime($date2) - strtotime($date1));
            $years = floor($diff / (365*60*60*24));
            $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
            $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
            if($years < 1){
                $data_umur = $months.' Bulan';
            } else {
                $data_umur = $years.','.$months.' Tahun';
            }
             //exit();
        } else {
            $data_umur = '(Belum diisi)';
        }
        
        $return = $data_umur;

        return $return;
    }

    public function GetColoumnValue($id,$table,$coloumn){
        $return = "";
        $data = DB::table($table)->where('id',$id)->pluck($coloumn);
        if ($data->isNotEmpty()) {
            $return = $data[0];
        }
        return $return;
    }

    public function BaseFile($id){
        $data = DB::table('t_aslis_files')->where('id_object',$id)->select('id','file','type')->get();
        if ($data->isNotEmpty()) {
            foreach ($data as $k => $v) {
                $data[$k]->file = env('BASE_IMG').$v->file;
            }
        }
        return $data;
    }

    public function FeeDelivery($id){
        $return = 0;
        $data = DB::table('t_shed_histories as h')->join('t_sheds as s','s.id','h.id_new_shed')->where('h.status',1)->where('id_cattle',$id)->pluck('delivery_fee');
        if ($data->isNotEmpty()) {
            $return = $data[0];
        }
        return $return;
    }

    public function ImageCattle($id){
        $return = 'https://souq.s3-id-jkt-1.kilatstorage.id/available_1654059656.png';
        $data = DB::table('t_aslis_files')->where('id_object',$id)->whereNull('deleted_at')->where('type',1)->pluck('file');
        if ($data->isNotEmpty()) {
            $return = env('BASE_IMG').$data[0];
        }
        return $return;
    }

    public function CityShed($id){
        $return = null;
        $data = DB::table('t_shed_histories as h')->join('t_sheds as s','s.id','h.id_new_shed')->where('h.status',1)->where('id_cattle',$id)->pluck('id_city');
        if ($data->isNotEmpty()) {
            $return = $data[0];
        }
        return $return;
    }

    public function MaxDistanceDelivery($id){
        $return = 0;
        $data = DB::table('t_shed_histories as h')->join('t_sheds as s','s.id','h.id_new_shed')->where('h.status',1)->where('id_cattle',$id)->pluck('maximum_distance');
        if ($data->isNotEmpty()) {
            $return = $data[0];
        }
        return $return;
    }
      
}

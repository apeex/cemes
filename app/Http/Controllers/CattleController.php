<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\CattleModel;
use App\Classes\upload;
//use App\Traits\admin_logs;
use Auth;
use App\Exceptions\Handler;
use Illuminate\Support\Str;
use App\Models\RoleModel;

class CattleController extends Controller
{
    public function index()
    {
        // dd(CattleModel::get());
        try {
            $data = parent::sidebar();
            $data['header_title'] = "Hewan Ternak";
            if ($data['access'] == 0) {
                return redirect('/');
            } else {
                $role_id          = Auth::guard('admin')->user()->id_role;
                $data['peternak'] = DB::table('t_users')->where('status',1)->select('id','user_name')->orderBy('user_name','asc')->get();
                $data['kandang']  = DB::table('t_sheds')->where('status',1)->select('id','shed_name')->orderBy('shed_name','asc')->get();
                return view('cattle.index', $data);
            }
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'CattleController@index';
            $insert_error = parent::InsertErrorSystem($data);
            $error = parent::sidebar();
            $error['id'] = $insert_error;
            return view('errors.index',$error); // jika Metode Get
            //return response()->json($data); // jika metode Post
        }
    }

    public function list_data(Request $request)
    {

        $limit = $request->input('length');
        $start = $request->input('start');
        $dir   = $request->input('order.0.dir');

        $posts = DB::table('t_cattles as c')->join('t_users as u','u.id','c.id_user')->whereNull('c.deleted_at')->orderBy('c.created_at','desc');

        $search = $request->input('search.value');

        if ($search) {
            $posts = $posts->where(function ($query) use ($search) {
                $query->where('user_name','ilike', "%{$search}%");
                $query->orWhere('user_phone','ilike', "%{$search}%");
                $query->orWhere('user_nik','ilike', "%{$search}%");
                $query->orWhere('cattle_name','ilike', "%{$search}%");
                $query->orWhere('cattle_id','ilike', "%{$search}%");
                $query->orWhere('weigth','ilike', "%{$search}%");
            });
        }

        if ($request->id_peternak != null) {
            $posts = $posts->where('c.id_user',$request->id_peternak);
        }

        if ($request->type != null) {
            $posts = $posts->where('c.type',$request->type);
        }

        if ($request->gender != null) {
            $posts = $posts->where('c.gender',$request->gender);
        }

        if ($request->kandang != null) {
            $in_array = DB::table('t_shed_histories')->where('status',1)->where('id_new_shed',$request->kandang)->pluck('id_cattle')->toArray();
            $posts = $posts->whereIn('c.id',$in_array);
        }

        $posts = $posts->select('c.*','user_name');

        $totalFiltered = $posts->count();
        $totalData = $totalFiltered;

        $posts = $posts->limit($limit)->offset($start)->get();

        $data = array();
        if (!empty($posts)) {
            $no = 0;
            foreach ($posts as $d) {
                $no = $no + 1;

                if ($d->status == 1) {
                    $status = '<div style="float: left; margin-left: 5px;"><a id="' . $d->id . '" aksi="nonaktif" tujuan="cattle" data="' . 'data_cattle' . '" class="btn btn-success btn-sm aksi">Aktif</a></div>';
                } else {
                    $status = '<div style="float: left; margin-left: 5px;"><a id="' . $d->id . '" aksi="aktif" tujuan="cattle" data="' . 'data_cattle' . '" class="btn btn-danger btn-sm aksi">Non Aktif</a></div>';
                }

                $action = '<a href="/cattle/list/'.base64_encode($d->id).'" class="btn btn-sm font-sm rounded btn-success me-2 d_detail"> <i class="material-icons md-edit"></i> Detail </a>';
                //delete
                // if ($d->slug != 'superadmin'){
                    $action .= '<a href="javascript:void(0)" class="btn btn-sm font-sm rounded  btn-danger aksi btn-aksi" id="' . $d->id . '" aksi="delete" tujuan="' . 'cattle' . '" data="' . 'data_cattle' . '"> <i class="material-icons md-delete"></i> Hapus </a>';
                //}
                if ($d->gender == 1) {
                    $gender = "Jantan";
                } elseif ($d->gender == 1) {
                    $gender = "Jantan";
                } else {
                    $gender = " - ";
                }
                $birthday = null;
                if ($d->birthday != null) {
                    $birthday = '<span>'.$d->birthday.'<br>('.getAge($d->birthday).')</span>';
                }
                $column['no']      = $no;
                $column['name']    = $d->cattle_name.' - '.$d->cattle_id;
                $column['farmer']  = $d->user_name;
                $column['weigth']   = $d->weigth.' Kg';
                $column['gender'] = $gender;
                $column['birthday'] = $birthday;
                $column['status']  = $status;
                $column['action']  = $action;
                $data[]            = $column;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data,
        );

        echo json_encode($json_data);

    }

    public function detail($id)
    {
        $id = base64_decode($id);
        $id = parent::cleanHazard($id);
        //dd($id);
        try {
            $data = parent::sidebar();
            $data['header_title'] = "Detail Data Pakan";
            //dd($data);
            if ($data['access'] == 0) {
                return redirect('/');
            } else {
                $role_id           = Auth::guard('admin')->user()->id_role;
                $data['data_role'] = RoleModel::whereNull('deleted_at')->where('status', 1)->get();
                $isUuid = Str::isUuid($id);
                if ($isUuid == true) {
                    $data['data'] = DB::table('t_cattles as s')->join('t_users as u','u.id','s.id_user')
                                ->where('s.id',$id)
                                ->select('s.*','user_name','user_email','user_nik','user_phone','u.birthday as ultah','u.gender as jk','profile')
                                ->first();
                    $data['shed'] = DB::table('t_sheds')->where('id_user',$data['data']->id_user)->where('status',1)->select('id','shed_name')->get();
                    $data['food'] = DB::table('t_foods')->where('id_user',$data['data']->id_user)->where('status',1)->select('id','food_name')->get();
                    return view('cattle.detail', $data);
                } else {
                    $data['error_message'] = "Data dengan ID tersebut tidak ditemukan";
                    $data['link_back'] = "/cattle/list";
                    return view('errors.empty_data',$data);
                }
            }
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'CattleController@detail';
            $insert_error = parent::InsertErrorSystem($data);
            $error = parent::sidebar();
            $error['id'] = $insert_error;
            return view('errors.index',$error); // jika Metode Get
            //return response()->json($data); // jika metode Post
        }
    }

    public function nonactive(Request $request)
    {
        try {
            $admin         = CattleModel::find($request->id);
            $admin->status = 0;
            $admin->save();
            if ($admin) {
                $data['code']    = 200;
                $data['message'] = 'Berhasil Menonaktifkan Data Kandang';
                $insert_log      = parent::LogAdmin(\Request::ip(),Auth::guard('admin')->user()->id,'Menon aktifkan Data Kandang '.$request->id.'','Kandang');
                return response()->json($data);
            } else {
                $data['code']    = 500;
                $data['message'] = 'Maaf Ada yang Error ';
                return response()->json($data);
            }
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'CattleController@nonaktif';
            $insert_error = parent::InsertErrorSystem($data);
            return response()->json($data); // jika metode Post
        }
    }

    public function active(Request $request)
    {
        try {
            $admin         = CattleModel::find($request->id);
            $admin->status = 1;
            $admin->save();
            if ($admin) {
                $data['code']    = 200;
                $data['message'] = 'Berhasil Mengaktifkan Data Kandang';
                $insert_log      = parent::LogAdmin(\Request::ip(),Auth::guard('admin')->user()->id,'Mengaktifkan Data Kandang '.$request->id.'','Kandang');
                return response()->json($data);
            } else {
                $data['code']    = 500;
                $data['message'] = 'Maaf Ada yang Error ';
                return response()->json($data);
            }
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'CattleController@active';
            $insert_error = parent::InsertErrorSystem($data);
            return response()->json($data); // jika metode Post
        }
    }

    public function delete(Request $request)
    {
        try {
            $admin             = CattleModel::find($request->id);
            //dd($admin);
            $admin->status     = 0;
            $admin->deleted_at = date('Y-m-d');
            $admin->save();
            //dd("berhasil");

            if ($admin) {
                $data['code']    = 200;
                $data['message'] = 'Berhasil Menghapus Data Kandang';
                $insert_log      = parent::LogAdmin(\Request::ip(),Auth::guard('admin')->user()->id,'Menghapus Data Kandang '.$request->id.'','Kandang');
                return response()->json($data);
            } else {
                $data['code']    = 500;
                $data['message'] = 'Maaf Ada yang Error ';
                return response()->json($data);
            }
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'CattleController@delete';
            $insert_error = parent::InsertErrorSystem($data);
            return response()->json($data); // jika metode Post
        }
    }

    public function list_food(Request $request){
        $limit = $request->input('length');
        $start = $request->input('start');
        $dir   = $request->input('order.0.dir');

        $posts = DB::table('t_eat_histories as e')->join('t_sheds as s','s.id','e.id_shed')->join('t_foods as f','f.id','e.id_food')->join('t_cattles as c','c.id','e.id_cattle')->where('e.id_cattle',$request->id_cattle)->orderBy('e.created_at','desc');

        $search = $request->input('search.value');

        if ($request->tanggal != null) {
            $posts = $posts->where('e.date',$request->tanggal);
        }

        if ($request->id_shed != null) {
            $posts = $posts->where('e.id_shed',$request->id_shed);
        }

        if ($request->id_food != null) {
            $posts = $posts->where('e.id_food',$request->id_food);
        }

        if ($search) {
            $posts = $posts->where(function ($query) use ($search) {
                $query->where('shed_name','ilike', "%{$search}%");
                $query->orWhere('shed_id','ilike', "%{$search}%");
                $query->orWhere('food_id','ilike', "%{$search}%");
                $query->orWhere('food_name','ilike', "%{$search}%");
            });
        }
        $posts = $posts->select('e.*','food_name','shed_name');

        $totalFiltered = $posts->count();
        $totalData = $totalFiltered;

        $posts = $posts->limit($limit)->offset($start)->get();

        $data = array();
        if (!empty($posts)) {
            $no = 0;
            foreach ($posts as $d) {
                $no = $no + 1;
                $column['no']      = $no;
                $column['food']    = $d->food_name;
                $column['shed']    = $d->shed_name;
                $column['weigth']  = $d->weigth.' Kg';
                $column['date']    = date('d-m-Y',strtotime($d->date));
                $data[]            = $column;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data,
        );

        echo json_encode($json_data);
    }

    public function list_weigth(Request $request){
        $limit = $request->input('length');
        $start = $request->input('start');
        $dir   = $request->input('order.0.dir');

        $posts = DB::table('t_weigth_histories as e')->join('t_sheds as s','s.id','e.id_shed')->join('t_cattles as c','c.id','e.id_cattle')->where('e.id_cattle',$request->id_cattle)->orderBy('e.created_at','desc');

        $search = $request->input('search.value');

        if ($request->tanggal != null) {
            $posts = $posts->where('e.date',$request->tanggal);
        }

        if ($request->id_shed != null) {
            $posts = $posts->where('e.id_shed',$request->id_shed);
        }

        if ($search) {
            $posts = $posts->where(function ($query) use ($search) {
                $query->where('shed_name','ilike', "%{$search}%");
                $query->orWhere('shed_id','ilike', "%{$search}%");
            });
        }
        $posts = $posts->select('e.*','shed_name');

        $totalFiltered = $posts->count();
        $totalData = $totalFiltered;

        $posts = $posts->limit($limit)->offset($start)->get();

        $data = array();
        if (!empty($posts)) {
            $no = 0;
            foreach ($posts as $d) {
                $no = $no + 1;
                $column['no']      = $no;
                $column['shed']    = $d->shed_name;
                $column['weigth']  = $d->weigth.' Kg';
                $column['date']    = date('d-m-Y',strtotime($d->date));
                $data[]            = $column;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data,
        );

        echo json_encode($json_data);
    }

    public function list_shed(Request $request){
        $limit = $request->input('length');
        $start = $request->input('start');
        $dir   = $request->input('order.0.dir');

        $posts = DB::table('t_shed_histories as e')->join('t_cattles as c','c.id','e.id_cattle')->where('e.id_cattle',$request->id_cattle)->orderBy('e.created_at','desc');

        $search = $request->input('search.value');

        if ($request->tanggal != null) {
            $posts = $posts->where('e.date',$request->tanggal);
        }

        if ($request->id_shed != null) {
            $posts = $posts->where(function ($query) use ($request) {
                $query->where('id_old_shed',$request->id_shed);
                $query->orWhere('id_new_shed',$request->id_shed);
            });
        }

        if ($search) {
            $posts = $posts->where(function ($query) use ($search) {
                $query->where('shed_name','ilike', "%{$search}%");
                $query->orWhere('shed_id','ilike', "%{$search}%");
            });
        }
        $posts = $posts->select('e.*');

        $totalFiltered = $posts->count();
        $totalData = $totalFiltered;

        $posts = $posts->limit($limit)->offset($start)->get();

        $data = array();
        if (!empty($posts)) {
            $no = 0;
            foreach ($posts as $d) {
                $no = $no + 1;
                $column['no']      = $no;
                $column['old_shed']= parent::GetColoumnValue($d->id_old_shed,'t_sheds','shed_name');
                $column['new_shed']= parent::GetColoumnValue($d->id_new_shed,'t_sheds','shed_name');
                $column['date']    = date('d-m-Y',strtotime($d->date));
                $column['description']  = $d->description;
                $data[]            = $column;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data,
        );

        echo json_encode($json_data);
    }
}
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\Fungsi;
use App\Classes\upload;
use App\Model\CategoryShop;
use App\Model\CabangModel;
use App\Model\RoleModel;
use Auth;
use DB;
use Redirect;
use App\Exceptions\Handler;
use Illuminate\Support\Str;
class CategoryShopController extends Controller
{
    public function index()
    {
        try {
            $role_id = Auth::guard('admin')->user()->role_id;
            $data = parent::sidebar();
            if ($data['access'] == 0) {
                return redirect('/');
            } else {
                $data['name_adm'] = Auth::guard('admin')->user()->name;
                $nama_role = RoleModel::where('id',$role_id)->pluck('name');

                return view('category_shop.index', $data);
            }
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'CategoryShopController@index';
            $insert_error = parent::InsertErrorSystem($data);
            $error = parent::sidebar();
            $error['id'] = $insert_error;
            return view('errors.index',$error); // jika Metode Get
            //return response()->json($data); // jika metode Post
        }
        
    }

    public function list_data(Request $request)
    {
        $category = $this->AtDataThis($request);
        if ($request->sort == 0) {
            $category = $category->orderBy('category_shop_name','asc');
        }
        if ($request->sort == 1) {
            $category = $category->orderBy('category_shop_name','desc');
        }
        if ($request->sort == 2) {
            $category = $category->orderBy('created_at','asc');
        }
        if ($request->sort == 3) {
            $category = $category->orderBy('created_at','desc');
        }

        $category = $category->select('*');
        $limit = $request->input('length');
        $start = $request->input('start');
        $dir   = $request->input('order.0.dir');

        $totalFiltered = $category->count();
        $totalData = $category->count();

        $category = $category->limit($limit)->offset($start)->get();

        $data = array();
        $no   = 0;
        foreach ($category as $d) {
            $ids = "'select_txt_".$d->id."'";
            if ($d->status == 1) {
                $status = '<div style="float: left; margin-left: 5px;">
                    <button type="button" id="' . $d->id . '" aksi="nonaktif" tujuan="category_shop" data="data_category_shop" class="btn btn-success btn-sm aksi">Aktif</button>
                    </div>';
            } else {
                $status = '<div style="float: left; margin-left: 5px;">
                    <button type="button" id="' . $d->id . '" data="data_category_shop" aksi="aktif" tujuan="category_shop" class="btn btn-danger btn-sm aksi">Tidak Aktif</button>
                    </div>';
            }

            $icon = " - ";
            if ($d->category_shop_icon != null) {
                $icon = '<img src="https://kandidat.s3-id-jkt-1.kilatstorage.id/'.$d->category_shop_icon.'" style="width : 100px;height:100px">';
            }

            $action = '<a href="/shop/category-shop/'.base64_encode($d->id).'" class="btn btn-success btn-sm font-sm me-2"><span class="navi-icon"><i class="fas fa-edit"></i></span><span class="navi-text">Edit</span></a>';
            $action .= '<a href="javascript:void(0)" class="btn btn-danger ml-2 btn-sm font-sm aksi btn-aksi" id="' . $d->id.'" aksi="delete" tujuan="category_shop" data="' . 'data_category_shop' . '"><span class="navi-icon"><i class="fa fa-trash"></i></span><span class="navi-text">Hapus</span></a>';
            $no     = $no + 1;
            $column['no']          = $no;
            $column['category']      = $d->category_shop_name;
            $column['jumlah']      = "4";
            $column['icon'] = $icon;
            $column['status'] = $status;
            $column['action']   = $action;
            $data[]              = $column;
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data,
        );

        echo json_encode($json_data);
    }

    public function AtDataThis($request){
        $posts = CategoryShop::whereNull('deleted_at')->where(function ($query) use ($request) {
            $search = $request->search;
            if (isset($request->search)) {
                if ($request->search != null) {
                    $query->where(static function ($query2) use ($search) {
                        $query2->where('category_shop_name','ilike', "%{$search}%");
                    });
                } 
            }
        });

        return $posts;
                    
    }

    public function post(Request $request)
    {
        try {
            $input = $request->all();
            if ($request->file('icon')) {
                $upload = new upload();
                $profile  = $upload->img2($request->icon);
            } else {
                $profile = null;
            }
            $lower = strtolower($request->name);
            $slug  = preg_replace('/\s+/', '-', $lower);
            $id_admin    = Auth::guard('admin')->user()->id;
            $insertadmin = array(
                'category_shop_name' => $request->name,
                'description' => $request->description,
                'category_shop_icon' => $profile,
                'slug' => $slug,
                'status' => 1, 
                'created_at' => date('Y-m-d H:i:s'), 
                'updated_at' => date('Y-m-d H:i:s'),
            );
            $insert = CategoryShop::insertGetId($insertadmin);
            if ($insert) {
                $insert_log      = parent::LogAdmin(\Request::ip(),Auth::guard('admin')->user()->id,'Menambah Data Kategori Shop'.$request->name.'','Kategori Shop');
                $data['code']    = 200;
                $data['message'] = 'Berhasil menambah Data Kategori Shop';
                return response()->json($data);
            } else {
                $data['code']    = 500;
                $data['message'] = 'Maaf Ada yang Error ';
                return response()->json($data);
            }
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'CategoryShopController@post';
            $insert_error = parent::InsertErrorSystem($data);
            return response()->json($data); // jika metode Post
        }
    }

    public function detail($id)
    {
        $id = base64_decode($id);
        $id = parent::cleanHazard($id);
        //dd($id);
        try {
            $data = parent::sidebar();
            $data['header_title'] = "Edit Data Kategori Shop";
            $query = CategoryShop::find($id);

            $isUuid = Str::isUuid($id);
            if ($isUuid == true) {
                
                $data['data']       = $query;
                
                return view('category_shop.dialog_edit', $data);
            } else {
                $data['error_message'] = "Data dengan ID tersebut tidak ditemukan";
                $data['link_back'] = "/shop/category-shop";
                return view('errors.empty_data',$data);
            }
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'CategoryShopController@detail';
            $insert_error = parent::InsertErrorSystem($data);
            $error = parent::sidebar();
            $error['id'] = $insert_error;
            return view('errors.index',$error); // jika Metode Get
            //return response()->json($data); // jika metode Post
        }
    }

    public function nonactive(Request $request)
    {
        try {
            $nonactive         = CategoryShop::find($request->id);
            $nonactive->status = 0;
            $nonactive->save();

            if ($nonactive) {
                $id_admin    = Auth::guard('admin')->user()->id;
                $insert_log      = parent::LogAdmin(\Request::ip(),Auth::guard('admin')->user()->id,'Memblokir Data Kategori Shop = '.$request->id.'','Kategori Shop');
                $data['code']    = 200;
                $data['message'] = 'Berhasil Memblokir Data Kategori Shop';
                return response()->json($data);
            } else {
                $data['code']    = 500;
                $data['message'] = 'Maaf Ada yang Error ';
                return response()->json($data);
            }
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'CategoryShopController@nonactive';
            $insert_error = parent::InsertErrorSystem($data);
            
            return response()->json($data); // jika metode Post
        }
    }

    public function active(Request $request)
    {
        try {
            $nonactive         = CategoryShop::find($request->id);
            $nonactive->status = 1;
            $nonactive->save();

            if ($nonactive) {
                $id_admin    = Auth::guard('admin')->user()->id;
                $insert_log      = parent::LogAdmin(\Request::ip(),Auth::guard('admin')->user()->id,'Mengaktifkan Kembali Data Kategori Shop = '.$request->id.'','Kategori Shop');
                $data['code']    = 200;
                $data['message'] = 'Berhasil Membuka Blokir Data Kategori Shop';
                return response()->json($data);
            } else {
                $data['code']    = 500;
                $data['message'] = 'Maaf Ada yang Error ';
                return response()->json($data);
            }
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'CategoryShopController@active';
            $insert_error = parent::InsertErrorSystem($data);
            
            return response()->json($data); // jika metode Post
        }
    }

    public function delete(Request $request)
    {
        try {
            $delete             = CategoryShop::find($request->id);
            $delete->status     = 0;
            $delete->deleted_at = date('Y-m-d');
            $delete->save();

            if ($delete) {
                $id_admin    = Auth::guard('admin')->user()->id;
                $insert_log      = parent::LogAdmin(\Request::ip(),Auth::guard('admin')->user()->id,'Menghapus Data Kategori Shop = '.$request->id.'','Kategori Shop');
                $data['code']    = 200;
                $data['message'] = 'Berhasil Menghapus Data Kategori Shop';
                return response()->json($data);
            } else {
                $data['code']    = 500;
                $data['message'] = 'Maaf Ada yang Error ';
                return response()->json($data);
            }
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'CategoryShopController@delete';
            $insert_error = parent::InsertErrorSystem($data);
            return response()->json($data); // jika metode Post
        }
    }

    public function update(Request $request)
    {
        try {
            $lower = strtolower($request->name);
            $slug  = preg_replace('/\s+/', '-', $lower);
            $id_admin    = Auth::guard('admin')->user()->id;
            $data_category = CategoryShop::where('id', $request->id)->first(); 
            if ($request->file('icon')) {
                $upload = new upload();
                $profile  = $upload->img2($request->icon);
            } else {
                $profile = $data_category->category_shop_icon;
            }
            $insertadmin = array(
                'category_shop_name' => $request->name,
                'description' => $request->description,
                'category_shop_icon' => $profile,
                'slug' => $slug,
                'updated_at' => date('Y-m-d H:i:s'),
            );
            $update = CategoryShop::where('id', $request->id)->update($insertadmin);
            if ($update) {
                $insert_log      = parent::LogAdmin(\Request::ip(),Auth::guard('admin')->user()->id,'Mengupdate Data Kategori Shop '.$request->name.'','Kategori Shop');
                $data['code']    = 200;
                $data['message'] = 'Berhasil Mnegupdate Data Kategori Shop';
                return response()->json($data);
            } else {
                $data['code']    = 500;
                $data['message'] = 'Maaf Ada yang Error ';
                return response()->json($data);
            }
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'CategoryShopController@update';
            $insert_error = parent::InsertErrorSystem($data);
            return response()->json($data); // jika metode Post
        }
    }
}
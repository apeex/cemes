<?php

namespace App\Http\Controllers;

use App\Models\BannerModel;
use App\Models\RoleModel;
use App\Traits\Fungsi;
use App\Classes\upload;
use Auth;
use DB;
use Illuminate\Http\Request;
use Redirect;
use App\Exceptions\Handler;
use Illuminate\Support\Str;

class TransactionController extends Controller
{
    
    public function index()
    {
        // dd("Ok");
        try {
            $role_id = Auth::guard('admin')->user()->id_role;
            $data = parent::sidebar();
            if ($data['access'] == 0) {
                return redirect('/');
            } else {
                $data['name_adm'] = Auth::guard('admin')->user()->name;
                return view('transaction.index',$data);
            }

        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'TransactionController@index';
            $insert_error = parent::InsertErrorSystem($data);
            $error = parent::sidebar();
            $error['id'] = $insert_error;
            return view('errors.index',$error); // jika Metode Get
            //return response()->json($data); // jika metode Post
        }
    }

    public function list(Request $request)
    {

        $limit = $request->input('length');
        $start = $request->input('start');
        $dir   = $request->input('order.0.dir');
        $search = $request->search;

        $posts = DB::table('t_transactions')->whereNull('deleted_at')->where(function ($query) use ($search,$request) {
                if ($search != null) {
                    $query->where('no_invoice','ilike', "%{$search}%");
                } 

                if ($request->status != null) {
                    $query->where('status',$request->status);
                } 

                if ($request->start != null) {
                    $query->whereDate('created_at','>=',$request->start);
                } 

                if ($request->end != null) {
                    $query->whereDate('created_at','<=',$request->end);
                } 

            })->orderBy('created_at','desc')->select('*');
        $totalFiltered = $posts->count();
        $totalData = $totalFiltered;
        $posts = $posts->limit($limit)->offset($start)->get();

        $data = array();
        if (!empty($posts)) {

            $no = 0;
           
            foreach ($posts as $d) {
                $no = $no + 1;
                
                $action = '<div style="float: left; margin-left: 5px;"><a href="/transaction/list/'.base64_encode($d->id).'">
                    <button type="button" id="' . $d->id . '" class="btn btn-warning btn-sm font-sm edit_data" style="min-width: 110px;margin-left: 2px;margin-top:3px;text-align:left"><i class="fa fa-eye"></i> Detail</button></a>
                </div>';
                
                if($d->status == 0) {
                    $status = '<span class="text-info">Transaksi Belum Diselesaikan</span>';
                } else if($d->status == 1) {
                    $status = '<span class="text-dark">Belum Dibayar</span>';
                } else if($d->status == 2) {
                    $status = '<span class="text-success">Sudah Dibayar</span>';
                } else if($d->status == 3) {
                    $status = '<span class="text-info">Sedang Diproses</span>';
                } else if($d->status == 4){
                    $status = '<span class="text-warning">Sedang Dikirim</span>';
                } else {
                    $status = '<span class="text-danger">Pesanan Selesai</span>';
                }

                $nestedData['no']           = $no;
                $nestedData['no_invoice']   = $d->no_invoice;
                $nestedData['date']       = date('d F Y H:i',strtotime($d->created_at));
                $nestedData['total']   = 'Rp '.number_format($d->total_of_pay);
                $nestedData['status']       = $status;
                $nestedData['action']       = $action;
                $data[]                     = $nestedData;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data,
        );
        echo json_encode($json_data);


    }

   
    public function detail($id)
    {
        $id = base64_decode($id);
        $id = parent::cleanHazard($id);
        //dd($id);
        try {
            $data = parent::sidebar();
            $data['header_title'] = "Detail Transaksi";
            $data['transaction'] = DB::table('t_transactions as t')->where('t.id',$id)->join('t_users as u','u.id','t.id_user')->select('t.*','user_name','user_phone','pos_code','address','id_city')->first();
            $data['data'] = DB::table('t_detail_transactions as c')->join('t_cattles as p','p.id','c.id_cattle')->join('t_users as u','u.id','p.id_user')->where('c.id_transaction',$id)->select('u.id','user_name','u.profile','u.user_phone')->groupBy('u.id','user_name','u.profile','u.user_phone')->get();
            if ($data['data']->isNotEmpty()) {
                $code = 200;
                foreach ($data['data'] as $k => $v) {
                    $data['data'][$k]->product = DB::table('t_detail_transactions as c')->join('t_cattles as p','p.id','c.id_cattle')->join('t_users as u','u.id','p.id_user')->where('c.id_transaction',$id)->where('u.id',$v->id)->select('c.id as id_cart','p.*','id_cattle','delivery_type')->get();
                    
                    foreach ($data['data'][$k]->product as $key => $value) {
                        
                        $data['data'][$k]->product[$key]->image =  parent::ImageCattle($value->id_cattle);
                        $data['data'][$k]->product[$key]->age = parent::getAge($value->birthday);
                        $data['data'][$k]->product[$key]->fee_delivery = parent::FeeDelivery($value->id_cattle);
                        $data['data'][$k]->product[$key]->max_distance = parent::MaxDistanceDelivery($value->id_cattle);
                    }
                    
                }
                
            }
                
            return view('transaction.detail', $data);
            
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'TransactionController@detail';
            $insert_error = parent::InsertErrorSystem($data);
            $error = parent::sidebar();
            $error['id'] = $insert_error;
            return view('errors.index',$error); // jika Metode Get
            //return response()->json($data); // jika metode Post
        }
    }

    public function update(Request $request) {
        try {
            $update = DB::table('t_transactions')->where('id',$request->id)->update(['status'=>$request->status]);
            $data['code']    = 200;
            $data['message'] = "Berhasil Mengubah status transaksi";
            return json_encode($data);
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'TransactionController@update';
            $insert_error = parent::InsertErrorSystem($data);
            return response()->json($data); // jika metode Post
        }

    }

}

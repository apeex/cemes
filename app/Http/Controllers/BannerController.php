<?php

namespace App\Http\Controllers;

use App\Models\BannerModel;
use App\Models\RoleModel;
use App\Traits\Fungsi;
use App\Classes\upload;
use Auth;
use DB;
use Illuminate\Http\Request;
use Redirect;
use App\Exceptions\Handler;
use Illuminate\Support\Str;

class BannerController extends Controller
{
    
    public function index()
    {
        // dd("Ok");
        try {
            $role_id = Auth::guard('admin')->user()->id_role;
            $data = parent::sidebar();
            if ($data['access'] == 0) {
                return redirect('/');
            } else {
                $data['name_adm'] = Auth::guard('admin')->user()->name;
                $data['product']  = DB::table('t_cattles')->where('status',1)->get();
                return view('master.banner.index',$data);
            }

        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'BannerController';
            $insert_error = parent::InsertErrorSystem($data);
            $error = parent::sidebar();
            $error['id'] = $insert_error;
            return view('errors.index',$error); // jika Metode Get
            //return response()->json($data); // jika metode Post
        }
    }

    public function get_type_banner($id){
        if ($id == '1') {
            $data = DB::table('t_cattles')->where('status',1)->select('id','cattle_name as name')->get();
        } else if ($id == '2') {
            $data = DB::table('t_sheds')->where('status',1)->select('id','shed_name as name')->get();
        } else if ($id == '3') {
            $data = DB::table('t_foods')->where('status',1)->select('id','food_name as name')->get();
        } else  {
            $data = DB::table('t_events')->where('status',1)->select('id','event_name as name')->get();
        }

        return json_encode($data);
    }

    public function list_data(Request $request)
    {

        $totalData = BannerModel::whereNull('deleted_at')->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $dir   = $request->input('order.0.dir');
        $search = $request->input('search.value');

        $posts = BannerModel::whereNull('deleted_at')->where(function ($query) use ($search,$request) {
                if ($search != null) {
                    $query->where('type','ilike', "%{$search}%");
                } 
            })->orderBy('created_at','desc')->select('*');
        $totalFiltered = $posts->count();
        $posts = $posts->limit($limit)->offset($start)->get();

        $data = array();
        if (!empty($posts)) {

            $no = 0;
           
            foreach ($posts as $d) {
                $no = $no + 1;
                $cek_role = parent::admin_data();
                if ($cek_role['role'] == 'viewer') {
                    $status = " - ";
                } else {
                    if ($d->status == 1) {
                        $status = '<div style="float: left; margin-left: 5px;">
                            <a id="'.$d->id.'" aksi="nonaktif" tujuan="banner" data="data_banner" class="btn btn-success btn-sm font-sm aksi">Aktif</a>
                            </div>';
                    } else {
                        $status = '<div style="float: left; margin-left: 5px;">
                            <button type="button" id="' . $d->id . '"data="data_banner" aksi="aktif" tujuan="banner" class="btn btn-danger btn-sm font-sm aksi">Tidak Aktif</button>
                            </div>';
                    }
                }
                if ($cek_role['role'] == 'viewer') {
                    $action = " - ";
                } else {
                    $action = '<div style="float: left; margin-left: 5px;"><a href="/master/banner/'.base64_encode($d->id).'">
                        <button type="button" id="' . $d->id . '" class="btn btn-warning btn-sm font-sm edit_data" style="min-width: 110px;margin-left: 2px;margin-top:3px;text-align:left"><i class="fa fa-edit"></i> Edit</button></a>
                    </div>
                    <div style="float: left; margin-left: 5px;">
                        <button type="button" class="btn btn-danger btn-sm font-sm aksi btn-aksi" data="data_banner"  id="' . $d->id . '" aksi="delete" tujuan="banner" style="min-width: 110px;margin-left: 2px;margin-top:3px;text-align:left"><i class="fa fa-trash"></i> Hapus</button>
                    </div>';
                }

                $foto = " - ";
                if ($d->photo != null) {
                    $foto = '<img src="'.env('BASE_IMG').$d->photo.'" style="width : 180px;height:80px">';
                }

                if ($d->type == 1) {
                    $tipe = "Hewan Ternak";
                } else if ($d->type == 2){
                    $tipe = "kandang";
                } else if ($d->type == 3){
                    $tipe = "Makanan";
                } else {
                    $tipe = "Event";
                }

                $nestedData['no']           = $no;
                $nestedData['tipe']         = $tipe;
                $nestedData['banner']       = $foto;
                $nestedData['status']       = $status;
                $nestedData['action']       = $action;
                $data[]                     = $nestedData;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data,
        );
        echo json_encode($json_data);


    }

    public function post(Request $request)
    {
        try {
            $input      = $request->all();
            //$profile = null;
            if ($request->file('profile')) {
                $profile  = parent::uploadFileS3($request->file('profile'));
            } else {
                $profile = null;
            }
            $id_admin    = Auth::guard('admin')->user()->id;
            $insertlog=array(
                            'photo'  => $profile
                            ,'type' => $request->type
                            ,'id_detail' => $request->id_detail
                            ,'id_admin' => $id_admin
                            ,'status' => 1
                            ,'created_at'=>date('Y-m-d H:i:s')
                            ,'updated_at'=>date('Y-m-d H:i:s')
                            );
            $insert = BannerModel::insert($insertlog);
            if ($insert) {
                $insert_log      = parent::LogAdmin(\Request::ip(),Auth::guard('admin')->user()->id,'Menambah Data Banner'.$request->title.'','banner');
                $data['code']    = 200;
                $data['message'] = 'Berhasil menambah Data Banner';
                return response()->json($data);
            } else {
                $data['code']    = 500;
                $data['message'] = 'Maaf Ada yang Error ';
                return response()->json($data);
            }
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'BannerController@posts';
            $insert_error = parent::InsertErrorSystem($data);
            return response()->json($data); // jika metode Post
        }
    }

   
    public function detail($id)
    {
        $id = base64_decode($id);
        $id = parent::cleanHazard($id);
        //dd($id);
        try {
            $data = parent::sidebar();
            $data['header_title'] = "Edit Data Banner";
            $query = BannerModel::find($id);

            $isUuid = Str::isUuid($id);
            if ($isUuid == true) {

                if ($query->type == '1') {
                    $data['product'] = DB::table('t_cattles')->where('status',1)->select('id','cattle_name as name')->get();
                } else if ($query->type == '2') {
                    $data['product'] = DB::table('t_sheds')->where('status',1)->select('id','shed_name as name')->get();
                } else if ($query->type == '3') {
                    $data['product'] = DB::table('t_foods')->where('status',1)->select('id','food_name as name')->get();
                } else  {
                    $data['product'] = DB::table('t_events')->where('status',1)->select('id','event_name as name')->get();
                }
                
                $data['data']       = $query;
                
                return view('master.banner.dialog_edit', $data);
            } else {
                $data['error_message'] = "Data dengan ID tersebut tidak ditemukan";
                $data['link_back'] = "/master/banner";
                return view('errors.empty_data',$data);
            }
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'BannerController@detail';
            $insert_error = parent::InsertErrorSystem($data);
            $error = parent::sidebar();
            $error['id'] = $insert_error;
            return view('errors.index',$error); // jika Metode Get
            //return response()->json($data); // jika metode Post
        }
    }

    public function nonactive(Request $request)
    {
        try {
            $nonactive         = BannerModel::find($request->id);
            $nonactive->status = 0;
            $nonactive->save();

            if ($nonactive) {
                $id_admin    = Auth::guard('admin')->user()->id;
                $insert_log      = parent::LogAdmin(\Request::ip(),Auth::guard('admin')->user()->id,'Menon aktifkan Data Banner '.$request->id.'','Banner');
                $data['code']    = 200;
                $data['message'] = 'Berhasil Memblokir Data Banner';
                return response()->json($data);
            } else {
                $data['code']    = 500;
                $data['message'] = 'Maaf Ada yang Error ';
                return response()->json($data);
            }
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'BannerController@nonactive';
            $insert_error = parent::InsertErrorSystem($data);
            return response()->json($data); // jika metode Post
        }
    }

    public function active(Request $request)
    {
        try {
            $active         = BannerModel::find($request->id);
            $active->status = 1;
            $active->save();

            if ($active) {
                $id_admin    = Auth::guard('admin')->user()->id;
                $insert_log      = parent::LogAdmin(\Request::ip(),Auth::guard('admin')->user()->id,'Mengaktifkan Data Banner '.$request->id.'','Banner');
                $data['code']    = 200;
                $data['message'] = 'Berhasil Mengaktifkan Data Banner';
                return response()->json($data);
            } else {
                $data['code']    = 500;
                $data['message'] = 'Maaf Ada yang Error ';
                return response()->json($data);
            }
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'BannerController@active';
            $insert_error = parent::InsertErrorSystem($data);
            
            return response()->json($data); // jika metode Post
        }
    }

    public function delete(Request $request)
    {
        // $delete             = BannerModel::where('id',$request->id)->get();
        try {
            $delete             = BannerModel::find($request->id);
            $delete->status     = 0;
            $delete->deleted_at = date('Y-m-d H:i:s');
            $delete->save();

            if ($delete) {
                $insert_log      = parent::LogAdmin(\Request::ip(),Auth::guard('admin')->user()->id,'Menghapus Data Banner '.$request->id.'','Banner');
                $data['code']    = 200;
                $data['message'] = 'Berhasil Menghapus Data Banner';
                return response()->json($data);
            } else {
                $data['code']    = 500;
                $data['message'] = 'Maaf Ada yang Error ';
                return response()->json($data);
            }
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'BannerController@delete';
            $insert_error = parent::InsertErrorSystem($data);
            return response()->json($data); // jika metode Post
        }
    }

    public function update(Request $request)
    {
        try {
            $input       = $request->all();
            $lower = strtolower($request->name);
            $slug  = preg_replace('/\s+/', '-', $lower);
            $id_admin    = Auth::guard('admin')->user()->id;
            $data_banner = BannerModel::where('id',$request->id)->get();
            if ($request->file('profile')) { 
                $icon  = parent::uploadFileS3($request->file('profile'));
            } else {
                $icon = $data_banner[0]->photo;
            }
            
            $insertadmin = array(
                'photo'  => $icon
                ,'type' => $request->type
                ,'id_detail' => $request->id_detail
                ,'id_admin' => $id_admin
                ,'updated_at'=>date('Y-m-d H:i:s')
            );
            $insert = BannerModel::where('id', $request->id)->update($insertadmin);
            if ($insert) {
                $insert_log      = parent::LogAdmin(\Request::ip(),Auth::guard('admin')->user()->id,'Mengupdate Data Banner '.$request->name.'','Banner');
                $data['code']    = 200;
                $data['message'] = 'Berhasil Mengupdate Data Country';
                return response()->json($data);
            } else {
                $data['code']    = 500;
                $data['message'] = 'Maaf Ada yang Error ';
                return response()->json($data);
            }
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'BannerController@update';
            $insert_error = parent::InsertErrorSystem($data);
            return response()->json($data); // jika metode Post
        }
    }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\ProblemModel;
use App\Classes\upload;
//use App\Traits\admin_logs;
use Auth;
use App\Exceptions\Handler;
use Illuminate\Support\Str;
use App\Models\RoleModel;

class ProblemController extends Controller
{
    public function index()
    {
        // dd(ProblemModel::get());
        try {
            $data = parent::sidebar();
            $data['header_title'] = "Master Problem";
            if ($data['access'] == 0) {
                return redirect('/');
            } else {
                $role_id           = Auth::guard('admin')->user()->id_role;
                return view('problem.index', $data);
            }
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'ProblemController@index';
            $insert_error = parent::InsertErrorSystem($data);
            $error = parent::sidebar();
            $error['id'] = $insert_error;
            return view('errors.index',$error); // jika Metode Get
            //return response()->json($data); // jika metode Post
        }
    }

    public function post(Request $request)
    {
        try {
            $data = $request->validate([
                'problem' => 'required',
            ]);

            $datas = [
                "problem" => $request->nama_category,
                "status" => 1,
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s'),
            ];

            $id_cat = DB::table('t_master_problems')->insertGetId($datas);
            $insert_log      = parent::LogAdmin(\Request::ip(),Auth::guard('admin')->user()->id,'Menambah Data Problem '.$request->nama_category.'','Problem');
            $data['code']    = 200;
            $data['message'] = 'Berhasil Menambah Data Problem';

            return response()->json($data);
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'ProblemController@post';
            $insert_error = parent::InsertErrorSystem($data);
            return response()->json($data); // jika metode Post
        }
    }

    public function list_data(Request $request)
    {
        
        $totalData = ProblemModel::whereNull('deleted_at')->count();

        $limit = $request->input('length');
        $start = $request->input('start');
        $dir   = $request->input('order.0.dir');

        $posts = DB::table('t_master_problems')->whereNull('deleted_at')->orderBy('created_at','desc');

        $search = $request->input('search.value');

        if ($search) {
            $posts = $posts->where('problem', 'ilike', "%{$search}%");
        }

        $totalFiltered = $posts->count();

        $posts = $posts->limit($limit)->offset($start)->get();

        $data = array();
        if (!empty($posts)) {
            $no = 0;
            foreach ($posts as $d) {
                $no = $no + 1;

                if ($d->status == 1) {
                    $status = '<div style="float: left; margin-left: 5px;"><a id="' . $d->id . '" aksi="nonaktif" tujuan="problem" data="' . 'data_problem' . '" class="btn btn-success btn-sm aksi">Aktif</a></div>';
                } else {
                    $status = '<div style="float: left; margin-left: 5px;"><a id="' . $d->id . '" aksi="aktif" tujuan="problem" data="' . 'data_problem' . '" class="btn btn-danger btn-sm aksi">Non Aktif</a></div>';
                }

                $action = '<a href="/master/problems/'.base64_encode($d->id).'" class="btn btn-sm font-sm rounded btn-success me-2 d_detail"> <i class="material-icons md-edit"></i> Edit </a>';
                //delete
                //if ($d->slug != 'superadmin'){
                    $action .= '<a href="javascript:void(0)" class="btn btn-sm font-sm rounded  btn-danger aksi btn-aksi" id="' . $d->id . '" aksi="delete" tujuan="' . 'problem' . '" data="' . 'data_problem' . '"> <i class="material-icons md-delete"></i> Hapus </a>';
                //}
                // $icon = "  ";
                // if ($d->icon != null) {
                //     $icon = '<img src="'.env('BASE_IMG').$d->icon.'" style="height:50px">';
                // }
                $column['no']      = $no;
                $column['problem'] = $d->problem;
                $column['total']   = $this->TotalProblem($d->id);
                $column['status']  = $status;
                $column['action']  = $action;
                $data[]            = $column;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data,
        );

        echo json_encode($json_data);

    }

    public function TotalProblem($id){
        
        $data = DB::table('t_notes')->where('id_problem',$id)->count();
        return $data;
    }

    public function detail($id)
    {
        $id = base64_decode($id);
        $id = parent::cleanHazard($id);
        //dd($id);
        try {
            $data = parent::sidebar();
            $data['header_title'] = "Edit Data Problem";
            //dd($data);
            if ($data['access'] == 0) {
                return redirect('/');
            } else {
                $role_id           = Auth::guard('admin')->user()->id_role;
                $data['data_role'] = RoleModel::whereNull('deleted_at')->where('status', 1)->get();
                $isUuid = Str::isUuid($id);
                if ($isUuid == true) {
                    $data['data'] = DB::table('t_master_problems')
                                ->where('id',$id)
                                ->whereNull('deleted_at')
                                ->get()
                                ->first();
                    return view('problem.edit', $data);
                } else {
                    $data['error_message'] = "Data dengan ID tersebut tidak ditemukan";
                    $data['link_back'] = "/master/problems";
                    return view('errors.empty_data',$data);
                }
            }
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'ProblemController@detail';
            $insert_error = parent::InsertErrorSystem($data);
            $error = parent::sidebar();
            $error['id'] = $insert_error;
            return view('errors.index',$error); // jika Metode Get
            //return response()->json($data); // jika metode Post
        }
    }


    public function update(Request $request)
    {
        try {
            $input = $request->validate([
            'problem' => 'required',
            ]);

            $update = DB::table('t_master_problems')->where('id',$request->id)->update($input);
            if ($update) {
                $data['code']    = 200;
                $data['message'] = 'Berhasil Mengupdate Data Problem';
                $insert_log      = parent::LogAdmin(\Request::ip(),Auth::guard('admin')->user()->id,'Mengubah Data Problem '.$request->id.'','Problem');
                return response()->json($data);
            } else {
                $data['code']    = 500;
                $data['message'] = 'Maaf Ada yang Error ';
                return response()->json($data);
            }
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'ProblemController@update';
            $insert_error = parent::InsertErrorSystem($data);
            return response()->json($data); // jika metode Post
        }
    }

    public function nonactive(Request $request)
    {
        try {
            $admin         = ProblemModel::find($request->id);
            $admin->status = 0;
            $admin->save();
            if ($admin) {
                $data['code']    = 200;
                $data['message'] = 'Berhasil Menonaktifkan Data Problem';
                $insert_log      = parent::LogAdmin(\Request::ip(),Auth::guard('admin')->user()->id,'Menon aktifkan Data Problem '.$request->id.'','Problem');
                return response()->json($data);
            } else {
                $data['code']    = 500;
                $data['message'] = 'Maaf Ada yang Error ';
                return response()->json($data);
            }
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'ProblemController@nonaktif';
            $insert_error = parent::InsertErrorSystem($data);
            return response()->json($data); // jika metode Post
        }
    }

    public function active(Request $request)
    {
        try {
            $admin         = ProblemModel::find($request->id);
            $admin->status = 1;
            $admin->save();
            if ($admin) {
                $data['code']    = 200;
                $data['message'] = 'Berhasil Mengaktifkan Data Problem';
                $insert_log      = parent::LogAdmin(\Request::ip(),Auth::guard('admin')->user()->id,'Mengaktifkan Data Problem '.$request->id.'','Problem');
                return response()->json($data);
            } else {
                $data['code']    = 500;
                $data['message'] = 'Maaf Ada yang Error ';
                return response()->json($data);
            }
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'ProblemController@active';
            $insert_error = parent::InsertErrorSystem($data);
            return response()->json($data); // jika metode Post
        }
    }

    public function delete(Request $request)
    {
        try {
            $admin             = ProblemModel::find($request->id);
            $admin->status     = 0;
            $admin->deleted_at = date('Y-m-d');
            $admin->save();

            if ($admin) {
                $data['code']    = 200;
                $data['message'] = 'Berhasil Menghapus Data Problem';
                $insert_log      = parent::LogAdmin(\Request::ip(),Auth::guard('admin')->user()->id,'Menghapus Data Problem '.$request->id.'','Problem');
                return response()->json($data);
            } else {
                $data['code']    = 500;
                $data['message'] = 'Maaf Ada yang Error ';
                return response()->json($data);
            }
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'ProblemController@delete';
            $insert_error = parent::InsertErrorSystem($data);
            return response()->json($data); // jika metode Post
        }
    }
}
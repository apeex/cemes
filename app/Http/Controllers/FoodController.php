<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\FoodModel;
use App\Classes\upload;
//use App\Traits\admin_logs;
use Auth;
use App\Exceptions\Handler;
use Illuminate\Support\Str;
use App\Models\RoleModel;

class FoodController extends Controller
{
    public function index()
    {
        // dd(FoodModel::get());
        try {
            $data = parent::sidebar();
            $data['header_title'] = "Kandang";
            if ($data['access'] == 0) {
                return redirect('/');
            } else {
                $role_id           = Auth::guard('admin')->user()->id_role;
                return view('food.index', $data);
            }
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'FoodController@index';
            $insert_error = parent::InsertErrorSystem($data);
            $error = parent::sidebar();
            $error['id'] = $insert_error;
            return view('errors.index',$error); // jika Metode Get
            //return response()->json($data); // jika metode Post
        }
    }

    public function history()
    {
        // dd(FoodModel::get());
        try {
            $data = parent::sidebar();
            $data['header_title'] = "Riwayat Pakan";
            $in_id_farmer = DB::table('t_eat_per_shed as e')->join('t_sheds as s','s.id','e.id_shed')->pluck('s.id_user');
            $data['farmer'] = DB::table('t_users')->where('status',1)->whereIn('id',$in_id_farmer)->select('id','user_name')->orderBy('user_name','asc')->get();
            if ($data['access'] == 0) {
                return redirect('/');
            } else {
                $role_id           = Auth::guard('admin')->user()->id_role;
                return view('food.history.index', $data);
            }
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'FoodController@history';
            $insert_error = parent::InsertErrorSystem($data);
            $error = parent::sidebar();
            $error['id'] = $insert_error;
            return view('errors.index',$error); // jika Metode Get
            //return response()->json($data); // jika metode Post
        }
    }

    public function list_data(Request $request)
    {

        $limit = $request->input('length');
        $start = $request->input('start');
        $dir   = $request->input('order.0.dir');

        $posts = DB::table('t_foods as s')->join('t_users as u','u.id','s.id_user')->whereNull('s.deleted_at')->orderBy('s.created_at','desc');

        $search = $request->input('search.value');

        if ($search) {
            $posts = $posts->where(function ($query) use ($search) {
                $query->where('user_name','ilike', "%{$search}%");
                $query->orWhere('user_phone','ilike', "%{$search}%");
                $query->orWhere('user_nik','ilike', "%{$search}%");
                $query->orWhere('food_name','ilike', "%{$search}%");
                $query->orWhere('food_id','ilike', "%{$search}%");
            });
        }
        $posts = $posts->select('s.*','user_name');

        $totalFiltered = $posts->count();
        $totalData = $totalFiltered;

        $posts = $posts->limit($limit)->offset($start)->get();

        $data = array();
        if (!empty($posts)) {
            $no = 0;
            foreach ($posts as $d) {
                $no = $no + 1;

                if ($d->status == 1) {
                    $status = '<div style="float: left; margin-left: 5px;"><a id="' . $d->id . '" aksi="nonaktif" tujuan="food" data="' . 'data_food' . '" class="btn btn-success btn-sm aksi">Aktif</a></div>';
                } else {
                    $status = '<div style="float: left; margin-left: 5px;"><a id="' . $d->id . '" aksi="aktif" tujuan="food" data="' . 'data_food' . '" class="btn btn-danger btn-sm aksi">Non Aktif</a></div>';
                }

                $action = '<a href="/food/list/'.base64_encode($d->id).'" class="btn btn-sm font-sm rounded btn-success me-2 d_detail"> <i class="material-icons md-edit"></i> Detail </a>';
                //delete
                // if ($d->slug != 'superadmin'){
                    $action .= '<a href="javascript:void(0)" class="btn btn-sm font-sm rounded  btn-danger aksi btn-aksi" id="' . $d->id . '" aksi="delete" tujuan="' . 'food' . '" data="' . 'data_food' . '"> <i class="material-icons md-delete"></i> Hapus </a>';
                //}
                $column['no']      = $no;
                $column['name']    = $d->food_name.' - '.$d->food_id;
                $column['farmer']  = $d->user_name;
                $column['stock']   = $d->stock.' Kg';
                $column['available'] = $d->available_stock.' Kg';
                $column['status']  = $status;
                $column['action']  = $action;
                $data[]            = $column;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data,
        );

        echo json_encode($json_data);

    }

    public function list_history(Request $request)
    {

        $limit = $request->input('length');
        $start = $request->input('start');
        $dir   = $request->input('order.0.dir');

        $posts = DB::table('t_eat_per_shed as e')->join('t_sheds as s','s.id','e.id_shed')->join('t_foods as f','f.id','e.id_food')->whereNull('e.deleted_at')->join('t_users as u','u.id','f.id_user')->orderBy('e.created_at','desc');

        $search = $request->input('search.value');

        if ($request->tanggal != null) {
            $posts = $posts->where('e.date',$request->tanggal);
        }

        if ($request->id_farmer != null) {
            $posts = $posts->where('f.id_user',$request->id_farmer);
        }

        if ($request->id_shed != null) {
            $posts = $posts->where('e.id_shed',$request->id_shed);
        }

        if ($request->id_food != null) {
            $posts = $posts->where('e.id_food',$request->id_food);
        }

        if ($search) {
            $posts = $posts->where(function ($query) use ($search) {
                $query->where('shed_name','ilike', "%{$search}%");
                $query->orWhere('shed_id','ilike', "%{$search}%");
                $query->orWhere('food_id','ilike', "%{$search}%");
                $query->orWhere('food_name','ilike', "%{$search}%");
            });
        }
        $posts = $posts->select('e.*','food_name','shed_name');

        $totalFiltered = $posts->count();
        $totalData = $totalFiltered;

        $posts = $posts->limit($limit)->offset($start)->get();

        $data = array();
        if (!empty($posts)) {
            $no = 0;
            foreach ($posts as $d) {
                $no = $no + 1;
                $id = "'".$d->id."'";

                $action = '<a href="javascript:void(0)" class="btn btn-sm font-sm rounded btn-success me-2" onclick="detail_pakan('.$id.')"> <i class="material-icons md-edit"></i> Lihat Detail </a>';
                
                $column['no']      = $no;
                $column['food']    = $d->food_name;
                $column['shed']    = $d->shed_name;
                $column['weigth']  = $d->weigth.' Kg';
                $column['date']    = date('d-m-Y',strtotime($d->date));
                $column['action']  = $action;
                $data[]            = $column;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data,
        );

        echo json_encode($json_data);

    }

    public function detail($id)
    {
        $id = base64_decode($id);
        $id = parent::cleanHazard($id);
        //dd($id);
        try {
            $data = parent::sidebar();
            $data['header_title'] = "Detail Data Pakan";
            //dd($data);
            if ($data['access'] == 0) {
                return redirect('/');
            } else {
                $role_id           = Auth::guard('admin')->user()->id_role;
                $data['data_role'] = RoleModel::whereNull('deleted_at')->where('status', 1)->get();
                $isUuid = Str::isUuid($id);
                if ($isUuid == true) {
                    $data['data'] = DB::table('t_foods as s')->join('t_users as u','u.id','s.id_user')
                                ->where('s.id',$id)
                                ->select('s.*','user_name','user_email','user_nik','user_phone','birthday','gender','profile')
                                ->first();
                    $in_shed = DB::table('t_eat_per_shed')->where('id_food',$id)->pluck('id_shed')->toArray();
                    $data['shed'] = DB::table('t_sheds')->whereIn('id',$in_shed)->select('id','shed_name')->orderBy('shed_name','asc')->get();
                    return view('food.detail', $data);
                } else {
                    $data['error_message'] = "Data dengan ID tersebut tidak ditemukan";
                    $data['link_back'] = "/food/list";
                    return view('errors.empty_data',$data);
                }
            }
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'FoodController@detail';
            $insert_error = parent::InsertErrorSystem($data);
            $error = parent::sidebar();
            $error['id'] = $insert_error;
            return view('errors.index',$error); // jika Metode Get
            //return response()->json($data); // jika metode Post
        }
    }

    public function nonactive(Request $request)
    {
        try {
            $admin         = FoodModel::find($request->id);
            $admin->status = 0;
            $admin->save();
            if ($admin) {
                $data['code']    = 200;
                $data['message'] = 'Berhasil Menonaktifkan Data Kandang';
                $insert_log      = parent::LogAdmin(\Request::ip(),Auth::guard('admin')->user()->id,'Menon aktifkan Data Kandang '.$request->id.'','Kandang');
                return response()->json($data);
            } else {
                $data['code']    = 500;
                $data['message'] = 'Maaf Ada yang Error ';
                return response()->json($data);
            }
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'FoodController@nonaktif';
            $insert_error = parent::InsertErrorSystem($data);
            return response()->json($data); // jika metode Post
        }
    }

    public function active(Request $request)
    {
        try {
            $admin         = FoodModel::find($request->id);
            $admin->status = 1;
            $admin->save();
            if ($admin) {
                $data['code']    = 200;
                $data['message'] = 'Berhasil Mengaktifkan Data Kandang';
                $insert_log      = parent::LogAdmin(\Request::ip(),Auth::guard('admin')->user()->id,'Mengaktifkan Data Kandang '.$request->id.'','Kandang');
                return response()->json($data);
            } else {
                $data['code']    = 500;
                $data['message'] = 'Maaf Ada yang Error ';
                return response()->json($data);
            }
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'FoodController@active';
            $insert_error = parent::InsertErrorSystem($data);
            return response()->json($data); // jika metode Post
        }
    }

    public function delete(Request $request)
    {
        try {
            $admin             = FoodModel::find($request->id);
            //dd($admin);
            $admin->status     = 0;
            $admin->deleted_at = date('Y-m-d');
            $admin->save();
            //dd("berhasil");

            if ($admin) {
                $data['code']    = 200;
                $data['message'] = 'Berhasil Menghapus Data Kandang';
                $insert_log      = parent::LogAdmin(\Request::ip(),Auth::guard('admin')->user()->id,'Menghapus Data Kandang '.$request->id.'','Kandang');
                return response()->json($data);
            } else {
                $data['code']    = 500;
                $data['message'] = 'Maaf Ada yang Error ';
                return response()->json($data);
            }
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'FoodController@delete';
            $insert_error = parent::InsertErrorSystem($data);
            return response()->json($data); // jika metode Post
        }
    }

    public function change_farmer(Request $request){
        $data['shed'] = DB::table('t_sheds')->where('id_user',$request->id)->where('status',1)->select('id','shed_name as name')->get();
        $data['food'] = DB::table('t_foods')->where('id_user',$request->id)->where('status',1)->select('id','food_name as name')->get();
        return json_encode($data);
    }

    public function detail_food($id){
        $data['data'] = DB::table('t_eat_histories as h')->join('t_cattles as c','c.id','h.id_cattle')->where('id_history',$id)->select('cattle_id','cattle_name','h.weigth')->get();
        return view('food.data_history',$data);
    }
}
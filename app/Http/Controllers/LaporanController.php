<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\CategoryFood;
use App\Classes\upload;
//use App\Traits\admin_logs;
use Auth;
use App\Exceptions\Handler;
use Illuminate\Support\Str;
use App\Models\RoleModel;

class LaporanController extends Controller
{
    public function laporan_ternak($id,$token)
    {
        // dd(CategoryFood::get());
        try {
            $id = base64_decode($id);
            $data['last_month'] = date('Y-m'); 
            $date = date('Y-m-d');
            $time = strtotime($date);
            $data['id'] = $id;
            $data['this_month'] = date("Y-m", strtotime("-1 month", $time));
            
            return view('laporan.cattle', $data);
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'LaporanController@laporan_ternak';
            $insert_error = parent::InsertErrorSystem($data);
            $error['id'] = $insert_error;
            return view('errors.index_mobile',$error); // jika Metode Get
            //return response()->json($data); // jika metode Post
        }
    }

    public function laporan_pakan($id,$token)
    {
        // dd(CategoryFood::get());
        try {
            $id = base64_decode($id);
            $data['last_month'] = date('Y-m'); 
            $date = date('Y-m-d');
            $time = strtotime($date);
            $data['id'] = $id;
            $data['this_month'] = date("Y-m", strtotime("-1 month", $time));
            
            return view('laporan.food', $data);
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'LaporanController@laporan_ternak';
            $insert_error = parent::InsertErrorSystem($data);
            $error['id'] = $insert_error;
            return view('errors.index_mobile',$error); // jika Metode Get
            //return response()->json($data); // jika metode Post
        }
    }

    public function get_graphic_ternak(Request $request) {
        try {
            //periode 1
            $now1 = date('Y-m-d',strtotime($request->periode1.'-'.'01'));
            $now1 = strtotime($now1);
            $last_year1 = date("Y-m", strtotime("-1 month", $now1));
            $catle_now1 = $this->CattleNumber($request->id,$request->periode1);
            $catle_last1 = $this->CattleNumber($request->id,$last_year1);
            $progress_now1 = $this->ProgressNumber($request->id,$request->periode1);
            $progress_last1 = $this->ProgressNumber($request->id,$last_year1);
            $weigth_now1 = $this->WeigthNumber($request->id,$request->periode1);
            $weigth_last1 = $this->WeigthNumber($request->id,$last_year1);

            $data['cattle_number1'] = $catle_now1;
            $data['grouwth1'] = $progress_now1;
            $data['weigth1']   = $weigth_now1;

            $data['cattle_number_progress1'] = 0;
            $data['grouwth_progress1'] =  0;
            $data['weigth_progress1'] = 0;

            if ($catle_now1 > 0) {
                $data['cattle_number_progress1'] = 100;
            }

            if ($progress_now1 > 0) {
                $data['grouwth_progress1'] =  100;
            }

            if ($weigth_now1 > 0) {
                $data['weigth_progress1'] = 100;
            }

            if($catle_last1 > 0){
                 $data['cattle_number_progress1'] = (($catle_now1 - $catle_last1) / $catle_last1) * 100;
            }
            if($progress_last1 > 0){
                $data['grouwth_progress1'] =  (($progress_now1 - $progress_last1) / $progress_last1) * 100;
            }
            if($weigth_last1 > 0){
                $data['weigth_progress1'] = (($weigth_now1 - $weigth_last1) / $weigth_last1) * 100;
            }

            //periode 2
            $now2 = date('Y-m-d',strtotime($request->periode2.'-'.'01'));
            $now2 = strtotime($now2);
            $last_year2 = date("Y-m", strtotime("-1 month", $now2));
            $catle_now2 = $this->CattleNumber($request->id,$request->periode2);
            $catle_last2 = $this->CattleNumber($request->id,$last_year2);
            $progress_now2 = $this->ProgressNumber($request->id,$request->periode2);
            $progress_last2 = $this->ProgressNumber($request->id,$last_year2);
            $weigth_now2 = $this->WeigthNumber($request->id,$request->periode2);
            $weigth_last2 = $this->WeigthNumber($request->id,$last_year2);

            $data['cattle_number2'] = $catle_now2;
            $data['grouwth2'] = $progress_now2;
            $data['weigth2']   = $weigth_now2;

            $data['cattle_number_progress2'] = 0;
            $data['grouwth_progress2'] =  0;
            $data['weigth_progress2'] = 0;

            if ($catle_now2 > 0) {
                $data['cattle_number_progress2'] = 100;
            }

            if ($progress_now2 > 0) {
                $data['grouwth_progress2'] =  100;
            }

            if ($weigth_now2 > 0) {
                $data['weigth_progress2'] = 100;
            }

            if($catle_last2 > 0){
                 $data['cattle_number_progress2'] = (($catle_now2 - $catle_last2) / $catle_last2) * 100;
            }
            if($progress_last2 > 0){
                $data['grouwth_progress2'] =  (($progress_now2 - $progress_last2) / $progress_last2) * 100;
            }
            if($weigth_last2 > 0){
                $data['weigth_progress2'] = (($weigth_now2 - $weigth_last2) / $weigth_last2) * 100;
            }

            $data['bulan1'] = date('F Y',strtotime($request->periode1.'-'.'01'));
            $data['bulan2'] = date('F Y',strtotime($request->periode2.'-'.'01'));

            //
            $month[] = $request->periode1;
            $month[] = $request->periode2;
            $res = [];
            $data['time'] = $month;
            $res[] = [
              'name' => 'Jantan',
              'data' => $this->DataGraphGender(1,$month,$request->id)
            ];
            $res[] = [
              'name' => 'Betina',
              'data' => $this->DataGraphGender(2,$month,$request->id)
            ];
            $data['data_graph'] = $res;

            $response = [
                'data' => $data,
                'messages' => "Success"
            ];
            return view('laporan.history_cattle', $data);

        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            //parent::LogErrorCreate($error,'LaporanController@get_graphic_ternak');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    public function get_graphic_pakan(Request $request) {
        try {
            //periode 1
            $now1 = date('Y-m-d',strtotime($request->periode1.'-'.'01'));
            $now1 = strtotime($now1);
            $last_year1 = date("Y-m", strtotime("-1 month", $now1));
            $price_now1 = $this->PriceFood($request->id,$request->periode1);
            $price_last1 = $this->PriceFood($request->id,$last_year1);
            $weigth_now1 = $this->WeigthFood($request->id,$request->periode1);
            $weigth_last1 = $this->WeigthFood($request->id,$last_year1);

            $data['price1'] = $price_now1;
            $data['weigth1']   = $weigth_now1;

            $data['price_progress1'] = 100;
            $data['weigth_progress1'] = 100;

            if($price_last1 > 0){
                 $data['price_progress1'] = (($price_now1 - $price_last1) / $price_last1) * 100;
            }
            
            if($weigth_last1 > 0){
                $data['weigth_progress1'] = (($weigth_now1 - $weigth_last1) / $weigth_last1) * 100;
            }

            //periode 2
            $now2 = date('Y-m-d',strtotime($request->periode2.'-'.'01'));
            $now2 = strtotime($now2);
            $last_year2 = date("Y-m", strtotime("-1 month", $now2));
            $price_now2 = $this->PriceFood($request->id,$request->periode2);
            $price_last2 = $this->PriceFood($request->id,$last_year2);
            $weigth_now2 = $this->WeigthFood($request->id,$request->periode2);
            $weigth_last2 = $this->WeigthFood($request->id,$last_year2);

            $data['price2'] = $price_now2;
            $data['weigth2']   = $weigth_now2;

            $data['price_progress2'] = 100;
            $data['weigth_progress2'] = 100;

            if($price_last2 > 0){
                 $data['price_progress2'] = (($price_now2 - $price_last2) / $price_last2) * 100;
            }
            
            if($weigth_last2 > 0){
                $data['weigth_progress2'] = (($weigth_now2 - $weigth_last2) / $weigth_last2) * 100;
            }


            $data['bulan1'] = date('F Y',strtotime($request->periode1.'-'.'01'));
            $data['bulan2'] = date('F Y',strtotime($request->periode2.'-'.'01'));

            //
            $month[] = $request->periode1;
            $month[] = $request->periode2;
            $res = [];
            $data['time'] = $month;
            $category = DB::table('t_food_categories')->where('status',1)->select('id','category_name')->get();
            foreach ($category as $key => $value) {
                $res[] = [
                  'name' => $value->category_name,
                  'data' => $this->DataGraphFood($value->id,$month,$request->id)
                ];
            }
            
            $data['data_graph'] = $res;

            $response = [
                'data' => $data,
                'messages' => "Success"
            ];
            return view('laporan.history_food', $data);

        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            //parent::LogErrorCreate($error,'LaporanController@get_graphic_ternak');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    public function DataGraphGender($gender,$date,$id)
    {
        $var = [];
        for ($i=0; $i < count($date); $i++) { 
            $year = date('Y', strtotime($date[$i]));
            $month = date('m', strtotime($date[$i]));
            $var[] = DB::table('t_cattles')->where('id_user',$id)->whereNull('deleted_at')->where('gender',$gender)->whereYear('created_at',$year)->whereMonth('created_at',$month)->count();
        }
        return $var;

    }

    public function DataGraphFood($id_category,$date,$id)
    {

        $var = [];
        for ($i=0; $i < count($date); $i++) { 
            $year = date('Y', strtotime($date[$i]));
            $month = date('m', strtotime($date[$i]));
            $stock = DB::table('t_foods')->where('id_user',$id)->whereNull('deleted_at')->where('id_category',$id_category)->whereYear('created_at',$year)->whereMonth('created_at',$month)->sum('stock');
            $var[] = (int)$stock;
        }
        return $var;

    }

    public function CattleNumber($id,$date){
        $year = date("Y", strtotime($date));
        $month = date("m", strtotime($date));
        $data = DB::table('t_cattles')->where('id_user',$id)->whereNull('deleted_at')->whereYear('created_at',$year)->whereMonth('created_at',$month)->count();
        return $data;
    }

    public function PriceFood($id,$date){
        $year = date("Y", strtotime($date));
        $month = date("m", strtotime($date));
        $data = DB::table('t_foods')->where('id_user',$id)->whereNull('deleted_at')->whereYear('created_at',$year)->whereMonth('created_at',$month)->sum('capital_price');
        return (int)$data;
    }

    public function WeigthFood($id,$date){
        $year = date("Y", strtotime($date));
        $month = date("m", strtotime($date));
        $data = DB::table('t_foods')->where('id_user',$id)->whereNull('deleted_at')->whereYear('created_at',$year)->whereMonth('created_at',$month)->sum('stock');
        return (int)$data;
    }

    public function WeigthNumber($id,$date){
        $year = date("Y", strtotime($date));
        $month = date("m", strtotime($date));
        $data = DB::table('t_cattles')->where('id_user',$id)->whereNull('deleted_at')->whereYear('created_at',$year)->whereMonth('created_at',$month)->sum('weigth');
        return (float)$data;
    }

    public function ShedNumber($id,$date){
        $year = date("Y", strtotime($date));
        $month = date("m", strtotime($date));
        $data = DB::table('t_sheds')->where('id_user',$id)->whereNull('deleted_at')->whereYear('created_at',$year)->whereMonth('created_at',$month)->count();
        return $data;
    }

    public function ProgressNumber($id,$date){
        $year = date("Y", strtotime($date));
        $month = date("m", strtotime($date));
        $data = DB::table('t_cattles')->where('id_user',$id)->where('type',1)->whereNull('deleted_at')->whereYear('created_at',$year)->whereMonth('created_at',$month)->count();
        return $data;
    }

    public function cronjobs_tanggal_input(){
        $data = DB::table('t_cattles')->get();
        foreach ($data as $k => $v) {
            $update = DB::table('t_cattles')->where('id',$v->id)->update(['input_date'=>$v->created_at]);
        }
        dd("success");
    }
}
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\Shed;
use App\Classes\upload;
//use App\Traits\admin_logs;
use Auth;
use App\Exceptions\Handler;
use Illuminate\Support\Str;
use App\Models\RoleModel;

class ShedController extends Controller
{
    public function index()
    {
        // dd(Shed::get());
        try {
            $data = parent::sidebar();
            $data['header_title'] = "Kandang";
            if ($data['access'] == 0) {
                return redirect('/');
            } else {
                $role_id           = Auth::guard('admin')->user()->id_role;
                return view('shed.index', $data);
            }
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'ShedController@index';
            $insert_error = parent::InsertErrorSystem($data);
            $error = parent::sidebar();
            $error['id'] = $insert_error;
            return view('errors.index',$error); // jika Metode Get
            //return response()->json($data); // jika metode Post
        }
    }

    public function history()
    {
        // dd(Shed::get());
        try {
            $data = parent::sidebar();
            $data['header_title'] = "Riwayat Kandang";
            $data['shed'] = DB::table('t_sheds as s')->join('t_users as u','u.id','s.id_user')->whereNull('s.deleted_at')->where('s.status',1)->select('s.id','shed_name','user_name')->orderBy('shed_name','desc')->get();
            if ($data['access'] == 0) {
                return redirect('/');
            } else {
                $role_id           = Auth::guard('admin')->user()->id_role;
                return view('shed.history.index', $data);
            }
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'ShedController@history';
            $insert_error = parent::InsertErrorSystem($data);
            $error = parent::sidebar();
            $error['id'] = $insert_error;
            return view('errors.index',$error); // jika Metode Get
            //return response()->json($data); // jika metode Post
        }
    }

    public function list_data(Request $request)
    {

        $limit = $request->input('length');
        $start = $request->input('start');
        $dir   = $request->input('order.0.dir');

        $posts = DB::table('t_sheds as s')->join('t_users as u','u.id','s.id_user')->whereNull('s.deleted_at')->orderBy('s.created_at','desc');

        $search = $request->input('search.value');

        if ($search) {
            $posts = $posts->where(function ($query) use ($search) {
                $query->where('user_name','ilike', "%{$search}%");
                $query->orWhere('user_phone','ilike', "%{$search}%");
                $query->orWhere('user_nik','ilike', "%{$search}%");
                $query->orWhere('shed_name','ilike', "%{$search}%");
                $query->orWhere('shed_id','ilike', "%{$search}%");
            });
        }
        $posts = $posts->select('s.*','user_name');

        $totalFiltered = $posts->count();
        $totalData = $totalFiltered;

        $posts = $posts->limit($limit)->offset($start)->get();

        $data = array();
        if (!empty($posts)) {
            $no = 0;
            foreach ($posts as $d) {
                $no = $no + 1;

                if ($d->status == 1) {
                    $status = '<div style="float: left; margin-left: 5px;"><a id="' . $d->id . '" aksi="nonaktif" tujuan="shed" data="' . 'data_shed' . '" class="btn btn-success btn-sm aksi">Aktif</a></div>';
                } else {
                    $status = '<div style="float: left; margin-left: 5px;"><a id="' . $d->id . '" aksi="aktif" tujuan="shed" data="' . 'data_shed' . '" class="btn btn-danger btn-sm aksi">Non Aktif</a></div>';
                }

                $action = '<a href="/shed/list/'.base64_encode($d->id).'" class="btn btn-sm font-sm rounded btn-success me-2 d_detail"> <i class="material-icons md-edit"></i> Detail </a>';
                //delete
                // if ($d->slug != 'superadmin'){
                    $action .= '<a href="javascript:void(0)" class="btn btn-sm font-sm rounded  btn-danger aksi btn-aksi" id="' . $d->id . '" aksi="delete" tujuan="' . 'shed' . '" data="' . 'data_shed' . '"> <i class="material-icons md-delete"></i> Hapus </a>';
                //}
                
                $column['no']      = $no;
                $column['name']    = $d->shed_name.' - '.$d->shed_id;
                $column['farmer']  = $d->user_name;
                $column['address'] = $d->address;
                $column['status']  = $status;
                $column['action']  = $action;
                $data[]            = $column;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data,
        );

        echo json_encode($json_data);

    }

    public function list_history(Request $request)
    {

        $limit = $request->input('length');
        $start = $request->input('start');
        $dir   = $request->input('order.0.dir');

        $posts = DB::table('t_shed_histories as s')->join('t_cattles as c','c.id','s.id_cattle')->whereNull('c.deleted_at')->orderBy('s.created_at','desc');

        $search = $request->input('search.value');

        if ($request->tanggal != null) {
            $posts = $posts->where('s.date',$request->tanggal);
        }

        if ($request->new_shed != null) {
            $posts = $posts->where('s.id_new_shed',$request->new_shed);
        }

        if ($request->old_shed != null) {
            $posts = $posts->where('s.id_old_shed',$request->old_shed);
        }

        if ($search) {
            $posts = $posts->where(function ($query) use ($search) {
                $query->where('cattle_name','ilike', "%{$search}%");
                $query->orWhere('cattle_id','ilike', "%{$search}%");
            });
        }
        $posts = $posts->select('s.*','cattle_name','cattle_id');

        $totalFiltered = $posts->count();
        $totalData = $totalFiltered;

        $posts = $posts->limit($limit)->offset($start)->get();

        $data = array();
        if (!empty($posts)) {
            $no = 0;
            foreach ($posts as $d) {
                $no = $no + 1;
                
                $column['no']      = $no;
                $column['name']    = $d->cattle_name.' - '.$d->cattle_id;
                $column['old_shed']  = parent::GetColoumnValue($d->id_old_shed,'t_sheds','shed_name');
                $column['new_shed'] = parent::GetColoumnValue($d->id_new_shed,'t_sheds','shed_name');
                $column['date']  = date('d-m-Y',strtotime($d->date));;
                $column['description']  = $d->description;
                $data[]            = $column;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data,
        );

        echo json_encode($json_data);

    }

    public function detail($id)
    {
        $id = base64_decode($id);
        $id = parent::cleanHazard($id);
        //dd($id);
        try {
            $data = parent::sidebar();
            $data['header_title'] = "Detail Data Kandang";
            //dd($data);
            if ($data['access'] == 0) {
                return redirect('/');
            } else {
                $role_id           = Auth::guard('admin')->user()->id_role;
                $data['data_role'] = RoleModel::whereNull('deleted_at')->where('status', 1)->get();
                $isUuid = Str::isUuid($id);
                if ($isUuid == true) {
                    $data['data'] = DB::table('t_sheds as s')->join('t_users as u','u.id','s.id_user')
                                ->where('s.id',$id)
                                ->select('s.*','user_name','user_email','user_nik','user_phone','birthday','gender','profile')
                                ->first();
                    return view('shed.detail', $data);
                } else {
                    $data['error_message'] = "Data dengan ID tersebut tidak ditemukan";
                    $data['link_back'] = "/shed/list";
                    return view('errors.empty_data',$data);
                }
            }
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'ShedController@detail';
            $insert_error = parent::InsertErrorSystem($data);
            $error = parent::sidebar();
            $error['id'] = $insert_error;
            return view('errors.index',$error); // jika Metode Get
            //return response()->json($data); // jika metode Post
        }
    }

    public function nonactive(Request $request)
    {
        try {
            $admin         = Shed::find($request->id);
            $admin->status = 0;
            $admin->save();
            if ($admin) {
                $data['code']    = 200;
                $data['message'] = 'Berhasil Menonaktifkan Data Kandang';
                $insert_log      = parent::LogAdmin(\Request::ip(),Auth::guard('admin')->user()->id,'Menon aktifkan Data Kandang '.$request->id.'','Kandang');
                return response()->json($data);
            } else {
                $data['code']    = 500;
                $data['message'] = 'Maaf Ada yang Error ';
                return response()->json($data);
            }
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'ShedController@nonaktif';
            $insert_error = parent::InsertErrorSystem($data);
            return response()->json($data); // jika metode Post
        }
    }

    public function active(Request $request)
    {
        try {
            $admin         = Shed::find($request->id);
            $admin->status = 1;
            $admin->save();
            if ($admin) {
                $data['code']    = 200;
                $data['message'] = 'Berhasil Mengaktifkan Data Kandang';
                $insert_log      = parent::LogAdmin(\Request::ip(),Auth::guard('admin')->user()->id,'Mengaktifkan Data Kandang '.$request->id.'','Kandang');
                return response()->json($data);
            } else {
                $data['code']    = 500;
                $data['message'] = 'Maaf Ada yang Error ';
                return response()->json($data);
            }
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'ShedController@active';
            $insert_error = parent::InsertErrorSystem($data);
            return response()->json($data); // jika metode Post
        }
    }

    public function delete(Request $request)
    {
        try {
            $admin             = Shed::find($request->id);
            //dd($admin);
            $admin->status     = 0;
            $admin->deleted_at = date('Y-m-d');
            $admin->save();
            //dd("berhasil");

            if ($admin) {
                $data['code']    = 200;
                $data['message'] = 'Berhasil Menghapus Data Kandang';
                $insert_log      = parent::LogAdmin(\Request::ip(),Auth::guard('admin')->user()->id,'Menghapus Data Kandang '.$request->id.'','Kandang');
                return response()->json($data);
            } else {
                $data['code']    = 500;
                $data['message'] = 'Maaf Ada yang Error ';
                return response()->json($data);
            }
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'ShedController@delete';
            $insert_error = parent::InsertErrorSystem($data);
            return response()->json($data); // jika metode Post
        }
    }
}
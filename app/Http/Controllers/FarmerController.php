<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\FarmerModel;
use App\Classes\upload;
//use App\Traits\admin_logs;
use Auth;
use App\Exceptions\Handler;
use Illuminate\Support\Str;
use App\Models\RoleModel;

class FarmerController extends Controller
{
    public function index()
    {
        // dd(FarmerModel::get());
        try {
            $data = parent::sidebar();
            $data['header_title'] = "Peternak";
            if ($data['access'] == 0) {
                return redirect('/');
            } else {
                $role_id           = Auth::guard('admin')->user()->id_role;
                return view('farmer.index', $data);
            }
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'FarmerController@index';
            $insert_error = parent::InsertErrorSystem($data);
            $error = parent::sidebar();
            $error['id'] = $insert_error;
            return view('errors.index',$error); // jika Metode Get
            //return response()->json($data); // jika metode Post
        }
    }

    public function post(Request $request)
    {
        try {
            $data = $request->validate([
                'nama_category' => 'required',
            ]);
            $slug = parent::MakeSlug($request->nama_category);

            if ($request->file('profile')) {
                
                $icon = parent::uploadFileS3($request->file('profile'));
            } else {
                $icon = null;
            }

            $datas = [
                "category_name" => $request->nama_category,
                "icon" => $icon,
                "slug"  => $slug,
                "status" => 1,
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s'),
            ];

            $id_cat = DB::table('t_users')->insertGetId($datas);
            $insert_log      = parent::LogAdmin(\Request::ip(),Auth::guard('admin')->user()->id,'Menambah Data Kategori Ternak '.$request->nama_category.'','Kategori Ternak');
            $data['code']    = 200;
            $data['message'] = 'Berhasil Menambah Data Kategori Ternak';

            return response()->json($data);
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'FarmerController@post';
            $insert_error = parent::InsertErrorSystem($data);
            return response()->json($data); // jika metode Post
        }
    }

    public function list_data(Request $request)
    {
        
        $totalData = FarmerModel::whereNull('deleted_at')->count();

        $limit = $request->input('length');
        $start = $request->input('start');
        $dir   = $request->input('order.0.dir');

        $posts = DB::table('t_users')->whereNull('deleted_at')->orderBy('created_at','desc');

        $search = $request->input('search.value');

        if ($search) {

            $posts = $posts->where(function ($query) use ($search) {
                $query->where('user_name','ilike', "%{$search}%");
                $query->orWhere('user_phone','ilike', "%{$search}%");
                $query->orWhere('user_nik','ilike', "%{$search}%");
            });
        }

        $totalFiltered = $posts->count();

        $posts = $posts->limit($limit)->offset($start)->get();

        $data = array();
        if (!empty($posts)) {
            $no = 0;
            foreach ($posts as $d) {
                $no = $no + 1;

                if ($d->status == 1) {
                    $status = '<div style="float: left; margin-left: 5px;"><a id="' . $d->id . '" aksi="nonaktif" tujuan="farmer" data="' . 'data_category_article' . '" class="btn btn-success btn-sm aksi">Aktif</a></div>';
                } else {
                    $status = '<div style="float: left; margin-left: 5px;"><a id="' . $d->id . '" aksi="aktif" tujuan="farmer" data="' . 'data_category_article' . '" class="btn btn-danger btn-sm aksi">Non Aktif</a></div>';
                }

                $action = '<a href="/farmer/list-farmer/'.base64_encode($d->id).'" class="btn btn-sm font-sm rounded btn-success me-2 d_detail"> <i class="material-icons md-edit"></i> Edit </a>';
                //delete
                //if ($d->slug != 'superadmin'){
                    $action .= '<a href="javascript:void(0)" class="btn btn-sm font-sm rounded  btn-danger aksi btn-aksi" id="' . $d->id . '" aksi="delete" tujuan="' . 'farmer' . '" data="' . 'data_category_article' . '"> <i class="material-icons md-delete"></i> Hapus </a>';
                //}
                if ($d->profile != null) {
                    $image = '<img src="'.env('BASE_IMG').$d->profile.'" class="img-sm img-avatar" alt="Userpic">';
                } else {
                    $image = '<img src="'.\URL::asset('assets').'/imgs/people/avatar-1.png" class="img-sm img-avatar" alt="Userpic">';
                }
                if ($d->user_name != null) {
                    $user_name = $d->user_name;
                } else {
                    $user_name = "Pelanggan Baru";
                }
                $user = '<a href="javascript:void(0)" class="itemside"><div class="left">'.$image.'</div>
                            <div class="info pl-3"><h6 class="mb-0 title">'.$user_name.'</h6><small class="text-muted">Phone: '.$d->user_phone.'</small></div>
                        </a>';
                if ($d->gender == 1) {
                    $gender = 'Laki - Laki';
                } else if($d->gender == 2) {
                    $gender = 'Perempuan';
                } else {
                    $gender = 'Belum ditentukan';
                }
                
                $column['no']      = $no;
                $column['name']      = $user;
                $column['gender'] = $gender;
                $column['peternakan'] = $d->farmer_name;
                $column['date']    = date('d-m-Y H:i',strtotime($d->created_at));
                $column['status']    = $status;
                $column['action']   = $action;
                $data[]              = $column;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data,
        );

        echo json_encode($json_data);

    }

    public function detail($id)
    {
        $id = base64_decode($id);
        $id = parent::cleanHazard($id);
        //dd($id);
        try {
            $data = parent::sidebar();
            $data['header_title'] = "Edit Data Kategori Ternak";
            //dd($data);
            if ($data['access'] == 0) {
                return redirect('/');
            } else {
                $role_id           = Auth::guard('admin')->user()->id_role;
                $data['data_role'] = RoleModel::whereNull('deleted_at')->where('status', 1)->get();
                $isUuid = Str::isUuid($id);
                if ($isUuid == true) {
                    $data['data'] = DB::table('t_users')
                                ->where('id',$id)
                                ->whereNull('deleted_at')
                                ->get()
                                ->first();
                    return view('farmer.edit', $data);
                } else {
                    $data['error_message'] = "Data dengan ID tersebut tidak ditemukan";
                    $data['link_back'] = "/farmer/list-farmer";
                    return view('errors.empty_data',$data);
                }
            }
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'FarmerController@detail';
            $insert_error = parent::InsertErrorSystem($data);
            $error = parent::sidebar();
            $error['id'] = $insert_error;
            return view('errors.index',$error); // jika Metode Get
            //return response()->json($data); // jika metode Post
        }
    }


    public function update(Request $request)
    {
        try {
            $input = $request->validate([
            'category_name' => 'required',
            ]);

            if ($request->file('profile')) {
                
                $input['icon'] = parent::uploadFileS3($request->file('profile'));
            }
            $input['slug'] = parent::MakeSlug($request->name);

            $update = DB::table('t_users')->where('id',$request->id)->update($input);
            if ($update) {
                $data['code']    = 200;
                $data['message'] = 'Berhasil Mengupdate Data Kategori Ternak';
                $insert_log      = parent::LogAdmin(\Request::ip(),Auth::guard('admin')->user()->id,'Mengubah Data Kategori Ternak '.$request->id.'','Kategori Ternak');
                return response()->json($data);
            } else {
                $data['code']    = 500;
                $data['message'] = 'Maaf Ada yang Error ';
                return response()->json($data);
            }
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'FarmerController@update';
            $insert_error = parent::InsertErrorSystem($data);
            return response()->json($data); // jika metode Post
        }
    }

    public function nonactive(Request $request)
    {
        try {
            $admin         = FarmerModel::find($request->id);
            $admin->status = 0;
            $admin->save();
            if ($admin) {
                $data['code']    = 200;
                $data['message'] = 'Berhasil Menonaktifkan Data Kategori Ternak';
                $insert_log      = parent::LogAdmin(\Request::ip(),Auth::guard('admin')->user()->id,'Menon aktifkan Data Kategori Ternak '.$request->id.'','Kategori Ternak');
                return response()->json($data);
            } else {
                $data['code']    = 500;
                $data['message'] = 'Maaf Ada yang Error ';
                return response()->json($data);
            }
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'FarmerController@nonaktif';
            $insert_error = parent::InsertErrorSystem($data);
            return response()->json($data); // jika metode Post
        }
    }

    public function active(Request $request)
    {
        try {
            $admin         = FarmerModel::find($request->id);
            $admin->status = 1;
            $admin->save();
            if ($admin) {
                $data['code']    = 200;
                $data['message'] = 'Berhasil Mengaktifkan Data Kategori Ternak';
                $insert_log      = parent::LogAdmin(\Request::ip(),Auth::guard('admin')->user()->id,'Mengaktifkan Data Kategori Ternak '.$request->id.'','Kategori Ternak');
                return response()->json($data);
            } else {
                $data['code']    = 500;
                $data['message'] = 'Maaf Ada yang Error ';
                return response()->json($data);
            }
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'FarmerController@active';
            $insert_error = parent::InsertErrorSystem($data);
            return response()->json($data); // jika metode Post
        }
    }

    public function delete(Request $request)
    {
        try {
            $admin             = FarmerModel::find($request->id);
            $admin->status     = 0;
            $admin->deleted_at = date('Y-m-d');
            $admin->save();

            if ($admin) {
                $data['code']    = 200;
                $data['message'] = 'Berhasil Menghapus Data Kategori Ternak';
                $insert_log      = parent::LogAdmin(\Request::ip(),Auth::guard('admin')->user()->id,'Menghapus Data Kategori Ternak '.$request->id.'','Kategori Ternak');
                return response()->json($data);
            } else {
                $data['code']    = 500;
                $data['message'] = 'Maaf Ada yang Error ';
                return response()->json($data);
            }
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'FarmerController@delete';
            $insert_error = parent::InsertErrorSystem($data);
            return response()->json($data); // jika metode Post
        }
    }
}
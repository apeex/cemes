<?php
namespace App\Http\Controllers;

use App\Model\AmalanModel;
use App\Model\RoleModel;
use App\Traits\Fungsi;
use Auth;
use Illuminate\Http\Request;
use Redirect;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Exceptions\Handler;
use Illuminate\Support\Str;


class DashboardController extends Controller
{  

    public function alawiyah()
    {
        // dd(AmalanModel::get());
        try {
            $data = parent::sidebar();
            $data['header_title'] = "List Data Amalan";
            if ($data['access'] == 0) {
                return redirect('/');
            } else {
                $role_id           = Auth::guard('admin')->user()->id_role;
                $data['data_role'] = RoleModel::whereNull('deleted_at')->where('status', 1)->get();
                $data['data'] = DB::table('rabithah_alawiyah')->where('status',1)->first();
                $data['data_foto']    = DB::table('relation_ra_image')->where('type',1)->where('group',1)->whereNull('deleted_at')->get();
                $data['data_video']    = DB::table('relation_ra_image')->where('type',2)->where('group',1)->whereNull('deleted_at')->get();
                return view('alawiyah.index', $data);
            }

        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'DashboardController@alawiyah';
            $insert_error = parent::InsertErrorSystem($data);
            $error = parent::sidebar();
            $error['id'] = $insert_error;
            return view('errors.index',$error); // jika Metode Get
            //return response()->json($data); // jika metode Post
        }
    } 

    public function data_rabitah_alawiyah(){

        $data['data'] = DB::table('rabithah_alawiyah')->where('status',1)->first();
        $data['data_file']    = DB::table('relation_ra_image')->where('group',1)->whereNull('deleted_at')->get();
        return view('alawiyah.view', $data);

    }

    public function data_rabitah_alawiyah_page($coloumn){

        $data['data'] = DB::table('rabithah_alawiyah')->where('status',1)->pluck($coloumn);
        return view('alawiyah.page', $data);

    }

    public function header_notification(){
        //$return = 0;
        $count = DB::table('t_questions')->where('is_answered',0)->count();
        return $count;
    }

    public function madros()
    {
        // dd(AmalanModel::get());
        try {
            $data = parent::sidebar();
            $data['header_title'] = "Data Madros";
            if ($data['access'] == 0) {
                return redirect('/');
            } else {
                $role_id           = Auth::guard('admin')->user()->id_role;
                $data['data_role'] = RoleModel::whereNull('deleted_at')->where('status', 1)->get();
                $data['data'] = DB::table('t_madros')->where('status',1)->first();
                return view('madros.index', $data);
            }

        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'DashboardController@madros';
            $insert_error = parent::InsertErrorSystem($data);
            $error = parent::sidebar();
            $error['id'] = $insert_error;
            return view('errors.index',$error); // jika Metode Get
            //return response()->json($data); // jika metode Post
        }
    } 

    public function data_madros(){

        $data['data'] = DB::table('t_madros')->where('status',1)->first();
        return view('madros.view', $data);

    }

    public function add_file_alawiyah($type,$no){
        $data['no'] = $no;
        return view('alawiyah.component-'.$type.'', $data);
    }  

    public function updatealawiyah(Request $request){
        try {

            $file_old = $request->file_old;
            $id_old   = $request->id_old;
            $desc_old = $request->desc_old;
            $file     = $request->file;
            $type     = $request->type;
            $desc     = $request->desc;
            
            if (isset($request->id_old)) {
                foreach ($id_old as $k => $v) {
                    $existing = DB::table('relation_ra_image')->where('id',$id_old[$k])->first();
                    $desc_o =isset($desc_old[$k]) ? $desc_old[$k] : null ;
                    $file = $existing->file;
                    if ($request->hasFile('file_old') != false){
                        if($request->hasFile('file_old')[$k] != false){
                            $file = parent::uploadFileS3($request->file('file_old')[$k]);
                        }      
                    }
                    //dd("success");
                    
                    $up = DB::table('relation_ra_image')->where('id',$id_old[$k])->update(['file'=>$file,'description'=>$desc_o]);
                }
            }

            if (isset($request->type)) {
                foreach ($type as $a => $b) {
                    //dd($request->file('file')[$a]);
                    if ($request->hasFile('file') != false){
                        //if($request->hasFile('file')[$a] != false){
                            if ($request->file('file')[$a]->isValid())
                            {
                                //dd("masuk sini");
                                $file = parent::uploadFileS3($request->file('file')[$a]);
                                $insertqua = [
                                   'file' => $file,
                                    'type' => $type[$a],
                                    'group' => 1,
                                    'group_name' => 'rabithoh_alawiyah',
                                    'description' => $desc[$a],
                                    'created_at'=>date('Y-m-d H:i:s'),
                                    'updated_at'=>date('Y-m-d H:i:s'),
                                ];
                                $insert2 = DB::table('relation_ra_image')->insert($insertqua);
                            }
                        //}
                    }
                    //dd("ora masuk");
                }
            }

            $update = DB::table('rabithah_alawiyah')->where('id',$request->id)->update(['description'=>$request->description,'social'=>$request->social,'dakwah'=>$request->dakwah,'education'=>$request->education]);
            if ($update) {
                $data['code']    = 200;
                $data['message'] = "Data Rabithah Alawiyah berhasil diperbarui";
            } else {
                $data['code']    = 500;
                $data['message'] = "Terdapat kesalahan system";
            }
            return response()->json($data);

        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'DashboardController@updatealawiyah';
            $insert_error = parent::InsertErrorSystem($data);
            return response()->json($data); // jika metode Post
        }
    }

    //update madros
    public function updatemadros(Request $request){
        try {
            $update = DB::table('t_madros')->where('id',$request->id)->update(['description'=>$request->description]);
            if ($update) {
                $data['code']    = 200;
                $data['message'] = "Data Madros berhasil diperbarui";
            } else {
                $data['code']    = 500;
                $data['message'] = "Terdapat kesalahan system";
            }
            return response()->json($data);

        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'DashboardController@updatemadros';
            $insert_error = parent::InsertErrorSystem($data);
            return response()->json($data); // jika metode Post
        }
    }

    public function hapus_relasi_image(Request $request){
        $update = DB::table('relation_ra_image')->where('id',$request->id)->update(['deleted_at'=>date('Y-m-d')]);
        if ($update) {
            $data['code']    = 200;
            $data['message'] = "berhasil menghapus data";
        } else {
            $data['code']    = 500;
            $data['message'] = "Terdapat kesalahan system";
        }
        return response()->json($data);
    }

    public function forgot_password($id,$token){
        $id = base64_decode($id);
        $id = parent::cleanHazard($id);
        try {
            $isUuid = Str::isUuid($id);
            if ($isUuid == true) {
                $user = DB::table('t_users')->where('id',$id)->where('forgot_password_token',$token)->first();
                if ($user != null) {
                    date_default_timezone_set("Asia/Jakarta");
                    $time = new \DateTime($user->forgot_password_time);
                    $diff = $time->diff(new \DateTime());
                    $minutes = ($diff->days * 24 * 60) +
                               ($diff->h * 60) + $diff->i;
                    if ($minutes > 120){
                        $data['error_message'] = "Mohon maaf link sudah Expired karena sudah melebihi 2 jam";
                        return view('errors.expired',$data);
                    } else {
                        $data['id'] = $user->id;
                        return view('forgot_password', $data);
                    }
                } else {
                    $data['error_message'] = "Data dengan ID tersebut tidak ditemukan";
                    return view('errors.expired',$data);
                }
            } else {
                $data['error_message'] = "Data dengan ID tersebut tidak ditemukan";
                return view('errors.expired',$data);
            }

        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'DashboardController@forgot_password';
            $insert_error = parent::InsertErrorSystem($data);
            $error = parent::sidebar();
            $error['id'] = $insert_error;
            return view('errors.index',$error); // jika Metode Get
            //return response()->json($data); // jika metode Post
        }
    }

    public function post_fargot_password(Request $request){
        try {
            if ($request->password != $request->password_confirmation) {
                $data['code']    = 500;
                $data['message'] = "Konfirmasi Password Salah";
                return response()->json($data);
            }
            if ($request->FORGOT_PASSWORD_KEY != 'mt1vStlYv8jy2StuqFqDocWJ+ypXNivdH60xdrteqdH') {
                $data['code']    = 500;
                $data['message'] = "Akses Ilegal";
                return response()->json($data);
            }

            $update = DB::table('t_users')->where('id',$request->id)->update(['password'=> Hash::make($request->password)]);
            if ($update) {
                $data['code']    = 200;
                $data['message'] = "Selamat Passwor Berhasil Diubah";
                return response()->json($data);
            } else {
                $data['code']    = 500;
                $data['message'] = "Terdapat error system";
                return response()->json($data);
            }

        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'DashboardController@post_fargot_password';
            $insert_error = parent::InsertErrorSystem($data);
            return response()->json($data); // jika metode Post
        }
    }

    public function index()
    {
        //dd("end data");
        $role_id = Auth::guard('admin')->user()->id_role;
        $data = parent::sidebar();
        $data['position_menu'] = "Dashboard";
        $data['header_title'] = "Dashboard";
        $bulan_ini = date('m');
        if ($data['access'] == 0) {
            return redirect('/');
        } else {  
            //dd("end data");
            $admin_s = parent::admin_data();
            $rules = $admin_s['role'];
            $end = date('Y-m-d');
            $data['start'] = $end;
            // $data['start'] = date('Y-m-d',strtotime('-1 day',strtotime($end)));
            $data['end'] = $end;
            // bisnis
              
        return view('dashboard',$data);
      }
    }

    public function get_dashboard(Request $request){

        $start = $request->start;
        $end = $request->end;
        if ($start != '0') {
            $data['start'] = $start;
        }
        if ($end != '0') {
            $data['end'] = $end;
        }

        $jumlah_transaksi = DB::table('transactions')->where('status',2);  
        // if ($start != '0') {
        //     $jumlah_transaksi = $jumlah_transaksi->where('created_at','>=',$start);
        // }
        // if ($end != '0') {
        //     $jumlah_transaksi = $jumlah_transaksi->where('created_at','<=',date('Y-m-d',strtotime($end . "+1 days")));
        // }      
        $jumlah_transaksi = $jumlah_transaksi->count();

        $terbayar = DB::table('transactions')->where('status',2);
        // if ($start != '0') {
        //     $terbayar = $terbayar->where('created_at','>=',$start);
        // }
        // if ($end != '0') {
        //     $terbayar = $terbayar->where('created_at','<=',date('Y-m-d',strtotime($end . "+1 days")));
        // }   
        $terbayar =  $terbayar->sum('total_off_pay');

        $user = DB::table('users')->whereNull('deleted_at')->count();
        //$user =  $user->count();

        $pasien = DB::table('patients')->whereNull('deleted_at')->count();
        // if ($start != '0') {
        //     $pasien = $pasien->where('created_at','>=',$start);
        // }
        // if ($end != '0') {
        //     $pasien = $pasien->where('created_at','<=',date('Y-m-d',strtotime($end . "+1 days")));
        // }   
        //$pasien =  $pasien->count();

        // jenis kelamin
        $data['jk_l'] = DB::table('patients')->where('gender',1)->whereNull('deleted_at')->count();
        $data['jk_p'] = DB::table('patients')->where('gender',2)->whereNull('deleted_at')->count();

        // transaksi
        $init = date('Y-m-d');
        $periode = 13;
        $penjualan = array();
        $nominal = array();
        $user_all = array();
        $pasien_all = array();
        $this_month = date('Y-m-d', strtotime($init.'+1 month'));
        for ($i=1; $i < $periode; $i++) { 
            $month[] =  date('F Y', strtotime($this_month.'-'.$i.' month'));
            $x_month[] =  date('Y-m-d', strtotime($this_month.'-'.$i.' month'));
            $x_month_num[] =  date('m', strtotime($this_month.'-'.$i.' month'));
            //$this_month = $month;
        }
        // dd($x_month_num);
        foreach ($x_month_num as $key) {
            $penjualan[] = $this->getJumlah($key);
        }

        foreach ($x_month_num as $key) {
            $nominal[] = $this->getNominal($key);
        }

        foreach ($x_month_num as $key) {
            $user_all[] = $this->getUser($key);
        }

        foreach ($x_month_num as $key) {
            $pasien_all[] = $this->getPasien($key);
        }
        // $data['jumlah_transaksi'] = DB::table('transactions')->where('status',1)->count();
        // $data['terbayar'] = DB::table('transactions')->where('status',1)->sum('total_off_pay');
        // $data['user'] = DB::table('users')->whereNull('deleted_at')->count();
        // $data['pasien'] = DB::table('patients')->whereNull('deleted_at')->count();
        $array_pasien = DB::table('schedule_patients')->pluck('id_patients')->toArray();
        $data['pasien_tabel'] = DB::table('patients')->whereDate('created_at',$init)->whereIn('id',$array_pasien)->get();

        $data['month'] = $month;
        $data['penjualan'] = $penjualan;
        $data['user_all'] = $user_all;
        $data['pasien_all'] = $pasien_all;
       
        $data['jumlah_transaksi'] =$jumlah_transaksi;
        $data['nominal'] = $nominal;
        $data['terbayar'] = $terbayar;
        $data['user'] = $user;
        $data['pasien'] = $pasien;
        // dd($data);
        return view('data_dashboard',$data);

    }


    public function getJumlah($date)
    {
        $jumlah = DB::table('transactions')
                ->where('status',1)
                ->whereMonth('created_at',$date)
                ->count();
        return $jumlah;
    }

    public function getNominal($date)
    {
        $penjualan = DB::table('transactions')
                ->where('status',1)
                ->whereMonth('created_at',$date)
                ->sum('total_off_pay');
        return $penjualan;
    }

    public function getUser($date)
    {
        $user = DB::table('users')
                ->where('status',1)
                ->whereMonth('created_at',$date)
                ->count();
        return $user;
    }


    public function getPasien($date)
    {
        $user = DB::table('patients')
                ->where('status',1)
                ->whereMonth('created_at',$date)
                ->count();
        return $user;
    }


    public function dasboard_schedule(Request $request)
    {
        $product = $this->DataSchedule($request);

        $product = $product->select('s.*','patient_name','branch_name','master_schedules.time as time');
        $limit = $request->input('length');
        $start = $request->input('start');
        $dir   = $request->input('order.0.dir');

        $totalFiltered = $product->count();
        $totalData = $product->count();

        $product = $product->limit($limit)->offset($start)->get();

        // dd($product);

        $data = array();
        $no   = 0;
        foreach ($product as $d) {
            $ids = "'select_txt_".$d->id."'";
            if ($d->status == 1) {
                $status = '<span class="text-success">Selesai</span>';
            } else {
                $status = '<span class="text-primary">Waiting</span>';
            }

            $action = '<a href="javascript:void(0)" id="'.$d->id.'" class="btn btn-success btn-sm d_detail"><span class="navi-icon"><i class="fas fa-eye"></i></span><span class="navi-text">Detail</span></a>';
            $action .= '<a href="javascript:void(0)" class="btn btn-danger ml-2 btn-sm aksi btn-aksi" id="' . $d->id.'" aksi="delete" tujuan="' . 'promosi' . '" data="' . 'data_promosi' . '"><span class="navi-icon"><i class="fa fa-trash"></i></span><span class="navi-text">Hapus</span></a>';
            $actions = " - ";

            $no     = $no + 1;
            $column['pasien']    = $d->patient_name;
            $column['cabang']    = $d->branch_name;
            $column['date'] = $d->day.' '.date('d F Y',strtotime($d->schedule_date));
            $column['time'] = date('H:i',strtotime($d->time));
            $column['status'] = $status;
            $column['no_urut']    = $d->no_urut;
            $data[]              = $column;
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data,
        );

        echo json_encode($json_data);

    }


      public function DataSchedule($request){
        $init = date('Y-m-d');
        // dd($request->all());
        $posts = ScheduleModel::join('schedule_patients as s','s.id_schedules','master_schedules.id')->join('patients as pt','s.id_patients','pt.id')->leftJoin('table_branchs as t','t.id','master_schedules.id_branch')->whereNull('master_schedules.deleted_at')->where(static function ($query) use ($request) {

            // if ($request->tgl_start != null) {
            //     $query->whereDate('s.schedule_date',$request->tgl_start);   
            // }
            
            if ($request->tgl_start != null) {
                $query->where('s.schedule_date','>=',$request->tgl_start);
            } 

            if ($request->akhir != null) {
                $query->where('s.schedule_date','<=',date('Y-m-d',strtotime($request->akhir . "+1 days")));
            } 
            
        });
        return $posts;             
    }
   

    public function merchant_category()
    {
        // $merchant_category = DB::table('table_category_merchant');
        $data = DB::table('table_merchant as m')
        ->leftJoin('table_category_merchant as cm', 'cm.id', '=', 'm.merchant_category')
        ->select( DB::raw('count(*) as total'), 'cm.category_merchant_name')
        ->groupBy('m.merchant_category', 'cm.category_merchant_name')
        ->whereNull('m.deleted_at')
        ->whereNull('cm.deleted_at')
        ->get();
        $datas = array();
        $labels = array();
        $fills = array();

        foreach ($data as $key => $value) {
            # code...
            $labels[] = $value->category_merchant_name;
            $fills[] =  $value->total;
        }
        $datas = [ $labels, $fills];
        
        return response()
            ->json($datas);
    }

    

    public function menu_category()
    {
        // $menu_category = DB::table('table_category_menu');
        $data = DB::table('table_product_menus as pm')
        ->leftJoin('table_category_product as cm', 'cm.id', '=', 'pm.id_category')
        ->select( DB::raw('count(*) as total'), 'cm.category_product_name')
        ->groupBy('pm.id_category', 'cm.category_product_name')
        ->whereNull('pm.deleted_at')
        ->whereNull('cm.deleted_at')
        ->get();
        $datas = array();
        $labels = array();
        $fills = array();

        foreach ($data as $key => $value) {
            # code...
            $labels[] = $value->category_product_name;
            $fills[] =  $value->total;
        }
        $datas = [ $labels, $fills];
        
        return response()
            ->json($datas);
    }


    public function type_product()
    {
        // $menu_category = DB::table('table_category_menu');
        $data = DB::table('table_product_menus as pm')
        ->leftJoin('table_type_product as tp', 'tp.id', '=', 'pm.id_type_product')
        ->select( DB::raw('count(*) as total'), 'tp.type_product_name')
        ->groupBy('pm.id_type_product', 'tp.type_product_name')
        ->whereNull('pm.deleted_at')
        ->whereNull('tp.deleted_at')
        ->get();
        $datas = array();
        $labels = array();
        $fills = array();

        foreach ($data as $key => $value) {
            # code...
            $labels[] = $value->type_product_name;
            $fills[] =  $value->total;
        }
        $datas = [ $labels, $fills];
        
        return response()
            ->json($datas);
    }

    public function get_print_barcode($id){

        $data['data'] = DB::table('relation_transaction_machine as r')->join('patients as p','r.code_patient','p.patient_code')->where('id_transaction',$id)->distinct('code_patient','nama_tes')->get();
        $data['code'] = '536474747474';
        $customPaper = array(0,0,141.7322834645669, 85.03937007874016);
        $pdf = PDF::loadView('get_barcode',$data)->setPaper($customPaper, 'potrait');
        return $pdf->stream();

        //return view('get_barcode',$data);
    }  

    public function get_address($table,$id_search,$id_object){
        $data = DB::table($table)->whereNull('deleted_at')->orderBy('name','asc')->where($id_search,$id_object)->get();
        return json_encode($data);
    }

    public function get_address_user($id,$id_user){
        $data['country'] = DB::table('countries')->whereNull('deleted_at')->get();
        $data['id_user'] = $id_user;
        if ($id == 'new') {
            $data['data'] = [];
            return view('customer.new_address', $data);
        } else {
            $data['data'] = DB::table('t_address')->where('id',$id)->first();
            return view('customer.address', $data);
        }
    }
}
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use App\Model\RoleModel;
use App\Model\LogUser;
use App\Traits\Fungsi;
use Redirect;
use App\Exceptions\Handler;

class LogUserController extends Controller
{
    public function customer()
    {
        try {
        	$role_id = Auth::guard('admin')->user()->id_role;
            $data = parent::sidebar();
            if ($data['access'] == 0) {
                return redirect('/');
            } else {
                $data['name_adm'] = Auth::guard('admin')->user()->name;
                
                return view('logs.user.customer',$data);
            }
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'LogUserController@customer';
            $insert_error = parent::InsertErrorSystem($data);
            $error = parent::sidebar();
            $error['id'] = $insert_error;
            return view('errors.index',$error); // jika Metode Get
            //return response()->json($data); // jika metode Post
        }
    }

    public function seller()
    {
        try {
            $role_id = Auth::guard('admin')->user()->id_role;
            $data = parent::sidebar();
            if ($data['access'] == 0) {
                return redirect('/');
            } else {
                $data['name_adm'] = Auth::guard('admin')->user()->name;
                
                return view('logs.user.seller',$data);
            }
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'LogUserController@seller';
            $insert_error = parent::InsertErrorSystem($data);
            $error = parent::sidebar();
            $error['id'] = $insert_error;
            return view('errors.index',$error); // jika Metode Get
            //return response()->json($data); // jika metode Post
        }
    }

    public function list_data_customer(Request $request)
    {

        $limit = $request->input('length');
        $start = $request->input('start');
        $dir   = $request->input('order.0.dir');
        $search = $request->input('search.value');

        $posts = LogUser::join('t_users as p','p.id','t_log_users.id_user')->whereNull('deleted_at')->orderBy('t_log_users.created_at','desc')->where(function ($query) use ($search,$request) {

            if ($search != null) {
                $query->where('user_name','ilike', "%{$search}%");
                $query->orWhere('text_logs','ilike', "%{$search}%");
            } 
        })->select('t_log_users.*','user_name');

        $totalFiltered = $posts->count();
        $totalData = $posts->count();
        $posts = $posts->limit($limit)->offset($start)->get();

        $data = array();
        if (!empty($posts)) {

            $no = 0;
           
            foreach ($posts as $d) {
               $no = $no + 1;

                $nestedData['no']           = $no;
                $nestedData['name']         = $d->user_name;
                $nestedData['menu']    = $d->menu;
                $nestedData['aktifitas']       = $d->text_logs;
                $nestedData['tanggal']       = date('d F Y H:i',strtotime($d->created_at));
                $data[]                     = $nestedData;
            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data,
        );
        echo json_encode($json_data);


    }

    public function list_data_seller(Request $request)
    {

        $limit = $request->input('length');
        $start = $request->input('start');
        $dir   = $request->input('order.0.dir');
        $search = $request->input('search.value');

        $posts = LogUser::join('t_sellers as p','p.id','t_log_users.id_user')->join('t_users as u','u.id','p.id_user')->whereNull('deleted_at')->orderBy('t_log_users.created_at','desc')->where(function ($query) use ($search,$request) {

            if ($search != null) {
                $query->where('user_name','ilike', "%{$search}%");
                $query->orWhere('text_logs','ilike', "%{$search}%");
            } 
        })->select('t_log_users.*','user_name');

        $totalFiltered = $posts->count();
        $totalData = $posts->count();
        $posts = $posts->limit($limit)->offset($start)->get();

        $data = array();
        if (!empty($posts)) {

            $no = 0;
           
            foreach ($posts as $d) {
               $no = $no + 1;

                $nestedData['no']           = $no;
                $nestedData['name']         = $d->user_name;
                $nestedData['menu']    = $d->menu;
                $nestedData['aktifitas']       = $d->text_logs;
                $nestedData['tanggal']       = date('d F Y H:i',strtotime($d->created_at));
                $data[]                     = $nestedData;
            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data,
        );
        echo json_encode($json_data);

    }

}
<?php

namespace App\Http\Controllers;

use App\Models\Administrator;
use App\Models\RoleModel;
use App\Models\CabangModel;
use App\Classes\upload;
use App\Traits\Fungsi;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Exceptions\Handler;
use Illuminate\Support\Str;

class AdministratorController extends Controller
{
    public function index()
    {
        // dd(Administrator::get());
        try {
            $data = parent::sidebar();
            $data['header_title'] = "List Data Admin";
            if ($data['access'] == 0) {
                return redirect('/');
            } else {
                $role_id           = Auth::guard('admin')->user()->id_role;
                $data['data_role'] = RoleModel::whereNull('deleted_at')->where('status', 1)->get();
                return view('admin.index', $data);
            }
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'Administrator@index';
            $insert_error = parent::InsertErrorSystem($data);
            $error = parent::sidebar();
            $error['id'] = $insert_error;
            return view('errors.index',$error); // jika Metode Get
            //return response()->json($data); // jika metode Post
        }
    }

    public function list_data(Request $request)
    {
        
        $totalData = Administrator::whereNull('deleted_at')->count();

        $limit = $request->input('length');
        $start = $request->input('start');
        $dir   = $request->input('order.0.dir');

        $posts = DB::table('t_administrators as a')->join('t_roles as t', 't.id', 'a.id_role')
            ->select('admin_name', 't.name as role_name', 'email', 'a.status', 'a.id','t.slug')
            ->whereNull('a.deleted_at')
            ->orderBy('admin_name');

        $search = $request->input('search.value');

        if ($search) {
            $posts = $posts->where('admin_name', 'ilike', "%{$search}%");
        }

        $totalFiltered = $posts->count();

        $posts = $posts->limit($limit)->offset($start)->get();

        $data = array();
        if (!empty($posts)) {
            $no = 0;
            foreach ($posts as $d) {
                $no = $no + 1;

                if ($d->status == 1) {
                    $status = '<div style="float: left; margin-left: 5px;"><a id="' . $d->id . '" aksi="nonaktif" tujuan="' . 'Administrator' . '" data="' . 'data_admin' . '" class="btn btn-success btn-sm aksi">Aktif</a></div>';
                } else {
                    $status = '<div style="float: left; margin-left: 5px;"><a id="' . $d->id . '" aksi="aktif" tujuan="' . 'Administrator' . '" data="' . 'data_admin' . '" class="btn btn-danger btn-sm aksi">Non Aktif</a></div>';
                }

                $action = '<a href="/administrator/list-admin/'.base64_encode($d->id).'" class="btn btn-sm font-sm rounded btn-success me-2 d_detail"> <i class="material-icons md-edit"></i> Edit </a>';
                // ganti password
                $action .= '<a href="javascript:void(0)" class="btn btn-sm font-sm rounded  btn-info me-2 edit_password" id="' . $d->id . '"> <i class="material-icons md-person"></i> Ganti Password </a>';
                //delete
                if ($d->slug != 'superadmin'){
                    $action .= '<a href="javascript:void(0)" class="btn btn-sm font-sm rounded  btn-danger aksi btn-aksi" id="' . $d->id . '" aksi="delete" tujuan="' . 'Administrator' . '" data="' . 'data_admin' . '"> <i class="material-icons md-delete"></i> Hapus </a>';
                }

                $column['name']      = $d->admin_name;
                $column['role_name'] = $d->role_name;
                $column['email']     = $d->email;
                $column['status']    = $status;
                $column['actions']   = $action;
                $data[]              = $column;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data,
        );

        echo json_encode($json_data);

    }

    public function post(Request $request)
    {
        try {
            $input = $request->all();
            if ($request->file('profile')) {
                $profile  = parent::uploadFileS3($request->file('profile'));
            } else {
                $profile = null;
            }

            //$profile = null;

            //LYWNdkyX8koF3ReBQGELUKMgq7GIZcuBBRH4dyD8.png
            $id_admin                  = Auth::guard('admin')->user()->id;
            $input['status']           = 'aktif';
            $input['password']         = Hash::make($request->password);
            $input['confirm_password'] = Hash::make($request->confirm_password);
            $insertadmin=array(
                        'admin_name'=>$request->name
                        ,'phone'=>$request->phone
                        ,'email'=>$request->email
                        ,'address'=>$request->address
                        ,'gender'=>$request->gender
                        ,'password'=> Hash::make($request->password)
                        ,'confirm_password'=>encrypt($request->password)
                        ,'id_role'=>$request->role_id
                        ,'profile'=>$profile
                        ,'status'=>1
                        ,'created_at'=>date('Y-m-d H:i:s')
                        ,'updated_at'=>date('Y-m-d H:i:s')
                        );
            $insert = Administrator::insertGetId($insertadmin);
            
            if ($insert) {
                $actifity = 'Menambah Data Admin';
                $object = Administrator::where('email',$request->email)->first();
                //parent::create_log($actifity, $object->id, null);
                $insert_log      = parent::LogAdmin(\Request::ip(),Auth::guard('admin')->user()->id,'Menambah Data Admin '.$request->name.'','Administrator');
                $data['code']    = 200;
                $data['message'] = 'Berhasil ' . $actifity;

                return response()->json($data);
            } else {
                $data['code']    = 500;
                $data['message'] = 'Maaf Ada yang Error ';
                return response()->json($data);
            }
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'Administrator@postAdmin';
            $insert_error = parent::InsertErrorSystem($data);
            return response()->json($data); // jika metode Post
        }
    }

    public function detail($id)
    {
        $id = base64_decode($id);
        $id = parent::cleanHazard($id);
        //dd($id);
        try {
            $data = parent::sidebar();
            $data['header_title'] = "Edit Data Admin";
            //dd($data);
            if ($data['access'] == 0) {
                return redirect('/');
            } else {
                $role_id           = Auth::guard('admin')->user()->id_role;
                $data['data_role'] = RoleModel::whereNull('deleted_at')->where('status', 1)->get();
                $isUuid = Str::isUuid($id);
                if ($isUuid == true) {
                    $admin = Administrator::find($id);
                    $data['data']      = $admin;
                    
                    return view('admin.dialog_edit', $data);
                } else {
                    $data['error_message'] = "Data dengan ID tersebut tidak ditemukan";
                    $data['link_back'] = "/administrator/list-admin";
                    return view('errors.empty_data',$data);
                }
            }
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'Administrator@detail';
            $insert_error = parent::InsertErrorSystem($data);
            $error = parent::sidebar();
            $error['id'] = $insert_error;
            return view('errors.index',$error); // jika Metode Get
            //return response()->json($data); // jika metode Post
        }
    }

    public function change_password($id)
    {
        $admin = Administrator::where('id',$id)->first();
        if ($admin != null) {
            $data['data'] = $admin;
        } else {
            $ustadz = Administrator::where('id_ustadz',$id)->first();
            $data['data'] = $ustadz;
        }
        
        return view('admin.edit_password', $data);
    }

    public function update_password(Request $request)
    {
        try {
            if ($request->new_password == $request->confirm_password) {
                $update_password = array(
                    'password'         => Hash::make($request->new_password),
                    'confirm_password' => Hash::make($request->confirm_password),
                    'updated_at'       => date('Y-m-d H:i:s'),
                );
                Administrator::find($request->id)->update($update_password);
                $insert_log      = parent::LogAdmin(\Request::ip(),Auth::guard('admin')->user()->id,'Mengubah Password Admin '.$request->id.'','Administrator');
                $data['code']    = 200;
                $data['message'] = 'Berhasil Mengubah Password';
                return response()->json($data);
            } else {
                $data['code']    = 500;
                $data['message'] = 'Old Password is wrong !';
                return response()->json($data);
            }
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'Administrator@update_password';
            $insert_error = parent::InsertErrorSystem($data);
            return response()->json($data); // jika metode Post
        }
    }

    public function update(Request $request)
    {
        try {
            $input = $request->all();
            $data_admin = Administrator::where('id',$request->id)->get();
            //dd("masuk sini");
            if ($request->file('profile')) {
                $input['profile']  = parent::uploadFileS3($request->file('profile'));
            } else {
                $input['profile'] = $data_admin[0]->profile;
            }

            $update = Administrator::find($request->id)->update($input);
            if ($update) {
                $data['code']    = 200;
                $data['message'] = 'Berhasil Mengupdate Data Admin';
                $insert_log      = parent::LogAdmin(\Request::ip(),Auth::guard('admin')->user()->id,'Mengubah Data Admin '.$request->id.'','Administrator');
                return response()->json($data);
            } else {
                $data['code']    = 500;
                $data['message'] = 'Maaf Ada yang Error ';
                return response()->json($data);
            }
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'Administrator@update';
            $insert_error = parent::InsertErrorSystem($data);
            return response()->json($data); // jika metode Post
        }
    }

    public function nonactive(Request $request)
    {
        try {
            $admin         = Administrator::find($request->id);
            $admin->status = 0;
            $admin->save();
            if ($admin) {
                $data['code']    = 200;
                $data['message'] = 'Berhasil Menonaktifkan Data Admin';
                $insert_log      = parent::LogAdmin(\Request::ip(),Auth::guard('admin')->user()->id,'Menon aktifkan Data Admin '.$request->id.'','Administrator');
                return response()->json($data);
            } else {
                $data['code']    = 500;
                $data['message'] = 'Maaf Ada yang Error ';
                return response()->json($data);
            }
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'Administrator@nonaktif';
            $insert_error = parent::InsertErrorSystem($data);
            return response()->json($data); // jika metode Post
        }
    }

    public function active(Request $request)
    {
        try {
            $admin         = Administrator::find($request->id);
            $admin->status = 1;
            $admin->save();
            if ($admin) {
                $data['code']    = 200;
                $data['message'] = 'Berhasil Mengaktifkan Data Admin';
                $insert_log      = parent::LogAdmin(\Request::ip(),Auth::guard('admin')->user()->id,'Mengaktifkan Data Admin '.$request->id.'','Administrator');
                return response()->json($data);
            } else {
                $data['code']    = 500;
                $data['message'] = 'Maaf Ada yang Error ';
                return response()->json($data);
            }
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'Administrator@active';
            $insert_error = parent::InsertErrorSystem($data);
            return response()->json($data); // jika metode Post
        }
    }

    public function delete(Request $request)
    {
        try {
            $admin             = Administrator::find($request->id);
            $admin->status     = 0;
            $admin->deleted_at = date('Y-m-d');
            $admin->save();

            if ($admin) {
                $data['code']    = 200;
                $data['message'] = 'Berhasil Menghapus Data Admin';
                $insert_log      = parent::LogAdmin(\Request::ip(),Auth::guard('admin')->user()->id,'Menghapus Data Admin '.$request->id.'','Administrator');
                return response()->json($data);
            } else {
                $data['code']    = 500;
                $data['message'] = 'Maaf Ada yang Error ';
                return response()->json($data);
            }
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'Administrator@delete';
            $insert_error = parent::InsertErrorSystem($data);
            return response()->json($data); // jika metode Post
        }
    }

    public function get_role($id,$jenis)
    {
        $cek = DB::table('roles')->where('id',$id)->pluck('slug');
        $data['jenis'] = $cek[0];
        //dd($data['jenis']);
        if ($jenis == 'company') {

            $data['data_company'] = DB::table('company_registrations')->whereNull('is_delete')->get();

            return view('admin.get_company', $data);
        } else {
            return view('admin.get_kode', $data);
        }
        
    }

}

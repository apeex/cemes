<?php

namespace App\Http\Controllers;

use App\Models\CountryModel;
use App\Models\ProvinceModel;
use App\Models\CityModel;
use App\Models\DistrictModel;
use App\Models\VillageModel;
use App\Models\RoleModel;
use App\Traits\Fungsi;
use Auth;
use DB;
use Illuminate\Http\Request;
use Redirect;
use App\Exceptions\Handler;

class VillageController extends Controller
{
    

    public function index()
    {
        // dd("Ok");
        try {
            $role_id = Auth::guard('admin')->user()->id_role;
            $data = parent::sidebar();
            if ($data['access'] == 0) {
                return redirect('/');
            } else {
                $data['name_adm'] = Auth::guard('admin')->user()->name;
                
                $data['data_country'] = CountryModel::whereNull('deleted_at')->get();
                

                return view('master.village.index',$data);
            }
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'VillageController@';
            $insert_error = parent::InsertErrorSystem($data);
            $error = parent::sidebar();
            $error['id'] = $insert_error;
            return view('errors.index',$error); // jika Metode Get
            //return response()->json($data); // jika metode Post
        }
    }

    public function list_data(Request $request)
    {

        $totalData = DB::table('villages')->whereNull('deleted_at')->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $dir   = $request->input('order.0.dir');

        $search = $request->input('search.value');

        $posts = VillageModel::join('districts as d','d.id','villages.district_id')->join('cities as c','c.id','d.regency_id')->whereNull('villages.deleted_at')->where(function ($query) use ($search,$request) {
            if ($search != null) {
                $query->where('villlages.name','ilike', "%{$search}%");
                $query->orWhere('d.name','ilike', "%{$search}%");
                $query->orWhere('c.name','ilike', "%{$search}%");
            } 
        })->orderBy('villages.updated_at','desc')->select('villages.*','d.name as kecamatan','c.name as kota');
        $totalFiltered = $posts->count();
        $posts = $posts->limit($limit)->offset($start)->get();

        $data = array();
        if (!empty($posts)) {

            $no = 0;
           
            foreach ($posts as $d) {
               $no = $no + 1;

                if ($d->status == 1) {
                    $status = '<div style="float: left; margin-left: 5px;">
                        <a id="' . $d->id . '" aksi="nonaktif" tujuan="village" data="data_master_village" class="btn btn-success btn-sm aksi">Aktif</a>
                        </div>';
                } else {
                    $status = '<div style="float: left; margin-left: 5px;">
                        <button type="button" id="' . $d->id . '"data="data_master_village" aksi="aktif" tujuan="village" class="btn btn-danger btn-sm aksi">Tidak Aktif</button>
                        </div>';
                }


                $action = '<div style="float: left; margin-left: 5px;"><a href="/master/desa/'.$d->id.'">
                    <button type="button" class="btn btn-warning btn-sm font-sm" style="min-width: 110px;margin-left: 2px;margin-top:3px;text-align:left"><i class="fa fa-edit"></i> Edit</button></a>
                </div>
                <div style="float: left; margin-left: 5px;">
                    <button type="button" class="btn btn-danger btn-sm font-sm aksi btn-aksi" data="data_master_village"  id="' . $d->id . '" aksi="delete" tujuan="village" style="min-width: 110px;margin-left: 2px;margin-top:3px;text-align:left"><i class="fa fa-trash"></i> Hapus</button>
                </div>';

                $nestedData['no']           = $no;
                $nestedData['name']         = $d->name;
                $nestedData['kecamatan']    = $d->kecamatan;
                $nestedData['kota']         = $d->kota;
                $nestedData['status']       = $status;
                $nestedData['action']       = $action;
                $data[]                     = $nestedData;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data,
        );
        echo json_encode($json_data);


    }

    public function get_villages($district_id)
    {
        $data = VillageModel::where('district_id', $district_id)
            ->whereNull('deleted_at')
            ->orderBy('name', 'asc')->get();

        echo json_encode($data);
    }

    public function post(Request $request)
    {
        try {
            $input      = $request->all();
            $insertlog=array(
                            'id'    => rand(10000,99999)
                            ,'district_id'  => $request->district_id
                            ,'name'          => $request->name
                            ,'created_at'=>date('Y-m-d H:i:s')
                            ,'updated_at'=>date('Y-m-d H:i:s')
                            );
            $insert = DB::table('villages')->insert($insertlog);
            if ($insert) {
                $insert_log      = parent::LogAdmin(\Request::ip(),Auth::guard('admin')->user()->id,'Menambah Data kelurahan '.$request->name.'','kelurahan');
                $data['code']    = 200;
                $data['message'] = 'Berhasil menambah Data District';
                return response()->json($data);
            } else {
                $data['code']    = 500;
                $data['message'] = 'Maaf Ada yang Error ';
                return response()->json($data);
            }
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'VillageController@post';
            $insert_error = parent::InsertErrorSystem($data);
            return response()->json($data); // jika metode Post
        }
    }

   
    public function detail($id)
    {
        try {
            $query = VillageModel::find($id);
            
            $data = parent::sidebar();
            if ($data['access'] == 0) {
                return redirect('/');
            } else {
                $data['name_adm'] = Auth::guard('admin')->user()->name;
                $cek              = VillageModel::where('id',$id)->get();
                if ($cek->isNotEmpty()) {
                    
                    $data['name_adm'] = Auth::guard('admin')->user()->name;
                    $district           = DistrictModel::find($query->district_id);
                    $city               = CityModel::find($district->regency_id);
                    // dd($district);
                    $province           = ProvinceModel::find($city->province_id);
                    $query->city_id     = $district->regency_id;
                    $query->province_id = $city->province_id;
                    $query->country_id  = $province->country_id;
                    $data['data']       = $query;
                    $data['data_country'] = CountryModel::whereNull('deleted_at')->where('status', 1)->get();
                    $data['provinces']  = ProvinceModel::where('country_id', $province->country_id)->whereNull('deleted_at')->get();
                    $data['cities']     = CityModel::where('province_id', $city->province_id)->get();
                    $data['districts']  = DistrictModel::where('regency_id', $district->regency_id)->whereNull('deleted_at')->get();
                    // return response()->json($data);
                    return view('master.village.dialog_edit', $data);

                } else {
                    $data['error_message'] = "Data dengan ID tersebut tidak ditemukan";
                    $data['link_back'] = "/master/desa";
                    return view('errors.empty_data',$data);
                }
            }
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'VillageController@detail';
            $insert_error = parent::InsertErrorSystem($data);
            $error = parent::sidebar();
            $error['id'] = $insert_error;
            return view('errors.index',$error); // jika Metode Get
            //return response()->json($data); // jika metode Post
        }
    }

    public function nonactive(Request $request)
    {
        try {
            $nonactive         = VillageModel::find($request->id);
            $nonactive->status = 0;
            $nonactive->save();

            if ($nonactive) {
                $id_admin    = Auth::guard('admin')->user()->id;
                $insert_log      = parent::LogAdmin(\Request::ip(),Auth::guard('admin')->user()->id,'Menon aktifkan Data kelurahan '.$request->id.'','kelurahan');
                $data['code']    = 200;
                $data['message'] = 'Berhasil Memblokir Data Village';
                return response()->json($data);
            } else {
                $data['code']    = 500;
                $data['message'] = 'Maaf Ada yang Error ';
                return response()->json($data);
            }
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'VillageController@nonactive';
            $insert_error = parent::InsertErrorSystem($data);
            return response()->json($data); // jika metode Post
        }
    }

    public function active(Request $request)
    {
        try {
            $active         = VillageModel::find($request->id);
            $active->status = 1;
            $active->save();

            if ($active) {
                $id_admin    = Auth::guard('admin')->user()->id;
                $insert_log      = parent::LogAdmin(\Request::ip(),Auth::guard('admin')->user()->id,'Mengaktifkan Data kelurahan '.$request->id.'','kelurahan');
                $data['code']    = 200;
                $data['message'] = 'Berhasil Mengaktifkan Data Village';
                return response()->json($data);
            } else {
                $data['code']    = 500;
                $data['message'] = 'Maaf Ada yang Error ';
                return response()->json($data);
            }
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'VillageController@';
            $insert_error = parent::InsertErrorSystem($data);
            return response()->json($data); // jika metode Post
        }
    }

    public function delete(Request $request)
    {
        try {
        // $delete             = VillageModel::where('id',$request->id)->get();
            $delete             = VillageModel::find($request->id);
            $delete->status     = 0;
            $delete->deleted_at = date('Y-m-d H:i:s');
            $delete->save();

            if ($delete) {
                $insert_log      = parent::LogAdmin(\Request::ip(),Auth::guard('admin')->user()->id,'Menghapus Data kelurahan '.$request->id.'','kelurahan');
                $data['code']    = 200;
                $data['message'] = 'Berhasil Menghapus Data Village';
                return response()->json($data);
            } else {
                $data['code']    = 500;
                $data['message'] = 'Maaf Ada yang Error ';
                return response()->json($data);
            }
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'VillageController@';
            $insert_error = parent::InsertErrorSystem($data);
            return response()->json($data); // jika metode Post
        }
    }

    public function update(Request $request)
    {
        try {
            $input       = $request->all();
            $id_admin    = Auth::guard('admin')->user()->id;
            $insertadmin = array(
                'district_id'  => $request->district_id,
                'name'          => $request->name,
                'updated_at'   => date('Y-m-d H:i:s'),
            );
            $insert = VillageModel::where('id', $request->id)->update($insertadmin);
            if ($insert) {
                $insert_log      = parent::LogAdmin(\Request::ip(),Auth::guard('admin')->user()->id,'Mengubah Data kelurahan '.$request->name.'','kelurahan');
                $data['code']    = 200;
                $data['message'] = 'Berhasil Mengupdate Data Country';
                return response()->json($data);
            } else {
                $data['code']    = 500;
                $data['message'] = 'Maaf Ada yang Error ';
                return response()->json($data);
            }
        } catch (\Exception $e) {
            $data['code']    = 500;
            $data['message'] = $e->getMessage();
            $data['line'] = $e->getLine();
            $data['controller'] = 'VillageController@update';
            $insert_error = parent::InsertErrorSystem($data);
            return response()->json($data); // jika metode Post
        }
    }
}

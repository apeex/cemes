<?php
function rcheck($menu_id,$id) {

    $menu = DB::select("select b.id as id,b.name as name,b.menu_id as menu_id from t_menus b join t_role_menus r on b.id = r.menu_id where r.role_id = '$id' and b.parent_menu_id = '0' and b.menu_id = $menu_id;"); 
    if (!empty($menu)) {
        $return = $menu[0]->menu_id;
    } else {
       $return = 500;
    }
    return $return;
}

function rcheck2($menu_id,$id,$parent_menu_id) {
    
    $cek_dulu = DB::table('t_role_menus')->where('role_id',$id)->where('menu_id',$menu_id)->get();
    if ($cek_dulu->isNotEmpty()) {
        $menu = DB::select("select b.id as id,b.name as name,b.parent_menu_id as parent_menu_id,b.slug as slug,b.menu_id as menu_id from t_menus b join t_role_menus r on b.id = r.menu_id where r.role_id = '$id' and b.menu_id = '0' and b.parent_menu_id = '$parent_menu_id';");
        $return = $menu[0]->parent_menu_id;
        
    } else {
       $return = 500;
    }
    return $return;
}

function HasSubmenu($id){
    $return = 0;
    $menu_id = DB::table('t_menus')->where('id',$id)->pluck('menu_id');
    $cek = DB::table('t_menus')->where('parent_menu_id',$menu_id[0])->get();
    if ($cek->isNotEmpty()) {
        $return = 1;
    }
    return $return;

}

function NamaTes($nomor){
    //jumlah panggil
    $jumlah_p = DB::table('interview_member')->where('nomor',$nomor)->pluck('name');
    return $jumlah_p[0];       
}

function nama_pend($id){
    $nama = DB::table('tb_form_pendidikan')->select('name')->where('id',$id)->pluck('name');
    return $nama[0];
}


function TipePerusahaan($id) {
    $data = DB::table('master_tipe_perusahaan')->where('id',$id)->pluck('tipe');
    if ($data->isNotEmpty()) {
        if ($data[0] == 'Perorangan' || $data[0] == 'PERORANGAN') {
            $return = 1;
        } else {
            $return = 0;
        }
    } else {
        $return = 0;
    }
}

function image_blank() {
    $img = "https://e-brochure.s3-id-jkt-1.kilatstorage.id/dfb9gm4oFdQBwy32dUaBOUkfMoWL01Hx3KC0aF67.svg";
    return $img;
}

function RelationPaket($id_product,$id_paket){
    $return = "";
    $cek = DB::table('relation_product_packages')->where('id_product',$id_product)->where('id_package',$id_paket)->get();
    if ($cek->isNotEmpty()) {
        $return = "checked";
    }
    return $return;
}

function JumlahMeja($id){
    
    $return = 0;
    $jumlah = DB::table('table_branchs')->where('id',$id)->get();
    if ($jumlah->isNotEmpty()) {
        $return = $jumlah[0]->number_of_cashier;
    }

    return $return;
}
// tanggal indo
function HariIndo($tanggal,$cetak_hari = false){
    $hari = array ( 1 =>'Senin',
                'Selasa',
                'Rabu',
                'Kamis',
                'Jumat',
                'Sabtu',
                'Minggu'
            );        
    
    $num = date('N', strtotime($tanggal));
        
    return $hari[$num];
}

function base_img(){
    $url = "https://kandidat.s3-id-jkt-1.kilatstorage.id/";
    return $url;
}

function base_img_local(){
    $url = URL::to('/').'/assets/file_hasil/';
    return $url;
}


function getUmur($tgl) {
    if ($tgl != null) {
        $date1 = $tgl;
        $date2 = date('Y-m-d');
        $diff = abs(strtotime($date2) - strtotime($date1));
        $years = floor($diff / (365*60*60*24));
        $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
        $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

        $data_umur = $years.' thn  '.$months.' bln  '.$days.' hr';
         //exit();
    } else {
        $data_umur = '(Belum diisi)';
    }
    
    $return = $data_umur;

    return $return;

}

function getUmurThn($tgl) {
    if ($tgl != null) {
        $date1 = $tgl;
        $date2 = date('Y-m-d');
        $diff = abs(strtotime($date2) - strtotime($date1));
        $years = floor($diff / (365*60*60*24));
        $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
        $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

        $data_umur = $years.' th';
         //exit();
    } else {
        $data_umur = ' - ';
    }
    
    $return = $data_umur;

    return $return;

}


function DataJurusan($id){
    $data = DB::table('major')->where('id_departement',$id)->get();
    return $data;
}

function CheckAdm($id,$type){
    if ($type == 'jurusan') {
        $return = DB::table('roles')->where('id',$id)->pluck('id_jurusan');
    } else {
        $return = DB::table('roles')->where('id',$id)->pluck('id_departement');
    }
    
    return $return[0];
}

function timeConverter($day)
{
    date_default_timezone_set("Asia/Jakarta");
    $time = new DateTime($day);
    $diff = $time->diff(new DateTime());
    $minutes = ($diff->days * 24 * 60) +
               ($diff->h * 60) + $diff->i;
    if ($minutes < 1440){
        if ($minutes > 60){
            $jam = $minutes/60;
            $lebih = $minutes%60;

            $return = (int)$jam." Jam yang lalu";
        } else {
             $return = $minutes." Menit yang lalu";
        }
      
    } else {
       $day = $minutes / 1440;
       $return = (int)$day." Hari yang lalu";
    }
    return $return;
}
function DataProvince($id){
    $data = DB::table('provinces')->where('country_id',$id)->get();
    
    return $data;
}

function DataCity($id){
    $data = DB::table('cities')->where('province_id',$id)->get();
    return $data;
}

function DataDistrict($id){
    $data = DB::table('districts')->where('regency_id',$id)->get();
    return $data;
}

function DataVillage($id){
    $data = DB::table('villages')->where('district_id',$id)->get();
    return $data;
}

function GetAddressName($table,$id){
    $return = "Belum Ditentukan";
    $data = DB::table($table)->where('id',$id)->pluck('name');
    if ($data->isNotEmpty()) {
        $return = $data[0];
    }
    return $return;
}

function WeigthOfShed($id,$type){
    $return = 0;
    if ($type == 1) {
        $data = DB::table('t_shed_histories')->where('id_new_shed',$id)->where('status',1)->count();
        $return = (float)$data;
    } else {
        $data = DB::table('t_shed_histories')->where('id_new_shed',$id)->where('status',1)->pluck('id_cattle');
        if ($data->isNotEmpty()) {
            $sum = DB::table('t_cattles')->whereIn('id',$data->toArray())->sum('weigth');
            $return = (float)$sum;
        }
    }
    return $return;
}

function GetColoumnValue($id,$table,$coloumn){
    $return = "Tidak diketahui";
    $data = DB::table($table)->where('id',$id)->pluck($coloumn);
    if ($data->isNotEmpty()) {
        $return = $data[0];
    }
    return $return;
}

function getAge($tgl) {
    if ($tgl != null) {
        $date1 = $tgl;
        $date2 = date('Y-m-d');
        $diff = abs(strtotime($date2) - strtotime($date1));
        $years = floor($diff / (365*60*60*24));
        $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
        $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
        if($years < 1){
            $data_umur = $months.' Bulan';
        } else {
            $data_umur = $years.','.$months.' Tahun';
        }
         //exit();
    } else {
        $data_umur = '(Belum diisi)';
    }
    
    $return = $data_umur;

    return $return;
}


?>
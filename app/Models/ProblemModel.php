<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ProblemModel extends Model
{
    protected $table 	= 't_master_problems';
    protected $guarded = [''];
    protected $hidden   = ['created_at','updated_at'];
    public $incrementing = false;
    protected $keyType = 'uuid';

}
<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class MajelisModel extends Model
{
    protected $table 	= 't_majelis_taklim';
    protected $guarded = [''];
    public $timestamps = true;
    public $incrementing = false;
    protected $keyType = 'uuid';

}
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class MenuModel extends Model
{
    protected $table 	= 't_menus';
    protected $guarded = [''];
    protected $hidden   = ['created_at','updated_at'];
    public $incrementing = false;
    protected $keyType = 'uuid';

}
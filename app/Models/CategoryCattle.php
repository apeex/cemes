<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class CategoryCattle extends Model
{
    protected $table 	= 't_cattle_categories';
    protected $guarded = [''];
    public $timestamps = true;
    public $incrementing = false;
    protected $keyType = 'uuid';

}
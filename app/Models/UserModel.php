<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class UserModel extends Model
{
    protected $table 	= 't_users';
    protected $guarded = [''];
    protected $hidden   = ['created_at','updated_at'];
    public $incrementing = false;
    protected $keyType = 'uuid';

}
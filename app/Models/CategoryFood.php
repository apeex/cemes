<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class CategoryFood extends Model
{
    protected $table 	= 't_food_categories';
    protected $guarded = [''];
    protected $hidden   = ['created_at','updated_at'];
    public $incrementing = false;
    protected $keyType = 'uuid';

}
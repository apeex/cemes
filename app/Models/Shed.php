<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Shed extends Model
{
    protected $table 	= 't_sheds';
    protected $guarded = [''];
    protected $hidden   = ['created_at','updated_at'];
    public $incrementing = false;
    protected $keyType = 'uuid';

}
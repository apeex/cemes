<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class EventModel extends Model
{
    protected $table 	= 't_events';
    protected $guarded = [''];
    protected $hidden   = ['created_at','updated_at'];
    public $incrementing = false;
    protected $keyType = 'uuid';

}
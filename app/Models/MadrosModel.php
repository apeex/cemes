<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class MadrosModel extends Model
{
    protected $table 	= 't_madros';
    protected $guarded = [''];
    public $timestamps = true;
    public $incrementing = false;
    protected $keyType = 'uuid';

}
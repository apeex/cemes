<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class BannerModel extends Model
{
    protected $table 	= 't_banners';
    protected $guarded = [''];
    public $timestamps = true;
    public $incrementing = false;
    protected $keyType = 'uuid';

}
<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class MadrasahModel extends Model
{
    protected $table 	= 't_madrasah';
    protected $guarded = [''];
    public $timestamps = true;
    public $incrementing = false;
    protected $keyType = 'uuid';

}
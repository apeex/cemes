<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class RoleModel extends Model
{
    protected $table 	= 't_roles';
    protected $guarded = [''];
    protected $hidden   = ['created_at','updated_at'];
    public $incrementing = false;
    protected $keyType = 'uuid';

}
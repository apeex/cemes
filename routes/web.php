
<?php

use App\Http\Controllers\AdministratorController;
use App\Http\Controllers\BannerController;
use App\Http\Controllers\CategoryCattleController;
use App\Http\Controllers\CategoryFoodController;
use App\Http\Controllers\CattleController;
use App\Http\Controllers\CityController;
use App\Http\Controllers\CountryController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\DistrictController;
use App\Http\Controllers\EventController;
use App\Http\Controllers\FarmerController;
use App\Http\Controllers\FoodController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\LaporanController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\ShedController;
use App\Http\Controllers\TransactionController;
use App\Http\Controllers\ProblemController;
use App\Http\Controllers\ProvinceController;
use App\Http\Controllers\VillageController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| CMS Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/', function () {
    return view('login');
})->name('login');

Route::post('/login',[LoginController::class,'login']);
//dashboard
Route::get('/dashboard',[DashboardController::class,'index'])->middleware('auth:admin');

// administrator
Route::get('/administrator/list-admin',[AdministratorController::class,'index'])->middleware('auth:admin');
Route::post('/data_admin',[AdministratorController::class,'list_data'])->middleware('auth:admin');
Route::post('/postadmin',[AdministratorController::class,'post'])->middleware('auth:admin');
Route::post('/aktif/Administrator',[AdministratorController::class,'active'])->middleware('auth:admin');
Route::post('/nonaktif/Administrator',[AdministratorController::class,'nonactive'])->middleware('auth:admin');
Route::post('/delete/Administrator',[AdministratorController::class,'delete'])->middleware('auth:admin');
Route::get('/administrator/list-admin/{id}',[AdministratorController::class,'detail'])->middleware('auth:admin');
Route::post('/update/administrator',[AdministratorController::class,'update'])->middleware('auth:admin');
Route::get('/change-password/{id}',[AdministratorController::class,'change_password'])->middleware('auth:admin');
Route::post('/update-password',[AdministratorController::class,'update_password'])->middleware('auth:admin');
Route::get('/select_role/{id}/{jenis}',[AdministratorController::class,'get_role'])->middleware('auth:admin');

//role
Route::get('/administrator/role-admin',[RoleController::class,'index'])->middleware('auth:admin');
Route::post('/post_role',[RoleController::class,'post'])->middleware('auth:admin');
Route::get('/data_role',[RoleController::class,'list_data'])->middleware('auth:admin');
Route::get('/administrator/role-admin/{id}',[RoleController::class,'detail'])->middleware('auth:admin');
Route::post('/nonaktif/role',[RoleController::class,'nonactive'])->middleware('auth:admin');
Route::post('/aktif/role',[RoleController::class,'active'])->middleware('auth:admin');
Route::post('/delete/role',[RoleController::class,'delete'])->middleware('auth:admin');
Route::post('/update/role', [RoleController::class,'update'])->middleware('auth:admin');

// ...........countries
Route::get('/master/negara',[CountryController::class,'index'])->middleware('auth:admin');
Route::get('/data_master_country',[CountryController::class,'list_data'])->middleware('auth:admin');
Route::post('/post_master_country',[CountryController::class,'post'])->middleware('auth:admin');
Route::post('/nonaktif/master_country',[CountryController::class,'nonactive'])->middleware('auth:admin');
Route::post('/aktif/master_country',[CountryController::class,'active'])->middleware('auth:admin');
Route::post('/delete/master_country',[CountryController::class,'delete'])->middleware('auth:admin');
Route::get('/master/negara/{id}',[CountryController::class,'detail'])->middleware('auth:admin');
Route::post('/update/master_country',[CountryController::class,'update'])->middleware('auth:admin');

//banner
//Banner
Route::get('/master/banner',[BannerController::class,'index'])->middleware('auth:admin');
Route::post('/data_banner',[BannerController::class,'list_data'])->middleware('auth:admin');
Route::post('/post_banner',[BannerController::class,'post'])->middleware('auth:admin');
Route::post('/nonaktif/banner', [BannerController::class,'onactive'])->middleware('auth:admin');
Route::post('/aktif/banner', [BannerController::class,'active'])->middleware('auth:admin');
Route::post('/delete/banner', [BannerController::class,'delete'])->middleware('auth:admin');
Route::get('/master/banner/{id}', [BannerController::class,'detail'])->middleware('auth:admin');
Route::get('/get_type_banner/{id}', [BannerController::class,'get_type_banner'])->middleware('auth:admin');
Route::post('/update/banner', [BannerController::class,'update'])->middleware('auth:admin');

// ............provinces
Route::get('/master/provinsi',[ProvinceController::class,'index'])->middleware('auth:admin');
Route::get('/data_master_province',[ProvinceController::class,'list_data'])->middleware('auth:admin');
Route::post('/post_master_province',[ProvinceController::class,'post'])->middleware('auth:admin');
Route::post('/nonaktif/master_province',[ProvinceController::class,'nonactive'])->middleware('auth:admin');
Route::post('/aktif/master_province', [ProvinceController::class,'active'])->middleware('auth:admin');
Route::get('/master/provinsi/{id}',[ProvinceController::class,'detail'])->middleware('auth:admin');
Route::post('/update/master_province',[ProvinceController::class,'update'])->middleware('auth:admin');
Route::post('/delete/master_province',[ProvinceController::class,'delete'])->middleware('auth:admin');
// ................cities

Route::get('/master/kota',[CityController::class,'index'])->middleware('auth:admin');
Route::post('/data_master_city',[CityController::class,'list_data'])->middleware('auth:admin');
Route::get('/data_province/{country_id}',[CityController::class,'province'])->middleware('auth:admin');
Route::post('/post_master_city',[CityController::class,'post'])->middleware('auth:admin');
Route::post('/aktif/city', [CityController::class,'active'])->middleware('auth:admin');
Route::post('/nonaktif/city',[CityController::class,'nonactive'])->middleware('auth:admin');
Route::post('/delete/city',[CityController::class,'delete'])->middleware('auth:admin');
Route::get('/master/kota/{id}',[CityController::class,'detail'])->middleware('auth:admin');
Route::post('/update/master_city',[CityController::class,'update'])->middleware('auth:admin');
Route::get('/cities/{province_id}',[CityController::class,'get_cities'])->middleware('auth:admin');

// .............district
Route::get('/master/kecamatan',[DistrictController::class,'index'])->middleware('auth:admin');
Route::post('/data_master_district',[DistrictController::class,'list_data'])->middleware('auth:admin');
Route::post('/aktif/district', [DistrictController::class,'active'])->middleware('auth:admin');
Route::post('/nonaktif/district', [DistrictController::class,'nonactive'])->middleware('auth:admin');
Route::get('/master/kecamatan/{id}', [DistrictController::class,'detail'])->middleware('auth:admin');
Route::get('/districts/{city_id}', [DistrictController::class,'get_districts'])->middleware('auth:admin');
Route::post('/post_master_district',[DistrictController::class,'post'])->middleware('auth:admin');
Route::post('/update/master_district',[DistrictController::class,'update'])->middleware('auth:admin');
Route::post('/delete/district',[DistrictController::class,'delete'])->middleware('auth:admin');

// ...........vilage
Route::get('/master/desa',[VillageController::class,'index'])->middleware('auth:admin');
Route::post('/data_master_village',[VillageController::class,'list_data'])->middleware('auth:admin');
Route::get('/villages/{district_id}', [VillageController::class,'get_villages'])->middleware('auth:admin');
Route::post('/post_master_village',[VillageController::class,'post'])->middleware('auth:admin');
Route::post('/aktif/village', [VillageController::class,'active'])->middleware('auth:admin');
Route::post('/nonaktif/village', [VillageController::class,'nonactive'])->middleware('auth:admin');
Route::post('/delete/village', [VillageController::class,'delete'])->middleware('auth:admin');
Route::get('/master/desa/{id}',[VillageController::class,'detail'])->middleware('auth:admin');
Route::post('/update/master_village',[VillageController::class,'update'])->middleware('auth:admin');

//notifikasi
Route::get('/notifikasi/notifikasi',[NotificationController::class,'index'])->middleware('auth:admin');
Route::post('/data_notifikasi',[NotificationController::class,'list_data'])->middleware('auth:admin');
Route::post('/post_notifikasi',[NotificationController::class,'post'])->middleware('auth:admin');
Route::post('/delete/notifikasi',[NotificationController::class,'delete'])->middleware('auth:admin');
Route::get('/search_user/{name}',[NotificationController::class,'search_user'])->middleware('auth:admin');
Route::get('/get_type_notification/{type}',[NotificationController::class,'get_type'])->middleware('auth:admin');

//cattle/category
Route::get('/cattle/category',[CategoryCattleController::class,'index'])->middleware('auth:admin');
Route::post('/data_category_cattle',[CategoryCattleController::class,'list_data'])->middleware('auth:admin');
Route::post('/post_category_cattle',[CategoryCattleController::class,'post'])->middleware('auth:admin');
Route::post('/aktif/category_cattle', [CategoryCattleController::class,'active'])->middleware('auth:admin');
Route::post('/nonaktif/category_cattle', [CategoryCattleController::class,'nonactive'])->middleware('auth:admin');
Route::post('/delete/category_cattle', [CategoryCattleController::class,'delete'])->middleware('auth:admin');
Route::get('/cattle/category/{id}',[CategoryCattleController::class,'detail'])->middleware('auth:admin');
Route::post('/update/category_cattle',[CategoryCattleController::class,'update'])->middleware('auth:admin');

///food/category
Route::get('/food/category',[CategoryFoodController::class,'index'])->middleware('auth:admin');
Route::post('/data_category_food',[CategoryFoodController::class,'list_data'])->middleware('auth:admin');
Route::post('/post_category_food',[CategoryFoodController::class,'post'])->middleware('auth:admin');
Route::post('/aktif/category_food', [CategoryFoodController::class,'active'])->middleware('auth:admin');
Route::post('/nonaktif/category_food', [CategoryFoodController::class,'nonactive'])->middleware('auth:admin');
Route::post('/delete/category_food', [CategoryFoodController::class,'delete'])->middleware('auth:admin');
Route::get('/food/category/{id}',[CategoryFoodController::class,'detail'])->middleware('auth:admin');
Route::post('/update/category_food',[CategoryFoodController::class,'update'])->middleware('auth:admin');


//farmer/list-farmer
Route::get('/farmer/list-farmer',[FarmerController::class,'index'])->middleware('auth:admin');
Route::post('/data_farmer',[FarmerController::class,'list_data'])->middleware('auth:admin');
//Route::post('/post_category_food',[FarmerController::class,'post'])->middleware('auth:admin');
Route::post('/aktif/farmer', [FarmerController::class,'active'])->middleware('auth:admin');
Route::post('/nonaktif/farmer', [FarmerController::class,'nonactive'])->middleware('auth:admin');
Route::post('/delete/farmer', [FarmerController::class,'delete'])->middleware('auth:admin');
Route::get('/food/farmer/{id}',[FarmerController::class,'detail'])->middleware('auth:admin');
Route::post('/update/farmer',[FarmerController::class,'update'])->middleware('auth:admin');

///shed/list (Kandang)
Route::get('/shed/list',[ShedController::class,'index'])->middleware('auth:admin');
Route::post('/data_shed',[ShedController::class,'list_data'])->middleware('auth:admin');
// Route::post('/post_shed',[ShedController::class,'post'])->middleware('auth:admin');
Route::post('/aktif/shed', [ShedController::class,'active'])->middleware('auth:admin');
Route::post('/nonaktif/shed', [ShedController::class,'nonactive'])->middleware('auth:admin');
Route::post('/delete/shed', [ShedController::class,'delete'])->middleware('auth:admin');
Route::get('/shed/list/{id}',[ShedController::class,'detail'])->middleware('auth:admin');
// Route::post('/update/shed',[ShedController::class,'update'])->middleware('auth:admin');

///food/list
Route::get('/food/list',[FoodController::class,'index'])->middleware('auth:admin');
Route::post('/data_food',[FoodController::class,'list_data'])->middleware('auth:admin');
// Route::post('/post_shed',[ShedController::class,'post'])->middleware('auth:admin');
Route::post('/aktif/food', [FoodController::class,'active'])->middleware('auth:admin');
Route::post('/nonaktif/food', [FoodController::class,'nonactive'])->middleware('auth:admin');
Route::post('/delete/food', [FoodController::class,'delete'])->middleware('auth:admin');
Route::get('/food/list/{id}',[FoodController::class,'detail'])->middleware('auth:admin');
Route::get('/food/history',[FoodController::class,'history'])->middleware('auth:admin');
Route::post('/data_food_history',[FoodController::class,'list_history'])->middleware('auth:admin');
Route::post('/change_farmer',[FoodController::class,'change_farmer'])->middleware('auth:admin');
Route::get('/detail_food/{id}',[FoodController::class,'detail_food'])->middleware('auth:admin');

//cattle/list
Route::get('/cattle/list',[CattleController::class,'index'])->middleware('auth:admin');
Route::post('/data_cattle',[CattleController::class,'list_data'])->middleware('auth:admin');
Route::post('/aktif/cattle', [CattleController::class,'active'])->middleware('auth:admin');
Route::post('/nonaktif/cattle', [CattleController::class,'nonactive'])->middleware('auth:admin');
Route::post('/delete/cattle', [CattleController::class,'delete'])->middleware('auth:admin');
Route::get('/cattle/list/{id}',[CattleController::class,'detail'])->middleware('auth:admin');
Route::post('/cattle/data_food_history',[CattleController::class,'list_food'])->middleware('auth:admin');
Route::post('/cattle/data_weigth_history',[CattleController::class,'list_weigth'])->middleware('auth:admin');
Route::post('/cattle/data_shed_history',[CattleController::class,'list_shed'])->middleware('auth:admin');

///master/problems
Route::get('/master/problems',[ProblemController::class,'index'])->middleware('auth:admin');
Route::post('/data_problem',[ProblemController::class,'list_data'])->middleware('auth:admin');
Route::post('/post_problem',[ProblemController::class,'post'])->middleware('auth:admin');
Route::post('/aktif/problem', [ProblemController::class,'active'])->middleware('auth:admin');
Route::post('/nonaktif/problem', [ProblemController::class,'nonactive'])->middleware('auth:admin');
Route::post('/delete/problem', [ProblemController::class,'delete'])->middleware('auth:admin');
Route::get('/master/problems/{id}',[ProblemController::class,'detail'])->middleware('auth:admin');
Route::post('/update/problem',[ProblemController::class,'update'])->middleware('auth:admin');

//master/events
Route::get('/master/events',[EventController::class,'index'])->middleware('auth:admin');
Route::post('/data_event',[EventController::class,'list_data'])->middleware('auth:admin');
Route::post('/post_event',[EventController::class,'post'])->middleware('auth:admin');
Route::post('/aktif/event', [EventController::class,'active'])->middleware('auth:admin');
Route::post('/nonaktif/event', [EventController::class,'nonactive'])->middleware('auth:admin');
Route::post('/delete/event', [EventController::class,'delete'])->middleware('auth:admin');
Route::get('/master/events/{id}',[EventController::class,'detail'])->middleware('auth:admin');
Route::post('/update/event',[EventController::class,'update'])->middleware('auth:admin');

//shed/history
Route::get('/shed/history',[ShedController::class,'history'])->middleware('auth:admin');
Route::post('/data_shed_history',[ShedController::class,'list_history'])->middleware('auth:admin');

//laporan//
Route::get('/laporan_ternak/{id}/{token}',[LaporanController::class,'laporan_ternak']);
Route::get('/laporan_pakan/{id}/{token}',[LaporanController::class,'laporan_pakan']);
Route::post('/get_graphic_ternak',[LaporanController::class,'get_graphic_ternak']);
Route::post('/get_graphic_pakan',[LaporanController::class,'get_graphic_pakan']);
Route::get('/cronjobs_tanggal_input',[LaporanController::class,'cronjobs_tanggal_input']);

//
Route::get('/transaction/list',[TransactionController::class,'index']);
Route::get('/transaction/list/{id}',[TransactionController::class,'detail']);
Route::post('/data_transaction',[TransactionController::class,'list']);
Route::post('/update_transaction',[TransactionController::class,'update']);





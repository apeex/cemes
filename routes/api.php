<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\GeraiController;
use App\Http\Controllers\GetDataController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\OnboardingController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ShopController;
use App\Http\Controllers\StoreController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('products/{id}',[ProductController::class,'detail']);
Route::post('search_product',[ProductController::class,'search']);
Route::get('products',[ProductController::class,'index']);
Route::prefix('auth')->group(function () {
	Route::post('register',[AuthController::class,'register']);
	Route::post('otp_validation',[AuthController::class,'otp_validation']);
	Route::post('send_otp',[AuthController::class,'send_otp']);
    Route::post('login',[AuthController::class,'login']);
    Route::post('login_sosmed',[AuthController::class,'login_sosmed']);
});



//private Route
Route::group(['middleware'=> ['auth:sanctum']], function () {

	Route::prefix('auth')->group(function () {
	    Route::post('update_name',[AuthController::class,'update_name']);
	    Route::post('update_pin',[AuthController::class,'update_pin']);
	    Route::post('update_profile',[AuthController::class,'update_profile']);
	    Route::post('logout',[AuthController::class,'logout']);
	});

	Route::post('post_product',[ProductController::class,'post']);

	// Group Sales
	Route::prefix('sales')->group(function () {
		//Route Home With AUTH
	    Route::get('/home', [HomeController::class,'index']);
	    Route::get('/home/performance', [HomeController::class,'performance']);
	    Route::get('/home/grafik', [HomeController::class,'grafik']);

	    //Route Shops With AUTH
	    Route::prefix('shop')->group(function () {
		    Route::get('/index', [ShopController::class,'index']);
		    Route::post('/verifikasi_akun', [ShopController::class,'verifikasi_akun']);
		    Route::post('/check_domain', [ShopController::class,'check_domain']);
		    Route::post('/register', [ShopController::class,'register']);
		    Route::post('/add_category', [ShopController::class,'add_category']);
		    Route::post('/add_master_delivery', [ShopController::class,'add_master_delivery']);
		    Route::post('/update_master_delivery', [ShopController::class,'update_master_delivery']);
		    Route::post('/delete_master_delivery', [ShopController::class,'delete_master_delivery']);
		    Route::post('/detail_master_delivery', [ShopController::class,'detail_master_delivery']);
		    Route::get('/master_delivery', [ShopController::class,'master_delivery']);
		});

		//Route Store With AUTH
	    Route::prefix('store')->group(function () {
		    Route::get('/index', [StoreController::class,'index']);
		    Route::get('/get_etalase', [StoreController::class,'get_etalase']);
		    Route::post('/add_etalase', [StoreController::class,'add_etalase']);
		    Route::post('/update_etalase', [StoreController::class,'update_etalase']);
		    Route::post('/delete_etalase', [StoreController::class,'delete_etalase']);
		    Route::post('/get_product', [StoreController::class,'get_product']);
		    Route::get('/get_category_product', [StoreController::class,'get_category_product']);
		});

	    //Route Product With AUTH
	    Route::prefix('product')->group(function () {
		    Route::get('/get_warehouse', [ProductController::class,'get_warehouse']);
		    Route::post('/add_warehouse', [ProductController::class,'add_warehouse']);
		    Route::post('/get_warehouse_by_id', [ProductController::class,'get_warehouse_by_id']);
		    Route::post('/delete_warehouse', [ProductController::class,'delete_warehouse']);
		    Route::post('/edit_warehouse', [ProductController::class,'edit_warehouse']);
		    Route::post('/add_category', [ProductController::class,'add_category']);
		    Route::post('/upload_image', [ProductController::class,'upload_image']);
		    Route::post('/add', [ProductController::class,'add']);
		    Route::post('/get_id', [ProductController::class,'get_id']);
		    Route::post('/update', [ProductController::class,'update']);
		    Route::post('/delete', [ProductController::class,'delete']);
		    Route::post('/set_status', [ProductController::class,'set_status']);
		    Route::post('/delete_image', [ProductController::class,'delete_image']);
		});

	});

	// Group Customer
	//Route Customer With AUTH
	Route::prefix('customer')->group(function () {
	    Route::get('/get_recomendation', [CustomerController::class,'get_recomendation']);
	    Route::post('/add_wishlist', [CustomerController::class,'add_wishlist']);
	    Route::post('/delete_wishlist', [CustomerController::class,'delete_wishlist']);
	    Route::post('/add_cart', [CustomerController::class,'add_wishlist']);
	    Route::post('/delete_cart', [CustomerController::class,'delete_cart']);
	    Route::get('/get_wishlist', [CustomerController::class,'get_wishlist']);
	    Route::get('/get_cart', [CustomerController::class,'get_cart']);
	    Route::get('/get_data', [CustomerController::class,'get_data']);
	    Route::get('/search_history', [CustomerController::class,'get_search_history']);
	    Route::post('/click_item_login', [CustomerController::class,'click_item_login']);
	    //cart
	    Route::prefix('cart')->group(function () {
		    Route::get('/index', [CartController::class,'index']);
		    Route::post('/add', [CartController::class,'add']);
		    Route::post('/checked', [CartController::class,'checked']);
		    Route::post('/edit', [CartController::class,'edit']);
		    Route::post('/delete', [CartController::class,'delete']);
		});
	});
});

//Route Customer
Route::prefix('customer')->group(function () {
    Route::post('/get_product', [ProductController::class,'get_product']);
    Route::get('/get_product_newly', [ProductController::class,'get_product_newly']);
    Route::get('/get_banner', [CustomerController::class,'get_banner']);
    Route::post('/get_seller_information', [CustomerController::class,'get_seller_information']);
    Route::post('/detail_product', [ProductController::class,'detail_product']);
    Route::get('/get_product_promotion', [ProductController::class,'get_product_promotion']);
    Route::post('/get_product/category', [ProductController::class,'product_by_category']);
    Route::post('/search_history', [CustomerController::class,'post_search_history']);
    Route::post('/search', [CustomerController::class,'search_recomendation']);
    Route::get('/riwayat_search', [CustomerController::class,'riwayat_search']);
    Route::post('/click_item', [CustomerController::class,'click_item']);
    Route::get('/list_promotion', [ProductController::class,'list_promotion']);
    Route::get('/data_promotion', [ProductController::class,'data_promotion']);
    Route::get('/data_promotion_flashsale', [ProductController::class,'data_promotion_flashsale']);
    Route::post('/detail_promotion', [ProductController::class,'detail_promotion']);
    Route::post('/get_product_shop', [ProductController::class,'get_product_shop']);
    Route::post('/get_product_shop/pagination', [ProductController::class,'get_product_shop_paginnation']);
    Route::post('/click_variant_produck', [ProductController::class,'click_variant_produck']);
    Route::post('/choose_variant_produck', [ProductController::class,'choose_variant_produck']);
    Route::get('/get_product/get_diskon', [ProductController::class,'get_diskon']);
    
});

// Get data umum
Route::prefix('get_data')->group(function () {
    //address
	Route::prefix('address')->group(function () {
        Route::get('/country', [GetDataController::class,'country']);
        Route::get('/province', [GetDataController::class,'province']);
        Route::get('/city/{id}', [GetDataController::class,'city']);
        Route::get('/district/{id}', [GetDataController::class,'district']);
        Route::get('/village/{id}', [GetDataController::class,'village']);
    });

    Route::get('/category_shop', [GetDataController::class,'category_shop']);
    Route::get('/category_product', [GetDataController::class,'category_product']);
    Route::get('/category_product_featured', [GetDataController::class,'category_product_featured']);
    Route::get('/master_delivery', [GetDataController::class,'master_delivery']);
    Route::post('/search_city', [GetDataController::class,'search_city']);
    Route::post('/etalase', [GetDataController::class,'etalase']);
    Route::get('/delivery', [GetDataController::class,'delivery']);
    Route::get('/product_of_city', [GetDataController::class,'product_of_city']);
    Route::get('/master_category', [GetDataController::class,'master_category']);
    Route::post('/get_sub_category', [GetDataController::class,'get_sub_category']);
    
});

Route::post('/update_position', [GetDataController::class,'update_position']);
